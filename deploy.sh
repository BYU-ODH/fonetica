#!/usr/bin/env bash
npx shadow-cljs release :app &&
echo "rsyncing..."
rsync -av --no-perms --del target/ humapps:/var/www/html/fonetica/fonetica-span
echo "rsync done"
echo "Deployment process finished. Please verify at:"
echo "https://fonetica-span.byu.edu"
