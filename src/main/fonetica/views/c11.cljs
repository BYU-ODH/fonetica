(ns fonetica.views.c11
  "Chapter 11"
  (:require [fonetica.views.components.shared :as shared :refer [ipa red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas Activity 1"
  []
  [:section.ra1
   [:li "Escriba el símbolo del alófono descrito por los siguientes rasgos." 
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.list-item
       [:li
        [:div.grid2
         [:div.l 
          [:div "media (semi-cerrada)"]
          [:div "anterior"]
          [:div "oral/sonora"]]
         [:div.r
          [:div.ipa.is-size-3 "[e]"]
          ]]]]
      [:div.list-item
       [:li
        [:div.grid2
         [:div.l 
          [:div "abierta"]
          [:div "central"]
          [:div "oral/ensordecida"]]
         [:div.r
          [:div.ipa.is-size-3 "[a4]"]
          ]]]]
      [:div.list-item
       [:li
        [:div.grid2
         [:div.l 
          [:div "cerrada"]
          [:div "anterior"]
          [:div "oral/sonora"]]
         [:div.r
          [:div.ipa.is-size-3 "[i]"]
          ]]]]
      [:div.list-item
       [:li
        [:div.grid2
         [:div.l 
          [:div "media"]
          [:div "posterior"]
          [:div "oral/sonora"]]
         [:div.r
          [:div.ipa.is-size-3 "[U]"]
          ]]]]
      [:div.list-item
       [:li
        [:div.grid2
         [:div.l 
          [:div "semi-vocal"]
          [:div "posterior"]
          [:div "oral/sonora"]]
         [:div.r
          [:div.ipa.is-size-3 "[å]"]
          ]]]]
      [:div.list-item
       [:li
        [:div.grid2
         [:div.l 
          [:div "semi-consonante"]
          [:div "anterior"]
          [:div "oral/sonora"]]
         [:div.r
          [:div.ipa.is-size-3 "[o]"]
          ]]]]
      [:div.list-item
       [:li
        [:div.grid2
         [:div.l 
          [:div "media (semi-abierta)"]
          [:div "anterior"]
          [:div "nasalizada/sonora"]]
         [:div.r
          [:div.ipa.is-size-3 "[j]"]
          ]]]]
      [:div.list-item
       [:li
        [:div.grid2
         [:div.l 
          [:div "cerrada"]
          [:div "posterior"]
          [:div "oral/sonora"]]
         [:div.r
          [:div.ipa.is-size-3 "[u]"]]]]]]]]])

(defn ra2
  "Respuestas Activity 2"
  []
  [:section.ra2
   [:li "Indique los rasgos fonéticos para los siguientes alófonos." 
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li.a 
        [:div.grid2 
         [:div.l  [ipa "[w]"]]
         [:div.r
          [:div "semi-consonante"]
          [:div "posterior"]
          [:div "oral/sonora"]]]]
       [:li.b 
        [:div.grid2 
         [:div.l  [ipa "[I]"]]
         [:div.r
          [:div "semi-vocal"]
          [:div "anterior"]
          [:div "oral/sonora"]]]]
       [:li.c 
        [:div.grid2 
         [:div.l  [ipa "[E]"]]
       [:div.r
        [:div "media (semi-abierta)"]
        [:div "anterior"]
        [:div "oral/sonora"]]]]
       [:li.d 
        [:div.grid2 
         [:div.l  [ipa "[ß]"]]
         [:div.r
          [:div "media"]
          [:div "posterior"]
          [:div "nasalizada/sonora"]
          ]
         ]]]
      [:div.r
       [:li.e
        [:div.grid2 
         [:div.l  [ipa "[e4]"]]
         [:div.r
          [:div "media (semi-cerrada)"]
          [:div "anterior"]
          [:div "oral/ensordecida"]]]]
       [:li.f
        [:div.grid2 
         [:div.l  [ipa "[à]"]]
         [:div.r
          [:div "abierta/baja"]
          [:div "central"]
          [:div "oral/sonora"]]]]
       [:li.g
        [:div.grid2
         [:div.l [ipa "[à]"]]
         [:div.r
          [:div "cerrada/alta"]
          [:div "posterior"]
          [:div "nasalizada/sonora"]]]]
       [:li.h 
        [:div.grid2 
         [:div.l  [ipa "[Þ]"]]
         [:div.r
          [:div "cerrada/alta"]
          [:div "anterior"]
          [:div "nasalizada/sonora"]]]]]]]]])

(defn ra3
  "Respuestas Activity 3"
  []
  [:section.ra3
   [:li "Haga una cadena fonética, cambiando un solo rasgo a la vez." 
    [:div "Las respuestas pueden variar; solo hay que asegurarse de que se cambie un solo rasgo a la vez. Para ejercicio d, solo es posible hacer un cambio. (¿Por qué?)"]]])

(defn ra4
  "Respuestas Activity 4"
  []
  [:section.ra4
   [:li "¿Cuál es la vocal que se produce con las siguientes conformaciones bucales?"
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:div.line-item 
        [:li [:img {:style {:margin-right "0.5em"}
                    :src  (shared/file "C11-A4a-ss.png")}]
         "[o]"]]
       [:div.line-item 
        [:li [:img {:style {:margin-right "0.5em"}
                    :src  (shared/file "C11-A4b-ss.png")}]
         "[i]"]]
       [:div.line-item 
        [:li [:img {:style {:margin-right "0.5em"}
                    :src  (shared/file "C11-A4c-ss.png")}]
         "[e]"]]
       [:div.line-item ]]
      [:div.r
       [:div.line-item 
        [:li [:img {:style {:margin-right "0.5em"}
                    :src  (shared/file "C11-A4d-ss.png")}]
         "[u]"]]
       [:div.line-item 
        [:li [:img {:style {:margin-right "0.5em"}
                    :src  (shared/file "C11-A4e-ss.png")}]
         "[a]"]]]]]]])
(defn ra5
  "Respuestas Activity 5"
  []
  [:section.ra5
   [:li "Para las siguientes reglas de distribución: (a) identifique el tipo de distribución, (b) interprete la regla en palabras, y (c) dé ejemplos de su aplicación, teniendo cuidado de ejemplificar todos los aspectos de la regla."
    [:ol.list.is-lower-alpha
     [:div.list-item 
      [:li
       [:div.grid2
        [:img.image {:style {"height" "6em"}
                     :src (shared/file "C11-A5a.jpg")}]
        [:div.r "(a) distribución complementaria; (b/c) el fonema /u/ se representa mediante la semi- consonante en posición prenuclear " [ipa "[pwÓDe]"] " , mediante la semi-vocal en posición pos- nuclear " [ipa "[kÒUsa]"] "  y mediante la vocal en posición nuclear " [ipa "[kÖna]"] " ."]]]]
     [:div.list-item 
      [:li
       [:div.grid2
        [:img {:style {"height" "9em"}
               :src (shared/file "C11-A5b.jpg")}]
        [:div.r "(a) distribución complementaria; (b/c) De modo general, el fonema /e/ resulta en una vocal semi-abierta en sílabas cerradas " [ipa "[sìªda]"] "  con la excepción de una sílaba cerrada en /s/ en posición final de palabra " [ipa "[klÒses]"] "  y esto con la única excepción del verbo " [ipa "[×s]"] " . De modo general, el fonema /e/ resulta en una vocal semi-cerrada en sílabas abiertas " [ipa "[sÓDa]"] "  con la excepción de cuando la próxima sílaba comienza con " [ipa "[r R t]"] "  " [ipa "[s×ro s×Ro m×ta]"] "  a no ser que la próxima " [ipa "[r t]"] "  aparezca en posición inicial de palabra " [ipa "[×sterÔo ×stetÔo]"] " ."]]]]
     [:div.list-item
      [:li
       [:div.grid2
        [:img {:style {"height" "6em"}
               :src (shared/file "C11-A5c.jpg")}]
        [:div.r "(a) distribución complementaria; (b/c) el fonema /i/ se representa mediante la semi- consonante en posición prenuclear " [ipa "[tjÓne]"] " , mediante la semi-vocal en posición pos- nuclear " [ipa "[ÒIRe]"] "  y mediante la vocal en posición nuclear " [ipa "[sÔma]"] " ."]]]]]]])

(defn ra6
  "Respuestas Activity 6"
  []
  [:section.ra6
   [:li "El fonema /e/ tiene dos alófonos: [e] y [E]. Llene el espacio en blanco de las siguientes
transcripciones con el símbolo correcto según la regla."
    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {"gap" "0"}}
      [:div.l
       [:li.ipa "[p" [red "Ó"]"so]"]
       [:li.ipa "[s" [red "×"] "D]"]
       [:li.ipa "[atl" [red "×"] "ta]"]
       [:li.ipa "[m" [red "×"] "Ro]"]
       [:li.ipa "[" [red "×"] "sto]"]]
      [:div.r
       [:li.ipa "[" [red "e"] "n" [red "×"] "Ro]"]
       [:li.ipa "[" [red "×"] "rÕr]"]
       [:li.ipa "[s" [red "Ó"] "s" [red "ì"] "ªta]"]
       [:li.ipa "[" [red "Ó"] "v" [red "ì"] "ªto]"]
       [:li.ipa "[s" [red "Ó"] "lo]"]]
      [:div.l2 
       [:li.ipa "[alfaB" [red "×"] "to]"]
       [:li.ipa "[tj" [red "×"] "ra]"]
       [:li.ipa "[tR" [red "Ó"] "s]"]
       [:li.ipa "[x" [red "ì"] "ªt" [red "e"] "rÒRa]"]
       [:li.ipa "[consj" [red "×"] "rto]"]]
      [:div.r2
       [:li.ipa "[m" [red "Ó"] "nos]"]
       [:li.ipa "[" [red "E"] "skÒp" [red "e"] "]"]
       [:li.ipa "[" [red "ì"] "ªtÕns" [red "e"] "s]"]
       [:li.ipa "[p" [red "E"] "R" [red "e"] "sÕso]"]
       [:li.ipa "[b" [red "e"] "x" [red "e"] "tÒl]"]]]]]])

(defn ra7
  "Respuestas Activity 7"
  []
  [:section.ra7
   [:li "Cada fonema vocálico tiene por lo menos dos alófonos vocálicos: uno oral y otro nasalizado/oronasal. Indique cuáles son las vocales que se nasalizan."
    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {"gap" "0"}}
      [:div.l
       [:li.ipa "[mÒno]"]
       [:li.ipa "[n×to]"]
       [:li.ipa "[mÒlo]"]
       [:li.ipa "[as" [red "ì"] "ªto]"]
       [:li.ipa "[m" [red "ì"] "ªte]"]
       [:li.ipa "[nÓne]"]
       [:li.ipa "[Ê" [red "æ"] "Ngo]"]
       [:li.ipa "[ma¦Òna]"]
       [:li.ipa "[tRi" [red "ê"] "Mfo]"]
       [:li.ipa "[matasÒnos]"]]
      [:div.r
       [:li.ipa "[man" [red "Ü"] "ªtjÒl]"]
       [:li.ipa "[m" [red "ì"] "nso]"]
       [:li.ipa "[lÓma]"]
       [:li.ipa "[makanÖDo]"]
       [:li.ipa "[nÔDo]"]
       [:li.ipa "[x" [red "ì"] "ªte]"]
       [:li.ipa "[m" [red "æ"] "mbo]"]
       [:li.ipa "[g" [red "æ"] "nso]"]
       [:li.ipa "[p" [red "æ"] "øÊo]"]
       [:li.ipa "[p" [red "è"] "Ngo]"]]]]]])

(defn render-respuestas
  "Chapter 11 respuestas"
  []
  [:main.c11.respuestas.content
   [shared/fonetica-title "Capítulo 11 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]
     [ra5]
     [ra6]
     [ra7]]]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:li "Escriba el símbolo del alófono descrito por los siguientes rasgos." [shared/media-icon :r (rfe/href :fonetica.routes/c11-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.list-item
       [:li
        [:div "media (semi-cerrada)"]
        [:div "anterior"]
        [:div "oral/sonora"]]]
      [:div.list-item
       [:li
        [:div "abierta"]
        [:div "central"]
        [:div "oral/ensordecida"]]]
      [:div.list-item
       [:li 
        [:div "cerrada"]
        [:div "anterior"]
        [:div "oral/sonora"]]]
      [:div.list-item
       [:li 
        [:div "media"]
        [:div "posterior"]
        [:div "oral/sonora"]]]
      [:div.list-item
       [:li 
        [:div "semi-vocal"]
        [:div "posterior"]
        [:div "oral/sonora"]]]
      [:div.list-item
       [:li 
        [:div "semi-consonante"]
        [:div "anterior"]
        [:div "oral/sonora"]]]
      [:div.list-item
       [:li 
        [:div "media (semi-abierta)"]
        [:div "anterior"]
        [:div "nasalizada/sonora"]]]
      [:div.list-item
       [:li 
        [:div "cerrada"]
        [:div "posterior"]
        [:div "oral/sonora"]]]]]]])

(defn a2
  "Actividades 2"
  []
  [:section.a2
   [:li "Indique los rasgos fonéticos para los siguientes alófonos." [shared/media-icon :r (rfe/href :fonetica.routes/c11-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li [ipa "[w]"]]
       [:li [ipa "[I]"]]
       [:li [ipa "[E]"]]
       [:li [ipa "[ß]"]]]
      [:div.r
       [:li [ipa "[e4]"]]
       [:li [ipa "[a]"]]
       [:li [ipa "[à]"]]
       [:li [ipa "[Þ]"]]]]]]])
(defn a3
  "Actividades 3"
  []
  [:section.a3
   [:li "Haga una cadena fonética, cambiando un solo rasgo a la vez." [shared/media-icon :r (rfe/href :fonetica.routes/c11-r) :same-session]
    [:ol.list.is-lower-alpha
     [:li [ipa "[j] > [ ] > [ ] > [ ]"]]
     [:li [ipa "[E] > [ ] > [ ] > [ ]"]]
     [:li [ipa "[o] > [ ] > [ ] > [ ]"]]
     [:li [ipa "[a] > [ ] > [ ] > [ ]"]]]]])
(defn a4
  "Actividades 4"
  []
  [:section.a4
   [:li "¿Cuál es la vocal que se produce con las siguientes conformaciones bucales?" [shared/media-icon :r (rfe/href :fonetica.routes/c11-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li [:img {:src (shared/file "C11-A4a-ss.png")}]]
       [:li [:img {:src (shared/file "C11-A4b-ss.png")}]]
       [:li [:img {:src (shared/file "C11-A4c-ss.png")}]]]
      [:div.r
       [:li [:img {:src (shared/file "C11-A4d-ss.png")}]]
       [:li [:img {:src (shared/file "C11-A4e-ss.png")}]]]]]]])

(defn a5
  "Actividades 5"
  []
  [:section.a5
   [:li "Para las siguientes reglas de distribución: (a) identifique el tipo de distribución, (b) interprete la regla en palabras, y (c) dé ejemplos de su aplicación, teniendo cuidado de ejemplificar todos los aspectos de la regla."
    [shared/media-icon :r (rfe/href :fonetica.routes/c11-r) :same-session] 
    [:ol.list.is-lower-alpha
     [:div.list-item 
      [:li [:img.image {:style {"height" "6em"}
                        :src (shared/file "C11-A5a.jpg")}]]]
     [:div.list-item 
      [:li [:img {:style {"height" "9em"}
                  :src (shared/file "C11-A5b.jpg")}]]]
     [:div.list-item
      [:li [:img {:style {"height" "6em"}
                  :src (shared/file "C11-A5c.jpg")}]]]]]])

(defn a6
  "Actividades 6"
  []
  [:section.a6
   [:li "El fonema /e/ tiene dos alófonos: [e] y [E]. Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    [shared/media-icon :r (rfe/href :fonetica.routes/c11-r) :same-session] 
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li [ipa "[p so]"]]
       [:li [ipa "[s D]"]]
       [:li [ipa "[atl ta]"]]
       [:li [ipa "[m Ro]"]]
       [:li [ipa "[ sto]"]]
       [:li [ipa "[ n Ro]"]]
       [:li [ipa "[ rÕr]"]]
       [:li [ipa "[s s ªta]"]]
       [:li [ipa "[ v ªto]"]]
       [:li [ipa "[s lo]"]]]
      [:div.r
       [:li [ipa "[alfaB to]"]]
       [:li [ipa "[tj ra]"]]
       [:li [ipa "[tR s]"]]
       [:li [ipa "[x ªt rÒRa]"]]
       [:li [ipa "[consj rto]"]]
       [:li [ipa "[m nos]"]]
       [:li [ipa "[ skÒp ]"]]
       [:li [ipa "[ ªtÕns s]"]]
       [:li [ipa "[p R sÕso]"]]
       [:li [ipa "[b x tÒl]"]]
       ]]]]])
(defn a7
  "Actividades 7"
  []
  [:section.a7
   [:li "Cada fonema vocálico tiene por lo menos dos alófonos vocálicos: uno oral y otro
nasalizado/oronasal. Indique cuáles son las vocales que se nasalizan."
    [shared/media-icon :r (rfe/href :fonetica.routes/c11-r) :same-session] 
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li [ipa "[mÒno]"]]
       [:li [ipa "[n×to]"]]
       [:li [ipa "[mÒlo]"]]
       [:li [ipa "[as×ªto]"]]
       [:li [ipa "[m×ªte]"]]
       [:li [ipa "[manaªtjÒl]"]]
       [:li [ipa "[m×nso]"]]
       [:li [ipa "[lÓma]"]]
       [:li [ipa "[makanÖDo]"]]
       [:li [ipa "[nÔDo]"]]]
      [:div.r
       [:li [ipa "[nÓne]"]]
       [:li [ipa "[ÊÒNgo]"]]
       [:li [ipa "[ma¦Òna]"]]
       [:li [ipa "[tRiÖMfo]"]]
       [:li [ipa "[matasÒnos]"]]
       [:li [ipa "[x×ªte]"]]
       [:li [ipa "[mÒmbo]"]]
       [:li [ipa "[gÒnso]"]]
       [:li [ipa "[pÒøÊo]"]]
       [:li [ipa "[pÔNgo]"]]]]]]])

(defn actividades
  "Chapter 11 actividades"
  []
  [:section.actividades.content
   [:h2 "ACTIVIDADES"]
   [:ol.actividades
    [a1]
    [a2]
    [a3]
    [a4]
    [a5]
    [a6]
    [a7]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li (shared/media-icon :video (shared/file "C11-M1.mp4")) "Video de la abertura progresiva de las secuencias vocálicas."]
    [:li (shared/media-icon :audio (shared/file "C11-M2.mp3")) "El ensordecimiento vocálico."]
    [:li (shared/media-icon :audio (shared/file "C11-M3.mp3")) "Las vocales sonoras y ensordecidas."]
    [:li (shared/media-icon :audio (shared/file "C11-M4.mp3")) "La nasalización vocálica."]
    [:li (shared/media-icon :audio (shared/file "C11-M5.mp3")) "Las vocales orales y oronasales."]
    [:li (shared/media-icon :audio (shared/file "C11-M6.mp3")) "Grado de nasalización en la palabra {canta} en español y portugués."]
    [:li (shared/media-icon :video (shared/file "C11-M7.mp4")) "Las vocales deslabializadas y labializadas."]
    [:li (shared/media-icon :audio (shared/file "C11-M8.mp3")) "Vocales átonas y tónicas."]
    [:li (shared/media-icon :audio (shared/file "C11-M9.mp3")) "El alargamiento vocálico."]
    [:li (shared/media-icon :audio (shared/file "C11-M10.mp3")) "El ataque vocálico creciente y el ataque vocálico abrupto."]
    [:li (shared/media-icon :audio (shared/file "C11-M11.mp3")) "La cesación vocálica abrupta y la cesación vocálica decreciente."]
    [:li (shared/media-icon :audio (shared/file "C11-M12.mp3")) "Pares mínimos de los fonemas vocálicos del español."]
    [:li (shared/media-icon :audio (shared/file "C11-M13.mp3")) "Ejemplos de los fonemas vocálicos del inglés."]
    [:li (shared/media-icon :audio (shared/file "C11-M14.mp3")) "Las vocales largas y breves inglesas (los Cuadros 11.10 y 11.11)."]
    [:li (shared/media-icon :audio (shared/file "C11-M15.mp3")) "Las vocales reducidas inglesas."]
    [:li (shared/media-icon :audio (shared/file "C11-M16.mp3")) "Las vocales tónicas, átonas y reducidas del inglés (el Cuadro 11.14)."]
    [:li (shared/media-icon :audio (shared/file "C11-M17.mp3")) "La reducción vocálica del inglés al cero fonético."]
    [:li (shared/media-icon :audio (shared/file "C11-M18.mp3")) "La comparación entre las vocales tónicas de español e inglés (el Cuadro 11.17)."]
    [:li (shared/media-icon :audio (shared/file "C11-M19.mp3")) "El contraste entre [" [ipa "e"] "] y [" [ipa "eI"] "] en español."]
    [:li (shared/media-icon :audio (shared/file "C11-M20.mp3")) "La distribución complementaria de /e/ (el Cuadro 11.25)."]
    [:li (shared/media-icon :audio (shared/file "C11-M21.mp3")) "La distribución complementaria de /i/ (el Cuadro 11.39)."]
    [:li (shared/media-icon :audio (shared/file "C11-M22.mp3")) "La distribución complementaria de /u/ (el Cuadro 11.45)."]
    [:li (shared/media-icon :ej (shared/file "C11-M23.mp3")) "Ejercicios de pronunciación: la vocal /a/."]
    [:li (shared/media-icon :ej (shared/file "C11-M24.mp3")) "Ejercicios de pronunciación: la vocal /e/."]
    [:li (shared/media-icon :ej (shared/file "C11-M25.mp3")) "Ejercicios de pronunciación: la vocal /o/."]
    [:li (shared/media-icon :ej (shared/file "C11-M26.mp3")) "Ejercicios de pronunciación: la vocal /i/."]
    [:li (shared/media-icon :ej (shared/file "C11-M27.mp3")) "Ejercicios de pronunciación: la vocal /u/."]]])
(defn render
  "Chapter eleven view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 11 — Los fonemas vocálicos"
    "E-Resources"]
   [:div.content.chapter-11
    [materiales]
    [actividades]]
   [shared/footer]])
