(ns fonetica.views.c9
  "Chapter 9"
  (:require [fonetica.views.components.shared :as shared :refer [ipa]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas Activity 1"
  []
  [:section.ra1
   [:li.respuestas-a1 "Escriba el símbolo del alófono descrito por los siguientes rasgos."
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li.a
        [:div.grid2 
         [:div.l [:div "fricativo"]
          [:div "bilabial"]
          [:div "sonoro"]]
         [:div.r.ipa.red.is-size-3
          [:div "[B]"]]]]
       [:li.b
        [:div.grid2 
         [:div.l
          [:div "fricativo"]
          [:div "velar"]
          [:div "sonoro"]]
         [:div.r.ipa.red.is-size-3
          [:div "[G]"]]]]
       [:li.c
        [:div.grid2 
         [:div.l
          [:div "lateral"]
          [:div "dental"]
          [:div "sonoro"]]
         [:div.r.ipa.red.is-size-3
          [:div "[¢]"]]]]
       [:li.d
        [:div.grid2 
         [:div.l
          [:div "oclusivo"]
          [:div "dental"]
          [:div "sonoro"]]
         [:div.r.ipa.red.is-size-3
          [:div "[d]"]]]]]
      [:div.r
       [:li.e
        [:div.grid2 
         [:div.l 
          [:div "nasal"]
          [:div "labiodental"]
          [:div "sonoro"]]
         [:div.r.ipa.red.is-size-3
          [:div "[M]"]]]]
       [:li.f
        [:div.grid2 
         [:div.l 
          [:div "fricativo"]
          [:div "alveolar"]
          [:div "sonoro"]]
         [:div.r.ipa.red.is-size-3
          [:div "[¸]"]]]]
       [:li.g
        [:div.grid2 
         [:div.l 
          [:div "oclusivo"]
          [:div "velar"]
          [:div "sordo"]]
         [:div.r.ipa.red.is-size-3
          [:div "[k]"]]]]
       [:li.h
        [:div.grid2 
         [:div.l 
          [:div "semiconsonante"]
          [:div "anterior"]]
         [:div.r.ipa.red.is-size-3
          [:div "[j]"]]]]]]]]])

(defn ra2
  "Respuestas Activity 2"
  []
  [:section.ra2
   [:li.respuestas-a2 "Indique los rasgos fonológicos para los siguientes fonemas."
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li.a [:div.grid2
               [:div.l.ipa
                [:div "[ª]"]]
               [:div.r.red
                [:div "nasal"]
                [:div "dental"]
                [:div "sonoro"]]]]
       [:li.b [:div.grid2
               [:div.l.ipa 
                [:div "[Î]"]]
               [:div.r.red
                [:div "africado"]
                [:div "palatal"]
                [:div "sordo"]]]]
       [:li.c [:div.grid2
               [:div.l.ipa 
                [:div "[U]"]]
               [:div.r.red
                [:div "semivocal"]
                [:div "posterior"]]]]
       [:li.d [:div.grid2
               [:div.l.ipa 
                [:div "[Ì]"]]
               [:div.r.red
                [:div "africado"]
                [:div "palatal"]
                [:div "sonoro"]]]]]
      [:div.r 
       [:li.e [:div.grid2
               [:div.l.ipa 
                [:div "[D]"]]
               [:div.r.red
                [:div "fricativo"]
                [:div "interdental"]
                [:div "sonoro"]]]]
       [:li.f [:div.grid2
               [:div.l.ipa 
                [:div "[N]"]]
               [:div.r.red
                [:div "nasal"]
                [:div "velar"]
                [:div "sonoro"]]]]
       [:li.g [:div.grid2
               [:div.l.ipa 
                [:div "[r]"]]
               [:div.r.red
                [:div "vibrante múltiple"]
                [:div "alveolar"]
                [:div "sonoro"]]]]
       [:li.h [:div.grid2
               [:div.l.ipa 
                [:div "[J]"]]
               [:div.r.red
                [:div "fricativo"]
                [:div "palatal"]
                [:div "sonoro"]]]]]]]]])

(defn ra3
  "Respuestas Activity 3"
  []
  [:section.ra3
   [:li.respuestas-a3 "Haga una cadena fonética, cambiando un solo rasgo a la vez."
    [:div 
     "Las respuestas varían; solo hay que asegurarse de que se cambie un solo rasgo a la vez sea el
modo de articulación, o el lugar de articulación, o el estado de las cuerdas vocales."]]])

(defn ra4
  "Respuestas Activity 4"
  []
  [:section.ra4
   [:li.respuestas-a4 "Para las siguientes reglas de distribución: (a) identifique el tipo de distribución, (b) interprete
la regla en palabras, y (c) dé ejemplos de su aplicación, teniendo cuidado de ejemplificar
todos los aspectos de la regla."
    [:ol.list.is-lower-alpha
     [:div.list-item 
      [:li
       [:div.grid2
        [:div.l
         [:img {:src (shared/file "C9-A4-a_pRule.jpg")
                :style {"height" "1rem"}}]]
        [:div.r
         "(a) distribución única; (b) el fonema /p/ se representa mediante su único alófono " (ipa "[p]") "; (c) " (ipa "[pÒn]") ""]]]]
     [:div.list-item 
      [:li
       [:div.grid2
        [:div.l [:img {:src (shared/file "C9-A4-b_sRule.jpg")
                       :style {"height" "4rem"}}]]
        [:div.r
         "(a) distribución complementaria; (b) el fonema /s/ se representa mediante el alófono sonoro " (ipa "[¸]") " delante de cualquier consonante sonora y mediante el alófono sordo " (ipa "[s]") " en los demás lugares; (c) " (ipa "[de¸De]") "
" (ipa "[Óste]") ""]]]]
     [:div.list-item 
      [:li
       [:div.grid2
        [:div.l 
         [:img {:src (shared/file "C9-A4-c_r.jpg")
                :style {"height" "3rem"}}]]
        [:div.r "(a) distribución libre; (b) el fonema /r/ puede representarse indistintamente mediante tres alófonos según el dialecto; (c) " (ipa "[kÒro]") " " (ipa "[kÒƒo]") " " (ipa "[kÒXo]") ""]]]]
     [:div.list-item 
      [:li
       [:div.grid2
        [:div.l [:img {:src (shared/file "C9-A4-d_bdg.jpg")
                       :style {"height" "6rem"}}]]
        [:div.r "(a) distribución complementaria; (b) los fonemas /b d g/ se representan mediante los alófonos oclusivos " (ipa "[b d g]") " respectivamente tras pausa o tras nasal; el fonema /d/ se representa mediante el alófono oclusivo " (ipa "[d]") " tras lateral; los fonemas /b d g/ se representan mediante los alófonos fricativos " (ipa "[B D G]") " respectivamente en los demás lugares; (c) " (ipa "[beBÓ]") " " (ipa "[umbeBÓ]") " " (ipa "[dÒDo]") " " (ipa "[uªdÒDo]") " " (ipa "[e¢dÒDo]") " " (ipa "[gÒGo]") " " (ipa "[uNgÒGo]") ""]]]]
     [:div.list-item 
      [:li
       [:div.grid2
        [:div.l [:img {:src (shared/file "C9-A4-e_R.jpg")
                       :style {"height" "4rem"}}]]
        [:div.r "a) distribución mixta; (b) el archifonema /R/ se representa mediante el alófono " (ipa "[R]") " en posición final de palabra ante vocal; en los demás lugares, puede representarse indistintamente mediante tres alófonos según el hablante; (c) " (ipa "[sÓRÖno]") " " (ipa "[sÓrDÕs]") "" (ipa "[sÓƒDÕs]") ""]]]]
     [:div.list-item 
      [:li
       [:div.grid2
        [:div.l [:img {:src (shared/file "C9-A4-f_N.jpg")
                       :style {"height" "20rem"}}]]
        [:div.r "(a) distribución complementaria; (b/c) el archifonema /N/ se representa mediante el alófono " (ipa "[m]") " ante consonante bilabial " (ipa "[kÒmpo]") "; mediante el alófono " (ipa "[M]") " ante consonante labiodental " (ipa "[uMfaRÕl]") "; mediante el alófono " (ipa "[©]") " ante consonante interdental en dialectos no seseístas " (ipa "[kÒ©Ter]") "; mediante el alófono " (ipa "[ª]") " ante consonante dental " (ipa "[deªtÒl]") "; mediante el alófono " (ipa "[n]") " ante consonante alveolar " (ipa "[kÒnsa]") ", en posición final de palabra ante vocal " (ipa "[enÒsia]") " y ante pausa " (ipa "[bÒn]") "; mediante el alófono " (ipa "[ø]") " ante consonante palatal " (ipa "[ÒøÎo]") "; mediante el alófono " (ipa "[N]") " ante consonante velar " (ipa "[tÓNgo]") ""]]]]]]])

(defn render-respuestas
  "Chapter 9 respuestas"
  []
  [:main.c9.respuestas.content
   [shared/fonetica-title "Capítulo 9 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]]]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:li "Escriba el símbolo del fonema descrito por los siguientes rasgos." [shared/media-icon :r (rfe/href :fonetica.routes/c9-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:li.a
       [:div
        [:div "fricativo"]
        [:div "bilabial"]
        [:div "sonoro"]]]
      [:li.b 
       [:div 
        [:div "fricativo"]
        [:div "velar"]
        [:div "sonoro"]]]
      [:li.c 
       [:div 
        [:div "lateral"]
        [:div "dental"]
        [:div "sonoro"]]]
       [:li.d 
       [:div 
        [:div "oclusivo"]
        [:div "dental"]
        [:div "sonoro"]]]
      [:li.e 
       [:div 
        [:div "nasal"]
        [:div "labiodental"]
        [:div "sonoro"]]]
      [:li.f 
       [:div 
        [:div "fricativo"]
        [:div "alveolar"]
        [:div "sonoro"]]]
      [:li.g
       [:div 
        [:div "oclusivo"]
        [:div "velar"]
        [:div "sordo"]]]       
      [:li.h 
       [:div 
        [:div "semiconsonante"]
        [:div "anterior"]]]]]]])

(defn a2
  "Actividades 2"
  []
  [:section.a2
   [:li "Escriba el símbolo del fonema descrito por los siguientes rasgos." [shared/media-icon :r (rfe/href :fonetica.routes/c9-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li (ipa "[ª]")]
       [:li (ipa "[Î]")]
       [:li (ipa "[U]")]
       [:li (ipa "[Ì]")]]
      [:div.r
       [:li (ipa "[D]")]
       [:li (ipa "[N]")]
       [:li (ipa "[r]")]
       [:li (ipa "[J]")]]
      ]]]])

(defn a3
  "Actividades 3"
  []
  [:section.a3
   [:li "Haga una cadena fonética, cambiando un solo rasgo a la vez." [shared/media-icon :r (rfe/href :fonetica.routes/c9-r) :same-session]
    [:div "Ejemplo:"
     (ipa "[b] > [B] > [G] > [x]")
     [:p "En el primer paso, solo se cambia el modo de articulación a fricativa."]
     [:p "En el segundo paso, solo se cambia el lugar de articulación a velar."]
     [:p "En el tercer paso, solo se cambia el estado de las cuerdas vocales a sordo."]]
       
    [:ol.list.is-lower-alpha
     [:li.ipa "[x] > [ ] > [ ] > [ ]"]
     [:li.ipa "[Î] > [ ] > [ ] > [ ]"]
     [:li.ipa "[l] > [ ] > [ ] > [ ]"]
     [:li.ipa "[m] > [ ] > [ ] > [ ]"]
     [:li.ipa "[R] > [ ] > [ ] > [ ]"]]]])

(defn a4
  "Actividades 4"
  []
  [:section.a4
   [:li "Para las siguientes reglas de distribución: (a) identifique el tipo de distribución, (b) interprete la regla en palabras, y (c) dé ejemplos de su aplicación, teniendo cuidado de ejemplificar todos los aspectos de la regla."
    [shared/media-icon :r (rfe/href :fonetica.routes/c9-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.list-item 
      [:li [:img {:src (shared/file "C9-A4-a_pRule.jpg")
                  :style {"height" "1rem"}}]]]
     [:div.list-item
      [:li [:div [:img {:src (shared/file "C9-A4-b_sRule.jpg")
                        :style {"height" "4rem"}}]]]]
     [:div.list-item 
      [:li [:img {:src (shared/file "C9-A4-c_r.jpg")
                  :style {"height" "3rem"}}]]]
     [:div.list-item 
      [:li [:img {:src (shared/file "C9-A4-d_bdg.jpg")
                  :style {"height" "6rem"}}]]]
     [:div.list-item 
      [:li [:img {:src (shared/file "C9-A4-e_R.jpg")
                  :style {"height" "4rem"}}]]]
     [:div.list-item 
      [:li [:img {:src (shared/file "C9-A4-f_N.jpg")
                  :style {"height" "20rem"}}]]]]]])


(defn actividades
  "Chapter 9 actividades"
  []
   [:section.actividades.content
    [:h2 "ACTIVIDADES"]
    [:ol.actividades
     [a1]
     [a2]
     [a3]
     [a4]]])

(defn fuentes
  "The fuentes section"
  []
  [:section.fuentas
   [:h3 "FUENTES DE LOS EJERCICIOS DE TRANSCRIPCIÓN"]
   [:ol
    [:li "“Envidia,” " [:em "Historias de la Artámila"] " Ana María Matute"]
    [:li "“Los alambradores,” " [:em "Historias de la Artámila"] " Ana María Matute"]
    [:li "“A la deriva,” " [:em "Cuentos de amor"] " de locura y de muerte, Horacio Quiroga"]
    [:li "“La fotografía,” " [:em "Después del temporal"] " Enrique Amorim"]
    [:li "“Rosarito,” " [:em "Femeninas: Seis historias amorosas"] " Ramón María del Valle-Inclán"]
    [:li "“Chac Mool,” " [:em "Los días enmascarados"] " Carlos Fuentes"]
    [:li [:em "Platero y yo"] ", Juan Ramón Jiménez"]
    [:li [:em "San Manuel Bueno, mártir"] ", Miguel de Unamuno"]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C9-M1.mp3")]     "Ejemplos de la distribución complementaria del fonema /b/."]    
    [:li [shared/media-icon :audio (shared/file "C9-M2.mp3")]     "Ejemplos de la distribución complementaria del fonema /d/."]    
    [:li [shared/media-icon :audio (shared/file "C9-M3.mp3")]     "Ejemplos de la distribución complementaria del fonema /g/."]    
    [:li [shared/media-icon :audio (shared/file "C9-M4.mp3")]     "Ejemplos de la distribución complementaria del fonema /s/."]    
    [:li [shared/media-icon :audio (shared/file "C9-M5.mp3")]     "Ejemplos de la distribución complementaria del fonema /J/."]    
    [:li [shared/media-icon :audio (shared/file "C9-M6.mp3")]     "Ejemplos de la distribución complementaria del fonema /T/."]    
    [:li [shared/media-icon :audio (shared/file "C9-M7.mp3")]     "Ejemplos de la distribución complementaria del archifonema /N/."]    
    [:li [shared/media-icon :audio (shared/file "C9-M8.mp3")]     "Ejemplos de la distribución complementaria del fonema /l/."]    
    [:li [shared/media-icon :audio (shared/file "C9-M9.mp3")]     "Ejemplos de la distribución complementaria del fonema /i/."]    
    [:li [shared/media-icon :audio (shared/file "C9-M10.mp3")]     "Ejemplos de la distribución complementaria del fonema /u/."]    
    [:li [shared/media-icon :audio (shared/file "C9-M11.mp3")]     "Ejemplos de la distribución libre del fonema /r/."]    
    [:li [shared/media-icon :audio (shared/file "C9-M12.mp3")]     "Ejemplos de la distribución mixta del archifonema /R/."]    ]])

(defn render
  "Chapter 9 view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 9 — La relación entre fonemas y alófonos: la distribución"
    "E-Resources"]
   [:div.content.chapter-9
    [fuentes]
    [materiales]
    [actividades]]
   
   [shared/footer]])
