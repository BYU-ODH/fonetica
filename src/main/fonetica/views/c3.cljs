(ns fonetica.views.c3
  "Chapter 3"
  (:require [fonetica.views.components.shared :as shared :refer [ipa femp]]
            [reitit.frontend.easy :as rfe]))

(defn render-respuestas
  "Chapter 3 respuestas"
  []
  [:main.c3.respuestas.container.content
   [shared/fonetica-title "Capítulo 3 — Respuestas a las actividades"]
   [:ol
    [:li.c3-r-1
     "El sonido para /s/ de {es mío} se produce con la vibración de las cuerdas vocales. El sonido se produce así debido a que las cuerdas vocales también vibran al producir la consonante que lo sigue: " (ipa "[m]") ". El fenómeno descrito entonces es FONÉTICO, porque describe las características físicas que rigen la elección del sonido."]
    [:li.c3-r-2
     "El fenómeno descrito es FONOLÓGICO, porque describe un proceso mental en que dos
sonidos físicamente diferentes entre sí, se consideran idénticas. Es decir el pronunciar la
consonante final del verbo {es} como " (ipa "[s]") " en " (ipa "[×stÖJo]") " o como " (ipa "[¸]") " en " (ipa "[×¸mÔo]") " no altera su significado."]]])


(defn a4-tables
  "The three tables at the bottom af c3"
  []
  [:section.a4-tables
   (let [rows [["Nivel"
                "habla/individuo"
                "lengua/sociedad"]
               ["Unidad básica"
                "sonido"
                "fonema"]
               ["Representación gráfica"
                "entre corchetes"
                "entre barras"]
               ["Procesos"
                "físicos"
                "mentales"]
               ["Enfoque"
                "los sonidos"
                "el sistema de sonidos"]
               ["Disciplinas"
                [:span 
                 [:strong "DESCRIPCIONES:"]
                 [:ol 
                  [:li "articulatorias"]
                  [:li "acústicas"]
                  [:li "auditivas"]]
                 ]
                [:span
                 [:strong "RELACIONES:"]
                 [:ol 
                  [:li "oposiciones entre fonemas"]
                  [:li "distribuciones de alófonos"]
                  [:li "la fonotáctica"]]]]]]
     [:section.tables
      [:table.ftable.table.is-bordered
       [:thead [:tr [:td.blank] [:td "Fonética"] [:td "Fonología"]]]
       (into [:tbody]
             (for [[a b c] rows]
               [:tr [:td [:strong a]] [:td b] [:td c]]))]
      [:table.ftable.table.is-bordered.is-fullwidth
       [:thead [:tr [:td.blank] [:td "Fonética"] [:td "Fonología"]]]
       (into [:tbody]
             (for [[a _ _] rows]
               [:tr [:td [:strong a]] [:td ] [:td ]]))]
      [:table.ftable.table.is-bordered.is-fullwidth
       [:thead [:tr [:td.blank] [:td "Fonética"] [:td "Fonología"]]]
       (into [:tbody]
             (for [[a _ _] rows]
               [:tr [:td [:strong a]] [:td ] [:td ]]))]])])

(defn actividades
  "Chapter 3 actividades"
  []
   [:section.actividades.content
    [:h2 "ACTIVIDADES"]
    [:ol
     [:li.a1 "En español al decir {es tuyo} " (ipa "[×stÖJo] ") [shared/media-icon :audio (shared/file "C3-A1-es-tuyo.mp3")] ", se puede observar que en la pronunciación tanto de /s/ como de /t/ que las cuerdas vocales no vibran. ¿Cómo difiere la pronunciación al decir {es mío} " (ipa "[×¸mÔo]") [shared/media-icon :audio (shared/file "C3-A1-es-mio.mp3")] "? ¿Por qué cambia la pronunciación de /s/? ¿Este es un fenómeno fonético o fonológico? Justifique su respuesta. " [shared/media-icon :r (rfe/href :fonetica.routes/c3-r) :same-session]]

     [:li.a2
      "En español existe un solo fonema /s/, que se representa físicamente mediante dos sonidos: un sonido en que no vibran las cuerdas vocales " (ipa "[s] ") [shared/media-icon :audio (shared/file "C3-A2-s.mp3")] " y otro sonido en que sí vibran las cuerdas vocales " (ipa "[¸] ") [shared/media-icon :audio (shared/file "C3-A2-z.mp3")] ". ¿El hecho de que los dos sonidos diferentes representen un solo concepto mental es un fenómeno fonético o fonológico? Justifique su respuesta. " [shared/media-icon :r (rfe/href :fonetica.routes/c3-r) :same-session]]

     [:li.a3
      "Diga las frases " (ipa "[×stÖJo]") " y " (ipa "[×¸mÔo]") ", teniendo cuidado de usar el sonido adecuado para el concepto de /s/ al final del verbo."] ;; again, our IPA font is failing to produce accented vowels
     
     [:li.a4
      "Es importante entender bien las distinciones entre la fonética y la fonología que se resumen en el Cuadro 3.4 del libro. Imprima el cuadro en blanco que se reproduce en la siguiente página y practique rellenándolo hasta saberlo bien."]]

    [a4-tables]] )

(defn render
  "Chapter three view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 3 — La fonética y la fonología"
    "E-Resources"]
   [:div.content.chapter-3
    [actividades]]   
   [shared/footer]
   ])
