(ns fonetica.views.c14
  "Chapter 14"
  (:require [fonetica.views.components.shared :as shared :refer [ipa femp red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas Actividades 1"
  []
  [:section.ra1
   [:li "Escriba el símbolo del alófono descrito por los siguientes rasgos."
    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {:gap 0}}
      [:div.l
       [:div.line-item
        [:li
         [:div.grid2
          [:div.l 
           [:div "fricativa"]
           [:div "palatal"]
           [:div "sonora"]]
          [:div.r.ipa.is-size-3
           "[J]"]
          ]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.l 
           [:div "fricativa"]
           [:div "alveolar"]
           [:div "sorda"]]
          [:div.r.ipa.is-size-3
           "[s]"]
          ]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.l 
           [:div "africada"]
           [:div "palatal"]
           [:div "sonora"]]
          [:div.r.ipa.is-size-3
           "[Ì]"]
          ]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.l 
           [:div "fricativa"]
           [:div "labiodental"]
           [:div "sorda"]]
          [:div.r.ipa.is-size-3
           "[f]"]
          ]]]]
      [:div.r
       [:div.line-item
        [:li
         [:div.grid2
          [:div.l 
           [:div "africada"]
           [:div "palatal"]
           [:div "sorda"]]
          [:div.r.ipa.is-size-3
           "[Ê]"]
          ]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.l 
           [:div "fricativa"]
           [:div "velar"]
           [:div "sorda"]]
          [:div.r.ipa.is-size-3
           "[x]"]
          ]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.l 
           [:div "fricativa"]
           [:div "interdental"]
           [:div "sorda"]]
          [:div.r.ipa.is-size-3
           "[T]"]
          ]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.l 
           [:div "fricativa"]
           [:div "alveolar"]
           [:div "sonora"]]
          [:div.r.ipa.is-size-3
           "[¸]"]
          ]]]]]]]])

(defn ra2
  "Respuestas Actividades 2"
  []
  [:section.ra2
   [:li "Indique los rasgos fonéticos para los siguientes alófonos."
    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {:gap 0}}
      [:div.l
       [:div.line-item
        [:div.grid2
         [:li [ipa "[Ê]"]
          ]
         [:div.r
          [:div "africada"]
          [:div "palatal"]
          [:div "sorda"]]]
        ]
       [:div.line-item
        [:div.grid2
         [:li [ipa "[f]"]
          ]
         [:div.r
          [:div "fricativa"]
          [:div "labiodental"]
          [:div "sorda"]]]
        ]
       [:div.line-item
        [:div.grid2
         [:li [ipa "[x]"]
          ]
         [:div.r
          [:div "fricativa"]
          [:div "velar"]
          [:div "sorda"]]]
        ]
       [:div.line-item
        [:div.grid2
         [:li [ipa "[s]"]
          ]
         [:div.r
          [:div "fricativa"]
          [:div "alveolar"]
          [:div "sorda"]]]
        ]]
      [:div.r
       [:div.line-item
        [:div.grid2
         [:li [ipa "[T]"]
          ]
         [:div.r
          [:div "fricativa"]
          [:div "interdental"]
          [:div "sorda"]]]
        ]
       [:div.line-item
        [:div.grid2
         [:li [ipa "[J]"]
          ]
         [:div.r
          [:div "fricativa"]
          [:div "palatal"]
          [:div "sonora"]]]
        ]
       [:div.line-item
        [:div.grid2
         [:li [ipa "[¸]"]
          ]
         [:div.r
          [:div "fricativa"]
          [:div "alveolar"]
          [:div "sonora"]]]
        ]
       [:div.line-item
        [:div.grid2
         [:li [ipa "[Ì]"]
          ]
         [:div.r
          [:div "africada"]
          [:div "palatal"]
          [:div "sonora"]]]
        ]]]]]])

(defn ra3
  "Respuestas Actividades 3"
  []
  [:section.ra3
   [:li "Haga una cadena fonética, cambiando un solo rasgo a la vez."
    [:div "Las respuestas pueden variar; solo hay que asegurarse de que se cambie un solo rasgo a la vez."]]])

(defn ra4
  "Respuestas Actividades 4"
  []
  [:section.ra4
   [:li "El fonema " [ipa "/s/"] " tiene dos alófonos: " [ipa "[s]"] " y " [ipa "[¸]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:div.line-item [:li.ipa "[tÕDo" [red "¸"] "lo" [red "¸"] "DÔa" [red "s"] "]"]]
       [:div.line-item [:li.ipa "[pe" [red "s"] "×ta" [red "s"] "]"]]
       [:div.line-item [:li.ipa "[" [red "s"] "u" [red "s"] "titÖto]"]]
       [:div.line-item [:li.ipa "[la" [red "¸"] "maD×ra" [red "¸"] "De" [red "s"] "ÒNxwÒn]"]]
       [:div.line-item [:li.ipa "[dE" [red "¸"] "Dìn]"]]
       [:div.line-item [:li.ipa "[pÒ" [red "s"] "a" [red "s"] "]"]]]
      [:div.r
       [:div.line-item [:li.ipa "[pRe" [red "s"] "iDìªte]"]]
       [:div.line-item [:li.ipa "[tìn" [red "s"] "o]"]]
       [:div.line-item [:li.ipa "[" [red "s"] "u" [red "s"] "Öro]"]]
       [:div.line-item [:li.ipa "[" [red "s"] "umaxE" [red "s"] "tÒD]"]]
       [:div.line-item [:li.ipa "[la" [red "s"] "i" [red "¸"] "la" [red "¸"] "Bale`ÒRe" [red "s"] "]"]]
       [:div.line-item [:li.ipa "[la" [red "s"] "wÓGRa]"]]]]]]])

(defn ra5
  "Respuestas Actividades 5"
  []
  [:section.ra5
   [:li "El fonema /J/ tiene dos alófonos: " [ipa "[J]"] " y " [ipa "[Ì]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:div.line-item [:li.ipa "[Eù" [red "Ì"] "Óso]"]]
       [:div.line-item [:li.ipa "[kÜnsi" [red "J"] "×r]"]]
       [:div.line-item [:li.ipa "[ma" [red "J"] "Òte]"]]
       [:div.line-item [:li.ipa "[rÓ" [red "J"] "es]"]]
       [:div.line-item [:li.ipa "[" [red "Ì"] "ÒnÕBjÓne]"]]
       [:div.line-item [:li.ipa "[la" [red "J"] "ÓDra]"]]]
      [:div.r
       [:div.line-item [:li.ipa "[uø" [red "Ì"] "ÖGo]"]]
       [:div.line-item [:li.ipa "[mÒ" [red "J"] "a]"]]
       [:div.line-item [:li.ipa "[a" [red "J"] "×r]"]]
       [:div.line-item [:li.ipa "[a" [red "J"] "anÒr]"]]
       [:div.line-item [:li.ipa "[uø" [red "Ì"] "Òte]"]]
       [:div.line-item [:li.ipa "[rÕ" [red "J"] "o]"]]]]]]])

(defn ra6
  "Respuestas Actividades 6"
  []
  [:section.ra6
   [:li "Complete el siguiente cuadro con la transcripción de las siguientes palabras or frases de acuerdo con dialectos seseístas o distinguistas."
    (let [rows [["diez centavos" "[djÓsåªtÒBos]" [:span "[djÓTåªtÒBo" [:span.ipa2 "†"]"]"]]
                ["tuércele el cuello al cisne" "[tw×rselElkwÓJo`alsÔ¸ne]" [:span "[tw×rTelElkwÓJo`alTÔ" [:span.ipa2 "‡"] "ne]"] ]
                ["esencia" "[esìnsja]" [:span "[e" [:span.ipa2 "†"] "ìnTja]"] ]
                ["coser" "[kos×r]" [:span "[ko" [:span.ipa2 "†"] "×r]"] ]
                ["cocer" "[kos×r]" "[koT×r]" ]
                ["buen mozo" "[bw×m:Õso]" "[bw×m:ÕTo]" ]
                ["la luz del día" "[lalÖ¸DE¢dÔa]" "[lalÖDE¢dÔa]" ]
                ["diez sellos" "[djÓsÓJos]" [:span "[djÓT†ÓJo" [:span.ipa2 "†]"]] ]]]
      [:table.table.ftable.is-bordered
       [:thead [:tr [:th "Palabra o frase"] [:th "Seseo"] [:th "Distinción"]]]
       (into [:tbody]
             (for [[p s d] rows]
               [:tr [:td p] [:td.ipa.red s] [:td.ipa.red d]]))])]])

(defn ra7
  "Respuestas Actividades 7"
  []
  [:section.ra7
[:li "Complete el siguiente cuadro con la transcripción de las siguientes palabras or frases de
acuerdo con dialectos yeístas o distinguistas."
 (let [rows [["halla" "[ÒJa]" "[ÒYa]"]
             ["haya" "[ÒJa]" "[ÒJa]"]
             ["el llano en llamas" "[ElÌÒno`×øÌÒmas]" "[EùYÒno`×øYÒmas]"]
             ["el hierro" "[ElÌ×ro]" "[ElÌ×ro]" ]
             ["lleva la llanta" "[ÌÓBalaJæªta]" "[YÓBalaYæªta]" ]
             ["las bellas artes" "[la¸B×JasÒrtes]" "[la¸B×YasÒrtes]" ]
             ["el yerno" "[ElÌ×rno]" "[ElÌ×rno]" ]
             ["leyendo" "[leJìªdo]" "[leJìªdo]" ]]]
      [:table.table.ftable.is-bordered
       [:thead [:tr [:th "Palabra o frase"] [:th "Yeísmo"] [:th "Distinción"]]]
       (into [:tbody]
             (for [[p y d] rows]
               [:tr [:td p] [:td.red.ipa y] [:td.red.ipa d]]))])]])

(defn ra8
  "Respuestas Actividades 8"
  []
  [:section.ra8
   [:li "Transcriba las siguientes palabras/frases fonéticamente:"
    [:ol.list.is-lower-alpha
     [:li
      [:div.list-item.grid2
       [:div "alfalfa"] [:div.red.ipa "[alfÒlfa]"]]]
     [:li
      [:div.list-item.grid2
       [:div "frenético"] [:div.red.ipa "[fRen×tiko]"]]]
     [:li
      [:div.list-item.grid2
       [:div "achacar"] [:div.red.ipa "[aÊakÒr]"]]]
     [:li
      [:div.list-item.grid2
       [:div "un giro"] [:div.red.ipa "[àNxÔRo]"]]]
     [:li
      [:div.list-item.grid2
       [:div "jaca negra"] [:div.red.ipa "[xÒkanÓGRa]"]]]
     [:li
      [:div.list-item.grid2
       [:div "México lindo querido"] [:div.red.ipa "[mÓxikolèªdokERÔDo]"]]]
     [:li
      [:div.list-item.grid2
       [:div "rehenes"] [:div.red.ipa "[rÓ:nes]"]]]
     [:li
      [:div.list-item.grid2
       [:div "sollozos"] [:div.red.ipa "[soJÕsos]"]]]
     [:li
      [:div.list-item.grid2
       [:div "sujetapapeles"] [:div.red.ipa "[suxEtapapÓles]"]]]
     [:li
      [:div.list-item.grid2
       [:div "asqueroso"] [:div.red.ipa "[askERÕso]"]]]
     [:li
      [:div.list-item.grid2
       [:div "la yegua"] [:div.red.ipa "[laJÓGwa]"]]]
     [:li
      [:div.list-item.grid2
       [:div "la jota"] [:div.red.ipa "[laxÕta]"]]]
     [:li
      [:div.list-item.grid2
       [:div "maracuyá"] [:div.red.ipa "[maRacuJÒ]"]]]
     [:li
      [:div.list-item.grid2
       [:div "perezoso"] [:div.red.ipa "[pEResÕso]"]]]
     [:li
      [:div.list-item.grid2
       [:div "esfuerzo"] [:div.red.ipa "[Esfw×rso]"]]]
     [:li
      [:div.list-item.grid2
       [:div "ejecución"] [:div.red.ipa "[exekusjén]"]]]
     [:li
      [:div.list-item.grid2
       [:div "leyenda negra"] [:div.red.ipa "[leJìªdanÓGRa]"]]]
     [:li
      [:div.list-item.grid2
       [:div "Algeciras"] [:div.red.ipa "[alxesÔRas]"]]]
     [:li
      [:div.list-item.grid2
       [:div "cofre"] [:div.red.ipa "[kÕfRe]"]]]
     [:li
      [:div.list-item.grid2
       [:div "alcachofa"] [:div.red.ipa "[alcaÊÕfa]"]]]]]])

(defn render-respuestas
  "Chapter 14 respuestas"
  []
  [:main.c14.respuestas.content
   [shared/fonetica-title "Capítulo 14 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]
     [ra5]
     [ra6]
     [ra7]
     [ra8]]]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:li "Escriba el símbolo del alófono descrito por los siguientes rasgos."
    [shared/media-icon :r (rfe/href :fonetica.routes/c14-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {:gap 0}}
      [:div.l
       [:div.line-item
        [:li 
         [:div "fricativa"]
         [:div "palatal"]
         [:div "sonora"]]]
       [:div.line-item
        [:li
         [:div "fricativa"]
         [:div "alveolar"]
         [:div "sorda"]]]
       [:div.line-item
        [:li 
         [:div "africada"]
         [:div "palatal"]
         [:div "sonora"]]]
       [:div.line-item
        [:li 
         [:div "fricativa"]
         [:div "labiodental"]
         [:div "sorda"]]]]
      [:div.r
       [:div.line-item
        [:li 
         [:div "africada"]
         [:div "palatal"]
         [:div "sorda"]]]
       [:div.line-item
        [:li 
         [:div "fricativa"]
         [:div "velar"]
         [:div "sorda"]]]
       [:div.line-item
        [:li 
         [:div "fricativa"]
         [:div "interdental"]
         [:div "sorda"]]]
       [:div.line-item
        [:li 
         [:div "fricativa"]
         [:div "alveolar"]
         [:div "sonora"]]]]]]]])

(defn a2
  "Actividades 2"
  []
  [:section.a2
   [:li "Indique los rasgos fonéticos para los siguientes alófonos."
    [shared/media-icon :r (rfe/href :fonetica.routes/c14-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {:gap 0}}
      [:div.l
       [:li [ipa "[Ê]"]]
       [:li [ipa "[f]"]]
       [:li [ipa "[x]"]]
       [:li [ipa "[s]"]]]
      [:div.r
       [:li [ipa "[T]"]]
       [:li [ipa "[J]"]]
       [:li [ipa "[¸]"]]
       [:li [ipa "[Ì]"]]]]]]])

(defn a3
  "Actividades 3"
  []
  [:section.a3
   [:li "Haga una cadena fonética, cambiando un solo rasgo a la vez."
    [shared/media-icon :r (rfe/href :fonetica.routes/c14-r) :same-session]
    [:ol.list.is-lower-alpha
     [:li.ipa "[s] > [ ] > [ ] > [ ]"]
     [:li.ipa "[Ê] > [ ] > [ ] > [ ]"]
     [:li.ipa "[x] > [ ] > [ ] > [ ]"]
     [:li.ipa "[J] > [ ] > [ ] > [ ]"]]]])

(defn a4
  "Actividades 4"
  []
  [:section.a4
   [:li "El fonema " [ipa "/s/"] " tiene dos alófonos: " [ipa "[s]"] " y " [ipa "[¸]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    [shared/media-icon :r (rfe/href :fonetica.routes/c14-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:div.line-item [:li.ipa "[tÕDo lo DÔa ]"]]
       [:div.line-item [:li.ipa "[pe ×ta ]"]]
       [:div.line-item [:li.ipa "[ u titÖto]"]]
       [:div.line-item [:li.ipa "[la maD×ra De ÒNxwÒn]"]]
       [:div.line-item [:li.ipa "[dE Dìn]"]]
       [:div.line-item [:li.ipa "[pÒ a ]"]]]
      [:div.r
       [:div.line-item [:li.ipa "[pRe iDìªte]"]]
       [:div.line-item [:li.ipa "[tìn o]"]]
       [:div.line-item [:li.ipa "[ u Öro]"]]
       [:div.line-item [:li.ipa "[ umaxE tÒD]"]]
       [:div.line-item [:li.ipa "[la i la Bale`ÒRe ]"]]
       [:div.line-item [:li.ipa "[la wÓGRa]"]]]]]]])

(defn a5
  "Actividades 5"
  []
  [:section.a5
   [:li "El fonema /J/ tiene dos alófonos: " [ipa "[J]"] " y " [ipa "[Ì]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    [shared/media-icon :r (rfe/href :fonetica.routes/c14-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:div.line-item [:li.ipa "[Eù Óso]"]]
       [:div.line-item [:li.ipa "[kÜnsi ×r]"]]
       [:div.line-item [:li.ipa "[ma Òte]"]]
       [:div.line-item [:li.ipa "[rÓ es]"]]
       [:div.line-item [:li.ipa "[ ÒnÕBjÓne]"]]
       [:div.line-item [:li.ipa "[la ÓDra]"]]]
      [:div.r
       [:div.line-item [:li.ipa "[uø ÖGo]"]]
       [:div.line-item [:li.ipa "[mÒ a]"]]
       [:div.line-item [:li.ipa "[a ×r]"]]
       [:div.line-item [:li.ipa "[a anÒr]"]]
       [:div.line-item [:li.ipa "[uø Òte]"]]
       [:div.line-item [:li.ipa "[rÕ o]"]]]]]]])

(defn a6
  "Actividades 6"
  []
  [:section.a6
   [:li "Complete el siguiente cuadro con la transcripción de las siguientes palabras or frases de acuerdo con dialectos seseístas o distinguistas."
    [shared/media-icon :r (rfe/href :fonetica.routes/c14-r) :same-session]
    (let [rows [["diez centavos" "" ""]
                ["tuércele el cuello al cisne" "" ""]
                ["esencia" "" ""]
                ["coser" "" ""]
                ["cocer" "" ""]
                ["buen mozo" "" ""]
                ["la luz del día" "" ""]
                ["diez sellos" "" ""]]]
      [:table.table.ftable.is-bordered
       [:thead [:tr [:th "Palabra o frase"] [:th "Seseo"] [:th "Distinción"]]]
       (into [:tbody]
             (for [[p s d] rows]
               [:tr [:td p] [:td s] [:td d]]))])]])

(defn a7
  "Actividades 7"
  []
  [:section.a7
[:li "Complete el siguiente cuadro con la transcripción de las siguientes palabras or frases de
acuerdo con dialectos yeístas o distinguistas."
    [shared/media-icon :r (rfe/href :fonetica.routes/c14-r) :same-session]
 (let [rows [["halla" "" ""]
             ["haya" "" ""]
             ["el llano en llamas" "" ""]
             ["el hierro" "" ""]
             ["lleva la llanta" "" ""]
             ["las bellas artes" "" ""]
             ["el yerno" "" ""]
             ["leyendo" "" ""]]]
      [:table.table.ftable.is-bordered
       [:thead [:tr [:th "Palabra o frase"] [:th "Yeísmo"] [:th "Distinción"]]]
       (into [:tbody]
             (for [[p y d] rows]
               [:tr [:td p] [:td y] [:td d]]))])]])
(defn a8
  "Actividades 8"
  []
  [:section.a8
   [:li "Transcriba las siguientes palabras/frases fonéticamente:"
    [shared/media-icon :r (rfe/href :fonetica.routes/c14-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li "alfalfa"]
       [:li "frenético"]
       [:li "achacar"]
       [:li "un giro"]
       [:li "jaca negra"]
       [:li "México lindo querido"]
       [:li "rehenes"]
       [:li "sollozos"]
       [:li "sujetapapeles"]
       [:li "asqueroso"]]
      [:div.r
       [:li "la yegua"]
       [:li "la jota"]
       [:li "maracuyá"]
       [:li "perezoso"]
       [:li "esfuerzo"]
       [:li "ejecución"]
       [:li "leyenda negra"]
       [:Li "Algeciras"]
       [:li "cofre"]
       [:li "alcachofa"]]]]]])

(defn actividades
  "Chapter 14 actividades"
  []
  [:section.actividades.content
   [:h2 "ACTIVIDADES"]
   [:ol.actividades
    [a1]
    [a2]
    [a3]
    [a4]
    [a5]
    [a6]
    [a7]
    [a8]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C14-M1.mp3")] "La oposición entre los fonemas fricativos (el Cuadro 14.1)."]
    [:li [shared/media-icon :audio (shared/file "C14-M2.mp3")] "Oposición y neutralización en relación con los fonemas " [ipa "/s/"] " y " [ipa "/T/"] " (el Cuadro 14.3)."]
    [:li [shared/media-icon :audio (shared/file "C14-M3.mp3")] "Oposición y neutralización en relación con los fonemas " [ipa "/J/"] " y " [ipa "/Y/"] " (el Cuadro 14.4)."]
    [:li [shared/media-icon :audio (shared/file "C14-M4.mp3")] "La distribución fonotáctica del fonema " [ipa "/f/"] " (el Cuadro 14.8)."]
    [:li [shared/media-icon :audio (shared/file "C14-M5.mp3")] "Variantes dialectales del fonema " [ipa "/f/"] "."]
    [:li [shared/media-icon :audio (shared/file "C14-M6.mp3")] "La distribución fonotáctica del fonema " [ipa "/s/"] " (el Cuadro 14.12)."]
    [:li [shared/media-icon :audio (shared/file "C14-M7.mp3")] "Los alófonos " [ipa "[s]"] " y " [ipa "[¸]"] " (las Figuras 14.14 y 14.15)."]
    [:li [shared/media-icon :audio (shared/file "C14-M8.mp3")] "Los alófonos del fonema " [ipa "/s/"] " según la tensión articulatoria."]
    [:li [shared/media-icon :audio (shared/file "C14-M9.mp3")] "Los alófonos del fonema " [ipa "/s/"] " según la aspiración (las Figuras 14.17 y 14.18)."]
    [:li [shared/media-icon :audio (shared/file "C14-M10.mp3")] "Las variantes del fonema " [ipa "/s/"] " en los dialectos de distinción."]
    [:li [shared/media-icon :audio (shared/file "C14-M11.mp3")] "Las variantes del fonema " [ipa "/T/"] " en los dialectos de distinción."]
    [:li [shared/media-icon :audio (shared/file "C14-M12.mp3")] "Los sonidos fricativos " [ipa "[s"] " † " [ipa "T]"] "."]
    [:li [shared/media-icon :audio (shared/file "C14-M13.mp3")] "Los sonidos fricativos alveolares sordos y sonoros en inglés y en español."]
    [:li [shared/media-icon :audio (shared/file "C14-M14.mp3")] "La oposición entre " [ipa "/s/"] " y " [ipa "/z/"] " en inglés (el Cuadro 14.25)."]
    [:li [shared/media-icon :audio (shared/file "C14-M15.mp3")] "El grafema {z} en español (el Cuadro 14.26)."]
    [:li [shared/media-icon :audio (shared/file "C14-M16.mp3")] "Los grafemas {s} y {z} en posición final de palabra en inglés y en español (el Cuadro 14.27)."]
    [:li [shared/media-icon :audio (shared/file "C14-M17.mp3")] "La diferencia entre " [ipa "[S]"] " en inglés y [sj] en español (el Cuadro 14.28)."]
    [:li [shared/media-icon :audio (shared/file "C14-M18.mp3")] "La pronunciación del fonema " [ipa "/s/"] " en posición final de sílaba ante consonante sonora (el Cuadro 14.29)."]
    [:li [shared/media-icon :audio (shared/file "C14-M19.mp3")] "Diferencias entre la fricativa " [ipa "[J]"] " y la semiconsonante " [ipa "[j]"] " (el Cuadro 14.33)."]
    [:li [shared/media-icon :audio (shared/file "C14-M20.mp3")] "El sonido fricativo " [ipa "[J]"] " en la palabra [kÒJe] (la Figura 14.34)."]
    [:li [shared/media-icon :audio (shared/file "C14-M21.mp3")] "El sonido africado " [ipa "[Ì]"] " en la palabra [ÌÒma] (la Figura 14.36)."]
    [:li [shared/media-icon :audio (shared/file "C14-M22.mp3")] "Realización fonética de las variantes de los fonemas " [ipa "/J/"] " y " [ipa "/Y/"] " en los dialectos de distinción (el Cuadro 14.37)."]
    [:li [shared/media-icon :audio (shared/file "C14-M23.mp3")] "Realización fonética de las variantes del fonema " [ipa "/J/"] " en los dialectos de yeísmo (el Cuadro 14.38)."]
    [:li [shared/media-icon :audio (shared/file "C14-M24.mp3")] "Variantes dialectales de los alófonos para el fonema " [ipa "/J/"] " (el Cuadro 14.39 y la Figura 14.40)."]
    [:li [shared/media-icon :audio (shared/file "C14-M25.mp3")] "Realización fonética del sonido " [ipa "/Y/"] " (la Figura 14.42)."]
    [:li [shared/media-icon :audio (shared/file "C14-M26.mp3")] "Diferencias entre el sonido " [ipa "[Y]"] " y la secuencia [lj]."]
    [:li [shared/media-icon :audio (shared/file "C14-M27.mp3")] "Realización del fonema " [ipa "/x/"] " (el Cuadro 14.44)."]
    [:li [shared/media-icon :audio (shared/file "C14-M28.mp3")] "Cómo aprender a articular el sonido " [ipa "[x]"] " y ejemplos de su realización."]
    [:li [shared/media-icon :audio (shared/file "C14-M29.mp3")] "La realización del sonido " [ipa "[Î]"] " (el Cuadro 14.48 y la Figura 14.50) "]
    [:li [shared/media-icon :ej (shared/file "C14-M30.mp3")] "Ejercicios de pronunciación: el fonema " [ipa "/s/"] ". "]
    [:li [shared/media-icon :ej (shared/file "C14-M31.mp3")] "Ejercicios de pronunciación: el fonema " [ipa "/J/"] ". "]
    [:li [shared/media-icon :ej (shared/file "C14-M32.mp3")] "Ejercicios de pronunciación: el fonema " [ipa "/x/"] ". "]
    [:li [shared/media-icon :ej (shared/file "C14-M33.mp3")] "Ejercicios de pronunciación: el grafema  {x}."]]])

(defn render
  "Chapter fourteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 14 — Los fonemas fricativos y el fonema africado"
    "E-Resources"]
   [:div.content.chapter-14
    [materiales]
    [actividades]]
   [shared/footer]])
