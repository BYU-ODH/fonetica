(ns fonetica.views.c5
  "Chapter 5"
  (:require [fonetica.views.components.shared :as shared :refer [ipa]]
            [reitit.frontend.easy :as rfe]))

(defn render-respuestas
  "Chapter 5 respuestas"
  []
  [:main.c5.respuestas.container.content
   [shared/fonetica-title "Capítulo 5 — Respuestas a las actividades"]
   [:ol.list.is-lower-alpha
    [:li "tengo " (ipa "[tìNgo]") [:span.red " 1) " (ipa "[ì]") " anticipante/nasalización; 2) " (ipa "[Ng]") " recíproca/lugar y modo de articulación"]]
    [:li "inferior " (ipa "[ÞmfERjÕr]") [:span.red " 1) " (ipa "[M]") " anticipante/lugar de articulación"]]
    [:li "la boca " (ipa "[laBÕka]") [:span.red " 1) " (ipa "[B]") " convergente/modo de articulación"]]
    [:li "ocho " (ipa "[ÕÎo4]") [:span.red " 1) " (ipa "[o4]") " perseverante/estado de las cuerdas vocales"]]
    [:li "la dama " (ipa "[laDÒma]") [:span.red " 1) " (ipa "[D]") " convergente/modo de articulación"]]
    [:li "un baso " (ipa "[àmbÒso]") [:span.red " 1) " (ipa "[à]") " anticipante/nasalización; 2) " (ipa "[mb]") " recíproca/lugar y modo de articulación"]]
    [:li "taquito " (ipa "[takÔto4]") [:span.red " 1) " (ipa "[o4]") " perseverante/estado de las cuerdas vocales"]]
    [:li "un vago " (ipa "[àmbÒGo]") [:span.red " 1) " (ipa "[à]") " anticipante/nasalización; 2) " (ipa "[mb]") " recíproca/lugar y modo de articulación"]]
    [:li "banda " (ipa "[bæªda]") [:span.red " 1) " (ipa "[à]") " anticipante/nasalización; 2) " (ipa "[ªd]") " recíproca/lugar y modo de articulación"]]
    [:li "peligroso " (ipa "[peliGrÕso]") [:span.red " 1) " (ipa "[G]") " convergente/modo de articulación"]]
    [:li "todos los días " (ipa "[tÕDo¸lo¸DÔas]") [:span.red " 1) " (ipa "[¸]") " anticipante/estado de las cuerdas vocales; 2) " (ipa "[D]") " convergente/modo de articulación"]]
    [:li "ancho " (ipa "[æøÎo]") [:span.red " 1) " (ipa "[æ]") " anticipante/nasalización; 2) " (ipa "[ø]") " anticipante/lugar de articulación"]]
    [:li "hago " (ipa "[ÒGo]") [:span.red " 1) " (ipa "[G]") " convergente/modo de articulación"]]]])

(defn ej-15
  "Capítulo 5: Nota 15"
  []
  (shared/render-ej {:index "Capítulo 5: Nota 15"
                     :title "Los lugares de articulación"
                     :body
                     [:section.nota15
                      "Llene los espacios en blanco con el término lingüístico para los distintos lugares de articulación de las consonantes de español."
                      [:img {:src (shared/file "C5-M15.jpg")
                             :alt "Llene los espacios en blanco con el término lingüístico para los distintos lugares de articulación de las consonantes de español."}]]}))

(defn ej-25
  "Capítulo 5: Nota 25"
  []
  (shared/render-ej {:index "Capítulo 5: Nota 25"
                     :title "El cuadro consonántico (para estudiar)"
                     :body
                     [:section.nota25
                      "Estudie bien el siguiente cuadro hasta saberlo bien."
                      [:img {:src (shared/file "C5-M25a.jpg")
                             :alt "Estudie bien el siguiente cuadro hasta saberlo bien."}]
                      [:section.nota25b
                       [shared/fonetica-title "El cuadro consonántico (para practicar)"]
                       [:p "Imprima el siguiente cuadro y rellénelo según los sonidos del español con los símbolos del Alfabeto Fonético Internacional hasta poder hacerlo a perfección."]
                       [:img {:src (shared/file "C5-M25b.jpg")
                              :alt "Estudie bien el siguiente cuadro hasta saberlo bien."}]]]}))

(defn ej-34
  "Capítulo 5: Nota 34"
  []
  (shared/render-ej {:index "Capítulo 5: Nota 34"
                     :title "El cuadro vocálico (para estudiar)"
                     :body
                     [:section.nota34
                      "Estudie bien el siguiente cuadro hasta saberlo bien."
                      [:img {:src (shared/file "C5-M34a.jpg")
                             :alt "Estudie bien el siguiente cuadro hasta saberlo bien."}]
                      [:section.nota34b
                       [shared/fonetica-title "El cuadro vocálico (para practicar)"]
                       [:p "Imprima el siguiente cuadro y rellénelo según los sonidos del español con los símbolos
del Alfabeto Fonético Internacional hasta poder hacerlo a perfección."]
                       [:img {:src (shared/file "C5-M34b.jpg")
                              :alt "Imprima el siguiente cuadro y rellénelo según los sonidos del español con los símbolos"}]]]}))

(defn ej-35
  "Capítulo 5: Nota 35"
  []
  (shared/render-ej {:index "Capítulo 5: Nota 35"
                     :title "La transcripción fonética"
                     :body
                     [:section.nota35
                      [:h3 "El texto ortográfico"]
                      [:p.quote "“Me volví y vi a un hombre en uniforme de portero y de guardián, con gorra y corto palo en la mano. Lo veía perfectamente en su figura humana, pero no estoy seguro de que fuera del todo opaco.”" [:sup "1"]]
                      [:div.transcripcion
                       [:h3 "Transcripción fonética del texto"]
                       [:span.ipa.is-size-5 "[meBolBÔ/iBÔaUnémbREnunifÕrmeDeport×roIDeGwarDjÒn/kßNgÕraIkÕrto pÒlo`ånlamÒno\\loBe`ÔapErf×k<tamìªte`EnsufiGÖRaUmÒna/pERonÕ`EstÕIseG ÖRo/dekefw×RaDE¢tÕDopÒko]"] ]
                      [:div.endnote [:sup "1"] "Quiroga, Horacio. “El síncope blanco”."]]}))

(defn actividad
  "Chapter 5 actividades"
  []
   [:section.actividad.content
    [:h2 "ACTIVIDAD"]
    [:p	"Las siguientes palabras contienen ejemplos de coarticulación/asimilación. Para cada palabra, identifique el tipo (los tipos) de coarticulación/asimilación ejemplificado(s). ¿Cuál es el rasgo fonético que se asimila?"  [shared/media-icon :r (rfe/href :fonetica.routes/c5-r) :same-session]]
    [:ol.list.is-lower-alpha 
     [:li "tengo" (ipa "[tìNgo]")]

     [:li "inferior" (ipa "[ÞMfERjÕr]")]

     [:li "la boca" (ipa "[laBÕka]")]

     [:li "ocho" (ipa "[ÕÎo4]")]

     [:li "la dama" (ipa "[laDÒma]")]

     [:li "un baso" (ipa "[àmbÒso]")]

     [:li "taquito" (ipa "[takÔto4]")]

     [:li "un vago" (ipa "[àmbÒGo]")]

     [:li "banda" (ipa "[bæªda]")]

     [:li "peligroso" (ipa "[peliGrÕso]")]

     [:li "todos los días" (ipa "[tÕDo̧lo̧DÔas]")]

     [:li "ancho" (ipa "[æøÎo]")]

     [:li "hago" (ipa "[ÒGo]")]]])

(defn render
  "Chapter three view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 5 — La fonética articulatoria"
    "E-Resources"]
   [:div.content.chapter-5
    [:section.materiales
     [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
     [:ol
      [:li [shared/media-icon :video (shared/file "C5-M1-voice_onset_video_3.mp4")] "Video de alta velocidad de la acción de las cuerdas vocales demostrando distintos estados de la glotis."]
      [:li [shared/media-icon :video (shared/file "C5-M2-videoPaper-Blow-Side.mp4")] "Video demostrando el efecto de Bernoulli en dos papelitos."]
      [:li [shared/media-icon :video (shared/file "C5-M3-Vocal-Chord-Movement.mov.ff.mp4")] "Video de la vibración de las cuerdas vocales tomado con un laringoscopio de KayPENTAX."]
      [:li [shared/media-icon :video (shared/file "C5-M4-Flouroscopy.mov.ff.mp4")] "Video cinerradiográfico de la pronunciación de varias oraciones en español."]
      [:li[shared/media-icon :audio (shared/file "C5-M5.mp3")] "Sonidos orales"]
      [:li[shared/media-icon :audio (shared/file "C5-M6.mp3")] "Sonidos nasales"]
      [:li[shared/media-icon :audio (shared/file "C5-M7.mp3")] "Sonidos oronasales"]
      [:li[shared/media-icon :audio (shared/file "C5-M8.mp3")] "Consonantes oclusivas"]
      [:li[shared/media-icon :audio (shared/file "C5-M9.mp3")] "Consonantes fricativas"]
      [:li[shared/media-icon :audio (shared/file "C5-M10.mp3")] "Consonantes africadas"]
      [:li[shared/media-icon :audio (shared/file "C5-M11.mp3")] "Consonantes nasales"]
      [:li[shared/media-icon :audio (shared/file "C5-M12.mp3")] "Consonantes laterales"]
      [:li[shared/media-icon :audio (shared/file "C5-M13.mp3")] "Consonante vibrante simple"]
      [:li[shared/media-icon :audio (shared/file "C5-M14.mp3")] "Consonante vibrante múltiple"]
      [:li[shared/media-icon :ej (rfe/href :fonetica.routes/c5-ej15) :same-session] "Los lugares de articulación"]
      [:li[shared/media-icon :audio (shared/file "C5-M16.mp3")] "Consonantes bilabiales"]
      [:li[shared/media-icon :audio (shared/file "C5-M17.mp3")] "Consonantes labiodentales"]
      [:li[shared/media-icon :audio (shared/file "C5-M18.mp3")] "Consonantes interdentales"]
      [:li[shared/media-icon :audio (shared/file "C5-M19.mp3")] "Consonantes dentales"]
      [:li[shared/media-icon :audio (shared/file "C5-M20.mp3")] "Consonantes alveolares"]
      [:li[shared/media-icon :audio (shared/file "C5-M21.mp3")] "Consonantes palatales"]
      [:li[shared/media-icon :audio (shared/file "C5-M22.mp3")] "Consonantes velares"]
      [:li[shared/media-icon :audio (shared/file "C5-M23.mp3")] "Consonantes sonoras"]
      [:li[shared/media-icon :audio (shared/file "C5-M24.mp3")] "Consonantes sordas"]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c5-ej25) :same-session]
       [shared/media-icon :ej (rfe/href :fonetica.routes/c5-ej25) :same-session]
       "Cuadro fonético consonántico completo (para estudiar) y cuadro fonético en blanco (para practicar)."]
      [:li[shared/media-icon :audio (shared/file "C5-M26.mp3")] "Semiconsonantes"]
      [:li[shared/media-icon :audio (shared/file "C5-M27.mp3")] "Semivocales"]
      [:li[shared/media-icon :audio (shared/file "C5-M28.mp3")] "Vocales cerradas"]
      [:li[shared/media-icon :audio (shared/file "C5-M29.mp3")] "Vocales medias"]
      [:li[shared/media-icon :audio (shared/file "C5-M30.mp3")] "Vocal abierta"]
      [:li[shared/media-icon :audio (shared/file "C5-M31.mp3")] "Vocales anteriores"]
      [:li[shared/media-icon :audio (shared/file "C5-M32.mp3")] "Vocal central"]
      [:li[shared/media-icon :audio (shared/file "C5-M33.mp3")] "Vocales posteriores"]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c5-ej34) :same-session]
       [shared/media-icon :ej (rfe/href :fonetica.routes/c5-ej34) :same-session]
       "Cuadro fonético vocálico completo (para estudiar) y cuadro fonético en blanco (para practicar)."]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c5-ej35) :same-session] "Transcripciones fonéticas"]
      [:li[shared/media-icon :audio (shared/file "C5-M36.mp3")] "Ejemplo de facilidad de articulación: " (ipa "[Ô¸la]")]
      [:li[shared/media-icon :audio (shared/file "C5-M37.mp3")] "Ejemplo de separación perceptiva suficiente: " (ipa "[kÒRo] [kÒro]")]
      [:li[shared/media-icon :audio (shared/file "C5-M38.mp3")] "Ejemplos de coarticulación anticipante"]
      [:li[shared/media-icon :audio (shared/file "C5-M39.mp3")] "Ejemplos de coarticulación perseverante"]
      [:li[shared/media-icon :audio (shared/file "C5-M40.mp3")] "Ejemplos de coarticulación recíproca"]
      [:li[shared/media-icon :audio (shared/file "C5-M41.mp3")] "Ejemplos de coarticulación convergente"]]]
    [actividad]]
   
   [shared/footer]])
