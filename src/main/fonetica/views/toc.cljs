(ns fonetica.views.toc
  "Overall Table of Contents"
  (:require [reitit.frontend.easy :as rfe]))

(defn fonetica-title
  "Title Hero for the page"
  []
  [:section.hero
   [:h1.container.box.is-title
    "Manual de fonética y fonología españolas"]])

(defn toc-link
  "An li and href for a toc entry"
  [text href]
  [:li [:a {:href href} text]])

(defn render
  "front table of contents view"
  []
  [:main#toc.container
   [fonetica-title]
   [:section
    [:h4 "SECCIÓN I: Introducción"]
    [:ul 
     [toc-link "Capítulo 1 — La comunicación humana" (rfe/href :fonetica.routes/chapter-1)]
     [toc-link "Capítulo 2 — La lingüística" (rfe/href :fonetica.routes/chapter-2)]
     [toc-link "Capítulo 3 — La fonética y la fonología"
      (rfe/href :fonetica.routes/chapter-3)]
     [toc-link "Capítulo 4 — Sistemas de escritura"
      (rfe/href :fonetica.routes/chapter-4)]]]
   
   [:section
    [:h4 "SECCIÓN II: La fonética"]
    [:ul 
     [toc-link "Capítulo 5 — La fonética articulatoria"
      (rfe/href :fonetica.routes/chapter-5)]
     [toc-link "Capítulo 6 — La fonética acústica"
      (rfe/href :fonetica.routes/chapter-6)]
     [toc-link "Capítulo 7 — La fonética auditiva"
      (rfe/href :fonetica.routes/chapter-7)]]]
   
   [:section
    [:h4 "SECCIÓN III: La fonología"]
    [:ul 
     [toc-link "Capítulo 8 — La relación entre fonemas: la oposición y la neutralización"
      (rfe/href :fonetica.routes/chapter-8)]
     [toc-link "Capítulo 9 — La relación entre fonemas y alófonos: la distribución"
      (rfe/href :fonetica.routes/chapter-9)]
     [toc-link "Capítulo 10 — El posicionamiento y la secuencia de fonemas: la fonotáctica"
      (rfe/href :fonetica.routes/chapter-10)]]]
   
   [:section
    [:h4 "SECCIÓN IV: Los fonemas vocálicos y sus sonidos"]
    [:ul 
     [toc-link "Capítulo 11 — Los fonemas vocálicos"
      (rfe/href :fonetica.routes/chapter-11)]
     [toc-link "Capítulo 12 — Secuencias vocálicas"
      (rfe/href :fonetica.routes/chapter-12)]]]
   
   [:section
    [:h4 "SECCIÓN V: Los fonemas consonánticos y sus sonidos"]
    [:ul 
     [toc-link "Capítulo 13 — Los fonemas oclusivos"
      (rfe/href :fonetica.routes/chapter-13)]
     [toc-link "Capítulo 14 — Los fonemas fricativos y el fonema africado"
      (rfe/href :fonetica.routes/chapter-14)]
     [toc-link "Capítulo 15 — Los fonemas nasales"
      (rfe/href :fonetica.routes/chapter-15)]
     [toc-link "Capítulo 16 — Los fonemas laterales y vibrantes"
      (rfe/href :fonetica.routes/chapter-16)]
     [toc-link "Capítulo 17 — Secuencias consonánticas"
      (rfe/href :fonetica.routes/chapter-17)]]]
   
   [:section
    [:h4 "SECCIÓN VI: Los elementos suprasegmentales"]
    [:ul 
     [toc-link "Capítulo 18 — La sílaba y el silabeo"
      (rfe/href :fonetica.routes/chapter-18)]
     [toc-link "Capítulo 19 — El acento"
      (rfe/href :fonetica.routes/chapter-19)]
     [toc-link "Capítulo 20 — La duración, el ritmo y el énfasis"
      (rfe/href :fonetica.routes/chapter-20)]
     [toc-link "Capítulo 21 — La entonación"
      (rfe/href :fonetica.routes/chapter-21)]]]
   ];; MAIN
  )
