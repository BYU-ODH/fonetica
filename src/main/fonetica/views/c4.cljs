(ns fonetica.views.c4
  "Chapter 4"
  (:require [fonetica.views.components.shared :as shared :refer [ipa femp]]
            [reitit.frontend.easy :as rfe]))

(defn render-m1
  "Chapter 4 m1 Sumerian"
  []
  [:main.c4.materiales.content
   [shared/fonetica-title "Texto en cuneiforme sumeria"]
   [:p "La frase de abajo viene de una inscrip­ción en unos ladri­llos de un tem­plo sumerio que dice que Ha­murabi (rey de Babilonia) era el rey para dios sobre la tierra." [:sup "1"] " Se puede ver que el nombre se representa mediante distintos símbolos para cada sílaba."]
   [:img {:src (shared/file "C4-M1-Sumerian-inscription.jpg")}]
   [:p.endnote {:style  {"marginTop" "2em"}} [:sup "1"] " L. W. King, The Letter and Inscriptions of Hammurabi (London: Luzac, 1898) vol. 1, p. 124, 183."]])

(defn render-m2
  "Chapter 4 m2 Egyptian"
  []
  [:main.c4.materiales.content
   [shared/fonetica-title "Texto en jeroglíficos egipcios"]
   [:p "El siguiente ejemplo textual" [:sup "1"] " contiene la aplicación al egipcio de la innovación sumeria de los dos tipos de determinativos. El primer tipo es el " [:strong "determinativo semántico"]", que sirve para precisar el significado de un logograma anterior. En el ejemplo de abajo, el logograma , que representa ‘timón’, llega a significar ‘voz’ cuando le sigue el determinativo semántico ‘hombre’. De esta manera, ciertos símbolos compuestos llegaron a representar conceptos específicos."] ;; TODO missing the character after "logograma"
   [:img {:src (shared/file "C4-M2-Egyptian-writing.jpg")}] 
   [:p.endnote {:style  {"margin-top" "2em"}} [:sup "1"] " W. V. Davies, " [:em "Egyptian Hieroglyphs"] " (London: British Museum, 1987), p. 43."]])

(defn render-m3
  "Chapter 4 m3 Chinese"
  []
  [:main.c4.materiales.content
   [shared/fonetica-title "Texto en caracteres chinos"]
   [:p "Como indica el ejemplo textual abajo," [:sup "1"] " la escritura del chino se orienta de arriba a abajo en columnas que se leen de derecha a izquierda. El texto, que viene de las analectas de Confucio, se traduce como: “El maestro dijo: ‘Para el lenguaje, solo se requiere comunicar el significado’.” Cada carácter del texto se acompaña de su significado, de su transliteración y de su transcripción fonética en mandarino."]
   [:img {:src (shared/file "C4-M3-Chinese-writing.jpg")}]
   [:p.endnote {:style {"margin-top" "2em"}} [:sup "1"] " James Legge, " [:em "The Chinese Classics"] " (Hong Kong: Hong Kong University Press, 1960) vol. 1 (" [:em "Confucian Analects"] "), p. 305."]])

(defn render-m4
  "Chapter 4 m4 Mediterranean"
  []
  [:main.c4.materiales.content
   [shared/fonetica-title "Texto en escritura silábica mediterránea"]
   [:p "El siguiente ejemplo textual" [:sup "1"] " con su transliteración y traducción, viene de una figurilla de bronce descubierta en Sevilla:"]
   [:img {:src (shared/file "C4-M4-mediterranea.png")}]
   [:p "Esta inscripción fenicia-ibérica data de la primera mitad del siglo ocho antes de Jesucristo. El texto completo alaba a la diosa Astarte por haber oído las oraciones dirigidas a ella."]
   [:p.endnote {:style {"margin-top" "2em"}} [:sup "1"] " Frank Moore Cross, Jr., “The Old Phoenician Inscription from Spain Dedicated to Hurrián
Astarte,” " [:em "Harvard Theological Review"] " 64 (1971): 189-90."]])

(defn render-m5
  "Chapter 4 m5 Japanese"
  []
  [:main.c4.materiales.content
   [shared/fonetica-title "Los silabarios japoneses"]
   [:img {:src (shared/file "C4-M5-japanese.png")}]])

(defn render-m6
  "Chapter 4 m6 Arabic"
  []
  [:main.c4.materiales.content
   [shared/fonetica-title "El alfabeto árabe"]
   [:p "El alfabeto árabe tiene distintos alógrafos para cada sonido. El grafema que se emplea en
cada circunstancia depende de si aparece en posición inicial de palabra, en posición media de
palabra, en posición final de palabra o en posición aislada. Hay que recordar que se escribe el
árabe de derecha a izquierda."]

   (let [rows [["'alif" "ا" "ﺎ" "ﺎ" "ا" "[?]"]
               ["baa'" "ﺑ" "ﺒ" "ﺐ" "ب" "[b]"]
               ["taa'" "ﺗ" "ﺘ" "ﺖ" "ت" "[t]"]
               ["thaa'" "ﺛ" "ﺜ" "ﺚ" "ث" "[T]"]
               ["jiim" "ﺟ" "ﺠ" "ﺞ" "ج" "[Ë Z F g]"]
               ["Haa'" "ﺣ" "ﺤ" "ﺢ" "ح" "[• —]"]
               ["khaa'" "ﺧ" "ﺨ" "ﺦ" "خ" "[X x]"]
               ["daal" "د" "ﺪ" "ﺪ" "د" "[d]"]
               ["dhaal" "ذ" "ﺬ" "ﺬ" "ذ" "[D]"]
               ["raa'" "ر" "ﺮ" "ﺮ" "ر" "[r rÂ]"]
               ["zaay" "ز" "ﺯ" "ﺯ" "ز" "[z]"]
               ["siin" "ﺳ" "ﺴ" "ﺲ" "س" "[s]"]
               ["shiin" "ﺷ" "ﺸ" "ﺶ" "ش" "[S]"]
               ["Saad" "ﺻ" "ﺼ" "ﺺ" "ص" "[sÂ]"]
               ["Daad" "ﺿ" "ﻀ" "ﺾ" "ض" "[dÂ zÂ]"]
               ["Taa'" "ﻃ" "ﻄ" "ﻂ" "ط" "[tÂ]"]
               ["dhaa'" "ﻇ" "ﻈ" "ﻆ" "ظ" "[DÂ]"]
               ["ain" "ﻋ" "ﻌ" "ﻊ" "ع" "[Â]"]
               ["ghain" "ﻏ" "ﻐ" "ﻎ" "غ" "[³ G]"]
               ["faa'" "ﻓ" "ﻔ" "ﻒ" "ف" "[f]"]
               ["qaaf" "ﻗ" "ﻘ" "ﻖ" "ق" "[q]"]
               ["kaaf" "ﻛ" "ﻜ" "ﻚ" "ك" "[k]"]
               ["laam" "ﻟ" "ﻠ" "ﻞ" "ل" "[l œ]"]
               ["miim" "ﻣ" "ﻤ" "ﻢ" "م" "[m]"]
               ["nuun" "ﻧ" "ﻨ" "ﻦ" "ن" "[n]"]
               ["haa'" "ﻫ" "ﻬ" "ﻪ" "ه" "[h]"]
               ["waaw" "و" "ﻮ" "ﻮ" "و" "[w]"]
               ["yaa'" "ﻳ" "ﻴ" "ﻲ" "ي" "[j]"]]]
     [:table.table.ftable.is-bordered
      [:thead
       [:tr
        [:td "Nombre"]
        [:td "Posición Inicial"]
        [:td "Posición Media"]
        [:td "Posición Final"]
        [:td "Posición Aislada"]
        [:td "Valor fonético"]]]
      (into [:tbody]
            (for [[a b c d e f] rows]
              [:tr [:td a] [:td b] [:td c] [:td d] [:td e] [:td.ipa f]]))])])

(defn render-m7
  "Chapter 4 m7 Phonecian"
  []
  [:main.c4.materiales.content
   [shared/fonetica-title "El alfabeto fenicio"]
   [:p "El alfabeto fenicio consistía en 22 grafemas consonánticos, que se presentan con una
correspondencia fonológica."]
   [:img {:src (shared/file "C4-M7-phonecian.png")}]])

(defn render-m8
  "Chapter 4 m8 Hebrew"
  []
  [:main.c4.materiales.content
   [:section.m8-1
    [shared/fonetica-title "El alfabeto hebraico"]
    [:p "El alfabeto hebreo consistía en 22 grafemas consonánticos, que se presentan con una
correspondencia fonológica."]
    [:img {:src (shared/file "C4-M8-hebrew1.png")}]]
   [:section.m8-2
    [shared/fonetica-title "Texto en el alfabeto hebraico"]
    [:p "El siguiente ejemplo textual, con su transliteración y traducción, es de un poema de
Todros Abulafia, quien vivió en Toledo.1 En el poema, que data del siglo XIII, se emplea el
alfabeto consonántico hebraico para representar el español."]
    [:img {:src (shared/file "C4-M8-hebrew2.png")}]]
   [:p.endnote {:style {"margin-top" "2em"}} [:sup "1"] "L. P. Harvey, " [:em "Hispano-Arabic Strophic Poetry: Studies by Samuel Miklos Stern"] "(Oxford: Clarendon Press, 1974) p. 146."]])

(defn render-m9
  "Chapter 4 m9 Iberian"
  []
  [:main.c4.materiales.content
   [:section.m9-1
    [shared/fonetica-title "Texto griego"]
    [:p "La lengua griega se usaba como un medio de transmisión de la cultura griega por todo el
mediterráneo; esto incluye la Península Ibérica. Mucho de lo que se sabe de la Iberia alrededor
del tiempo de Jesucristo se debe al geógrafo griego Estrabón, quien escribió sobre todo el mundo
conocido por los griegos y romanos de esa época. El siguiente texto introduce sus comentarios
sobre la Iberia" [:sup "1"]]
    [:p "Πρῶτον δὲ μέρος αὐτῆς ἐστι τὸ ἑσπέριον, ὡς ἔφαμεν, ἡ ᾽Ιβηρία."]
    [:p "Prōton de meros autēs esti to Hesperion, hōs efamen, hē Ibēria."]
    [:p "Como decía, la primera parte de Europa es la occidental, a saber, Iberia."]
    ]
   [:p.endnote {:style {"margin-top" "2em"}} [:sup "1"] "L. P. Harvey, " [:em "Hispano-Arabic Strophic Poetry: Studies by Samuel Miklos Stern"] "(Oxford: Clarendon Press, 1974) p. 146."]])

(defn render-m10
  "Chapter 4 m10 Latin"
  []
  [:main.c4.materiales.content
   [:section.m10
    [shared/fonetica-title "Texto en latín en el afabeto romano"]
    [:p "Lo que asombra del alfabeto romano es su extensión geográfica y lingüística. El latín se impuso desde las islas británicas en el oeste hasta Babilonia en el este y desde el Río Danubio en el norte hasta el norte de África en el sur. El latín, y por consecuencia su alfabeto, se usaron en la península Ibérica desde fines del siglo tres antes de Jesucristo. La Península contribuyó mucho al Imperio Romano, incluso filósofos, escritores y emperadores. Entre sus escritores más famosos se halla Séneca, natural de Córdoba, cuya observación filosófica en cuanto a la importancia del silencio para el estudio es:" [:sup "1"]]
    [:div.has-text-centered
     [:p.is-size-5 "PEREAM SI EST TAM NECESSARIUM QUAM VIDETUR SILENTIUM IN STUDIA SEPOSITO"]
     [:p.is-size-6 "Me moriría si el silencio fuera tan esencial para el estudio en reclusión como se le ve."]]]
   [:p.endnote {:style {"margin-top" "2em"}} [:sup "1"] "Seneca," [:em "Annaei Senecae ad Lucilium Epistulae Morales"] "(Oxford: Oxford University Press, 1965) vol. 1, p. 147."]])

(defn render-m11
  "Chapter 4 m11 cyrillic"
  []
  [:main.c4.materiales.content
   [:section.m11
    [shared/fonetica-title "Texto en ruso en el alfabeto cirílico"]
    [:p "Un ejemplo textual del ruso, con su transliteración y traducción,  es la siguiente cita de la novela «Анна Каренина» (Anna Karenina), del escritor Лев Николаевич Толстой (Lev NikolaevicÏ Tolstoj)." [:sup "1"]]
    [:div.has-text-centered
     [:p.is-size-5 "Все счастливые семьи похожи друг на друга, каждая несчастливая семья
несчастлива по-своему."]
     [:p.is-size-6 "Vse sčastlivye semji pokhoži drug na druga, každaya nesčastlivaya semja nesčastliva po-svoemu."
      [:br]
      "Todas las familias felices se parecen, toda familia infeliz lo es por sus propios motivos."]]]
   [:p.endnote {:style {"margin-top" "2em"}} [:sup "1"] "Лев Николаевич Толстой, " [:em "Анна Каренина"] " (New York: International University Press, 1900) p. 7."]])

(defn render
  "Chapter four view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 4 — Sistemas de escritura"
    "E-Resources"]
   [:div.content.chapter-4
    [:section.materiales
     [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
     [:ol 
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c4-m1) :same-session] "Texto en cuneiforme sumeria."]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c4-m2) :same-session] "Texto en jeroglíficos egipcios."]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c4-m3) :same-session] "Texto en caracteres chinos."]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c4-m4) :same-session] "Texto en escritura silábica mediterránea."]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c4-m5) :same-session] "Los silabarios japoneses."]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c4-m6) :same-session] "El alfabeto árabe."]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c4-m7) :same-session] "El alfabeto fenicio."]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c4-m8) :same-session] "Alfabeto y texto hebraicos."]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c4-m9) :same-session] "Texto griego."]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c4-m10) :same-session] "Texto en latín en el alfabeto romano."]
      [:li [shared/media-icon :text (rfe/href :fonetica.routes/c4-m11) :same-session] "Texto en ruso en el alfabeto cirílico."]]]]
   
   [shared/footer]])
