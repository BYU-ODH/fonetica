(ns fonetica.views.c10
  "Chapter 10"
  (:require [fonetica.views.components.shared :as shared :refer [ipa red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas Activity 1"
  []
  [:section.ra1
   [:li.respuestas-a1 "Dé cinco palabras que ejemplifiquen cada una de las siguientes secuencias fonotácticas.
Puede usar el Cuadro 10.8 del libro como referencia. " [:span.red "Las respuestas variarán. Para cada uno
se da un ejemplo."]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li "#CV " [red "ma"] "pa"]
       [:li "#CCV " [red "tre"] "s"]
       [:li "$CV pa" [red "la"] "bras"]
       [:li "$CCV encuent" [red "ra"]]]
      [:div.r
       [:li "VC# césp" [red "ed"]]
       [:li "VCC# " [red "(no es posible)"]]
       [:li "VC$ h" [red "as"] "ta"]
       [:li "VCC$ t" [red "rans"] "porte"]
       ]]]]])

(defn ra2
  "Respuestas Activity 2"
  []
  [:section.ra2
   [:li.respuestas-a2 "Identifique la estructura silábica interna y la división silábica de las siguientes palabras según el modelo del Cuadro 10.10 del libro."
    [:ol.list.is-lower-alpha
     [:li "abstracto"
      [:img {:src (shared/file "C10-Respuestas-2a.png")}]]
     [:li "catástrofe"
      [:img {:src (shared/file "C10-Respuestas-2b.png")}]]
     [:li "clausura"
      [:img {:src (shared/file "C10-Respuestas-2c.png")}]]
     [:li "contiene"
      [:img {:src (shared/file "C10-Respuestas-2d.png")}]]
     [:li "riñeron"
      [:img {:src (shared/file "C10-Respuestas-2e.png")}]]
     [:li "exacto"
      [:img {:src (shared/file "C10-Respuestas-2f.png")}]]
     [:li "mexicano"
      [:img {:src (shared/file "C10-Respuestas-2g.png")}]]
     [:li "perspicaz"
      [:img {:src (shared/file "C10-Respuestas-2h.png")}]]]]])

(defn ra3
  "Respuestas Activity 3"
  []
  [:section.ra3
   [:li.respuestas-a3 "Demuestre cómo la estructura silábica afecta la realización de las vocales cerradas /i u/ sea en el ataque silábico, el núcleo silábico, o la coda silábica."
    [:div.red "En el ataque silábico /i u/ se realizan como semiconsonantes: " [ipa "[j w]"] "."]
    [:div.red "En el núcleo silábico /i u/ se realizan como vocales cerradas/altas: " [ipa "[i u]"] "."]
    [:div.red "En la coda silábica /i u/ se realizan como semivocales: " [ipa "[I U]"] "."]]])

(defn ra4
  "Respuestas Activity 4"
  []
  [:section.ra4
   [:li.respuestas-a4 "Demuestre cómo la estructura silábica afecta la realización de las consonantes nasales y vibrantes sea en el ataque silábico o la coda silábica."
    [:div.red "En el ataque silábico los fonemas nasales se oponen: /" [ipa "n"] "/ - /" [ipa "m"] "/ - /" [ipa "¦"] "/"]
    [:div.red "En la coda silábica los fonemas nasales se neutralizan: /N/."]]])

(defn ra5
  "Respuestas Activity 5"
  []
  [:section.ra5
   [:li.respuestas-a5 "Invente cinco palabras para ejemplificar el concepto de laguna accidental y otras cinco para el concepto de laguna sistemática."
    [:div.red "Las respuesta pueden variar; aquí se presentan unos ejemplos."]
    [:div.red "Lagunas accidentales: trampo, blan, siño, charo, alco."]
    [:div.red "Lagunas sistemáticos: sreír, mbole, serrap, pans, tlan."]]])

(defn render-respuestas
  "Chapter 10 respuestas"
  []
  [:main.c10.respuestas.content
   [shared/fonetica-title "Capítulo 10 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]
     [ra5]]]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:li "ACTIVIDADES
1. Dé cinco palabras que ejemplifiquen cada una de las siguientes secuencias fonotácticas.
Puede usar el Cuadro 10.8 del libro como referencia." [shared/media-icon :r (rfe/href :fonetica.routes/c10-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li "#CV"]
       [:li "#CCV"]
       [:li "$CV"]
       [:li "$CCV"]]
      [:div.r
       [:li "VC#"]
       [:li "VCC#"]
       [:li "VC$"]
       [:li "VCC$"]]]]]])

(defn a2
  "Actividades 2"
  []
  [:section.a2
   [:li "Identifique la estructura silábica interna y la división silábica de las siguientes palabras según el modelo del Cuadro 10.10 del libro." [shared/media-icon :r (rfe/href :fonetica.routes/c10-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li "abstracto"]
       [:li "catástrofe"]
       [:li "clausura"]
       [:li "contiene"]]
      [:div.r
       [:li "riñeron"]
       [:li "exacto"]
       [:li "mexicano"]
       [:li "perspicaz"]]]]]])

(defn a3
  "Actividades 3"
  []
  [:section.a3
   [:li "Demuestre cómo la estructura silábica afecta la realización de las vocales cerradas /i u/ sea en el ataque silábico, el núcleo silábico, o la coda silábica." [shared/media-icon :r (rfe/href :fonetica.routes/c10-r) :same-session] ]])

(defn a4
  "Actividades 4"
  []
  [:section.a4
   [:li "Demuestre cómo la estructura silábica afecta la realización de las consonantes nasales y vibrantes sea en el ataque silábico o la coda silábica." [shared/media-icon :r (rfe/href :fonetica.routes/c10-r) :same-session] ]])

(defn a5
  "Actividades 5"
  []
  [:section.a5
   [:li "Invente cinco palabras para ejemplificar el concepto de laguna accidental y otras cinco para el concepto de laguna sistemática." [shared/media-icon :r (rfe/href :fonetica.routes/c10-r) :same-session] ]])

(defn actividades
  "Chapter 10 actividades"
  []
   [:section.actividades.content
    [:h2 "ACTIVIDADES"]
    [:ol.actividades
     [a1]
     [a2]
     [a3]
     [a4]
     [a5]]])

(defn render
  "Chapter ten view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 10 — El posicionamiento y la secuencia de fonemas: la fonotáctica"
    "E-Resources"]
   [:div.content.chapter-10
    [actividades]]
   
   [shared/footer]])
