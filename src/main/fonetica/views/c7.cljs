(ns fonetica.views.c7
  "Chapter 7"
  (:require [fonetica.views.components.shared :as shared :refer [ipa]]
            [reitit.frontend.easy :as rfe]))

(defn render-respuestas
  "Chapter 7 respuestas"
  []
  [:main.c7.respuestas.container.content
   [shared/fonetica-title "Capítulo 7 — Respuestas a las actividades"]
   [:section.respuestas1
    [:div "1. Hay cuatro componentes que se emplean en la audición: el oído exterior, el oído medio, el oído interno y el sistema nervioso. Identifique el componente en que se encuentran los siguientes elementos."]
    [:div.grid-center     
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li "oído exterior"]
       [:li "oído exterior/oído medio"]
       [:li "oído medio"]
       [:li "oído medio/ oído interior"]
       [:li "oído interior"]
       [:li "oído interior/sistema nervioso"]
       [:li "oído medio"]
       [:li "oído interior"]]
      [:div.r
       [:li "oído exterior"]
       [:li "oído interior"]
       [:li "sistema nervioso"]
       [:li "oído interior"]
       [:li "oído medio"]
       [:li "oído interior"]
       [:li "sistema nervioso"]
       [:li "oído interior"]]]]]]

   [:section.respuestas2
    [:div "2. Complete el siguiente cuadro referente a los componentes del proceso de la audición."]
    (let [rows
          [["el oído exterior"
            "conducir la onda sonora a la membrana timpánica y amplificarla"
            "transmisión acústica a través del aire"]
           ["el oído medio"
            "amplificar la energía del sonido"
            "transmisión mecánica a través de la cadena osicular"]
           ["el oído interno"
            "dividir la onda compuesta para reconocer en qué frecuencias se encuentra la energía"
            "transmisión hidráulica a través del líquido de la cóclea"]
           ["el sistema nervioso"
            "transmitir el espectro acústico del sonido hacia el cerebro"
            "transmisión neural a través del nervio auditivo"]]]
      
      [:table.table.ftable.is-bordered
       [:thead [:tr
                [:th "Componente"]
                [:th "¿Cuál es su función?"]
                [:th "¿De qué forma se transmite la energía?"]]]
       (into [:tbody]
             (for [[a b c] rows]
               [:tr [:td a] [:td b] [:td c]]))])]

   [:section.respuestas3
    [:div "3. Complete el siguiente cuadro sobre el proceso de la percepción:"]
    (let [rows
          [["Segmentación"
            "Dividir una cadena fluida de impulsos en segmentos"
            "Segmentos discretos a ser identificados"
            "Número infinito de sonidos"
            [:span "La palabra {nave}" [shared/media-icon :audio (shared/file "C7-R1.mp3")] " contiene cuatro segmentos"]]
           ["Identificación"
            "Emparejar el segmento con un sonido o alófono"
            "Una secuencia de sonidos identificados"
            "Número finito de alófonos"
            [:span "La palabra {nave} es una secuencia de cuatro alófonos: " (ipa "[nÒBe]")]]
           ["Sistematización"
            "Emparejar el alófono con su fonema"
            "Una secuencia de fonemas"
            "Número finito y reducido de fonemas"
            [:span "La palabra {nave} es una secuencia de cuatro fonemas: " (ipa "/nÒbe/")]]]]
      
      [:table.table.ftable.is-bordered
       [:thead [:tr
                [:th "Etapa"]
                [:th "Proceso"]
                [:th "Producto lingüístico"]
                [:th "Número y tipo de elementos"]
                [:th "Ejemplo"]]]
       (into [:tbody]
             (for [[a b c d e] rows]
               [:tr [:td a] [:td b] [:td c] [:td d] [:td e]]))])]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:p "1. Hay cuatro componentes que se emplean en la audición: el oído exterior, el oído medio, el
oído interno y el sistema nervioso. Identifique el componente en que se encuentran los
siguientes elementos."
    [shared/media-icon :r (rfe/href :fonetica.routes/c7-r) :same-session]]
   [:div.grid-center     
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li "canal auditivo"]
       [:li "membrana timpánica"]
       [:li "martillo"]
       [:li "ventana oval"]
       [:li "escala timpánica"]
       [:li "nervio auditivo"]
       [:li "estribo"]
       [:li "órgano de Corti"]]
      [:div.r
       [:li "oreja"]
       [:li "membrana basilar"]
       [:li "área de Wernicke"]
       [:li "fibras ciliadas"]
       [:li "yunque"]
       [:li "membrana tectoria"]
       [:li "corteza auditiva central"]
       [:li "escala media"]]]]]])

(defn a2
  "Actividades 2"
  []
  [:section.a2
   [:p "2. Complete el siguiente cuadro referente a los componentes del proceso de la audición."
    [shared/media-icon :r (rfe/href :fonetica.routes/c7-r) :same-session]]
   (let [rows ["el oído exterior"
               "el oído medio"
               "el oído interno"
               "el sistema nervioso"]]
     [:table.table.ftable.is-bordered
      [:thead [:tr
               [:th "Componente"]
               [:th "¿Cuál es su función?"]
               [:th "¿De qué forma se transmite la energía?"]]]
      (into [:tbody]
            (for [s rows]
              [:tr [:td s][:td][:td]]))])])

(defn a3
  "Actividades 3"
  []
  [:section.a3
   [:p "3. Complete el siguiente cuadro sobre el proceso de la percepción:"
    [shared/media-icon :r (rfe/href :fonetica.routes/c7-r) :same-session]]
   (let [rows ["Segmentación"
               "Identificación"
               "Sistematización"]]
     [:table.table.ftable.is-bordered
      [:thead [:tr
               [:th "Etapa"]
               [:th "Proceso"]
               [:th "Producto lingüístico"]
               [:th "Número y tipo de elementos"]
               [:th "Ejemplo"]]]
      (into [:tbody]
            (for [s rows]
              [:tr [:td s][:td][:td][:td][:td]]))])])

(defn actividades
  "Chapter 7  actividades"
  []
   [:section.actividades.content
    [:h2 "ACTIVIDADES"]
    [a1]
    [a2]
    [a3]])

(defn render
  "Chapter seven view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 7 — La fonética acústica"
    "E-Resources"]
   [:div.content.chapter-7
    [actividades]]
   
   [shared/footer]])
