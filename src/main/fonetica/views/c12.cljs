(ns fonetica.views.c12
  "Chapter 12"
  (:require [fonetica.views.components.shared :as shared :refer [ipa femp]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas Activity 1"
  []
  [:section.ra1
   [:li "Identifique la solución fonética preferida (fusión, diptongo/triptongo, sinéresis/sinalefa, hiato) y haga la transcripción fonética para las secuencias vocálicas en las siguientes palabras o frases."
    [:ol.list.is-lower-alpha
     [:div.line-item.grid3
      [:li "com" [femp "ía"] ""]
      [:div.red "hiato natural"]
      [:div.red.ipa "[komÔa]"]]
     [:div.line-item.grid3
      [:li "p" [femp "ea"] "tón"]
      [:div.red "sinéresis"]
      [:div.red.ipa "[pe`atén]"]]
     [:div.line-item.grid3
      [:li "b" [femp "oi"] "na"]
      [:div.red "diptongo decreciente"]
      [:div.red.ipa "[bÕIna]"]]
     [:div.line-item.grid3
      [:li "roc" [femp "iái"] "s"]
      [:div.red "triptongo"]
      [:div.red.ipa "[rosjÒIs]"]]
     [:div.line-item.grid3
      [:li "h" [femp "ue"] "co"]
      [:div.red "diptongo creciente"]
      [:div.red.ipa "[wÓko]"]]
     [:div.line-item.grid3
      [:li "cr" [femp "ee"] "s"]
      [:div.red "fusión"]
      [:div.red.ipa "[krÓs]"]]
     [:div.line-item.grid3
      [:li "alcanz" [femp "a a "] "s" [femp "u a"] "migo"]
      [:div.red "fusión; diptongo"]
      [:div.red.ipa "[alkÒnsaswamÔGo]"]]
     [:div.line-item.grid3
      [:li "ven" [femp "ía a "] "las c" [femp "ua"] "tro"]
      [:div.red "hiato; fusión; diptongo"]
      [:div.red.ipa "[benÔalaskwÒtRo]"]]
     [:div.line-item.grid3
      [:li "l" [femp "o e"] "senc" [femp "ia"] "l"]
      [:div.red "sinalefa; diptongo"]
      [:div.red.ipa "[lo`esensjÒl]"]]
     [:div.line-item.grid3
      [:li "v" [femp "a a i"] "r"]
      [:div.red "fusión; sinalefa"]
      [:div.red.ipa "[bÒ`Ôr]"]]
     [:div.line-item.grid3
      [:li "hac" [femp "ia e"] "l" [femp " o"] "r" [femp "ie"] "nte"]
      [:div.red "diptongo; sinalefa; diptongo"]
      [:div.red.ipa "[asja`Elorjìªte]"]]
     [:div.line-item.grid3
      [:li "s" [femp "ie"] "t" [femp "e u o"] "cho"]
      [:div.red "diptongo; hiato; diptongo"]
      [:div.red.ipa "[sj×tewÕÊo]"]]
     [:div.line-item.grid3
      [:li "d" [femp "ue"] "l" [femp "e e "] "hincha"]
      [:div.red "diptongo; fusión; sinalefa"]
      [:div.red.ipa "[dwÓle`ÔøÊa]"]]
     [:div.line-item.grid3
      [:li "com" [femp "e y "] "s" [femp "e "] "va"]
      [:div.red "diptongo decreciente"]
      [:div.red.ipa "[kÕmeIseBÒ]"]]
     [:div.line-item.grid3
      [:li "averig" [femp "üéi"] "s"]
      [:div.red "triptongo"]
      [:div.red.ipa "[aBERiGwÓIs]"]]
     [:div.line-item.grid3
      [:li "él" [femp " o"] "d" [femp "ia a e"] "lla"]
      [:div.red "diptongo; fusión; sinalefa"]
      [:div.red.ipa "[×lÕDja`ÓJa]"]]
     [:div.line-item.grid3
      [:li "energ" [femp "ía au"] "stral"]
      [:div.red "hiato; fusión; diptongo"]
      [:div.red.ipa "[enERxÔaUstRÒl]"]]
     [:div.line-item.grid3
      [:li "n" [femp"ihi"] "lista"]
      [:div.red "fusión"]
      [:div.red.ipa "[nilÔsta]"]]
     [:div.line-item.grid3
      [:li "h" [femp "ie"] "dra"]
      [:div.red "vocal simple"]
      [:div.red.ipa "[ÌÓDRa]"]]
     [:div.line-item.grid3
      [:li "f" [femp "ia"] "r" [femp "ía"] ""]
      [:div.red "diptongo; hiato natural"]
      [:div.red.ipa "[fjaRÔa]"]]
     [:div.line-item.grid3
      [:li "est" [femp "á au"] "sente"]
      [:div.red "fusión; diptongo"]
      [:div.red.ipa "[EstÒUsìªte]"]]
     [:div.line-item.grid3
      [:li "est" [femp "á a"] "gr" [femp "io"] ""]
      [:div.red "fusión; diptongo"]
      [:div.red.ipa "[EstÒ:GRjo]"]]
     [:div.line-item.grid3
      [:li "t" [femp "ú u"] "sas mucho"]
      [:div.red "fusión"]
      [:div.red.ipa "[tÖ:sa¸mÖÊo]"]]
     [:div.line-item.grid3
      [:li "habl" [femp "ó o"] "tro"]
      [:div.red "fusión"]
      [:div.red.ipa "[aBlÕ:tRo]"]]]]])

(defn ra2
  "Respuestas Activity 2"
  []
  [:section.ra2
   [:li "Indique la solución fonética preferida para las siguientes descripciones de secuencias vocálicas. Dé por lo menos cuatro ejemplos para cada fenómeno, dando tanto la ortografía como la transcripción fonética."
    (let [rows [["dos vocales homólogas"
                 "C12-A2-1ss.png"
                 [:div "fusión"]
                 [:div 
                  [:div "alcohol " [ipa "[alkÕ:l]"]]
                  [:div "habla a los otros " [ipa "[ÒBlalosÕtros]"]]
                  [:div "etc."]]]
                
                ["una vocal alta y átona seguida de otra vocal" 
                 "C12-A2-2ss.png"
                 [:div "diptongo crecient"]
                 [:div
                  [:div "epiojo " [ipa "[pjÕxo]"]]
	          [:div "puente " [ipa "[pwìªte]"]]
	          [:div "etc."]]]
                
                ["una vocal no alta seguida de una vocal alta y átona" 
                 "C12-A2-3ss.png"
                 [:div "diptongo decreciente"]
                 [:div
                  [:div "ara y planta " [ipa "[ÒRaIplæªta]"]]
	          [:div "maguey " [ipa "[maGÓI]"]]
	          [:div "etc."]]]
                
                ["una vocal alta y átona seguida de una vocal no alta seguida de una vocal alta y átona" 
                 "C12-A2-4ss.png"
                 [:div "triptongo"]
                 [:div
                  [:div "apreciáis " [ipa "[apResjÒIs]"]]
	          [:div "tribu aislada " [ipa "[tRÔBwÒI¸lÒDa]"]]
	          [:div "etc."]]]
                
                ["dos vocales no altas o una vocal no alta seguida de una vocal alta y tónica dentro de una sola palabra" 
                 "C12-A2-5ss.png"
                 [:div "sinéresis"]
                 [:div
                  [:div "apear " [ipa "[ape`Òr]"]]
	          [:div "poema " [ipa "[po`Óma]"]]
	          [:div "etc."]]]
                
                ["dos vocales no altas o una vocal no alta seguida de una vocal alta y tónica entre dos palabras" 
                 "C12-A2-6ss.png"
                 [:div "sinalefa"]
                 [:div
                  [:div "gana el partido " [ipa "[gÒna`ElpaRtÔDo]"]]
	          [:div "habla escocés " [ipa "[ÒBla`EskosÓs]"]]
	          [:div "etc."]]]
                ["una vocal alta y tónica seguida de otra vocal" "C12-A2-7ss.png"
                 [:div "hiato natural"]
                 [:div
                  [:div "todavía " [ipa "[toDaBÔa]"]]
	          [:div "búho " [ipa "[bÖo]"]]
	          [:div "etc."]]]]]
      [:table.table.ftable.is-bordered
       [:thead
        [:tr
         [:th "Secuencia Vocálica"]
         [:th "Fórmula"]
         [:th "Solución fonética"]
         [:th "Ejemplos"]]]
       (into [:tbody]
             (for [[w i s e] rows]
               [:tr [:td w]
                [:td [:img {:src (shared/file i)}]]
                [:td.red s]
                [:td.red e]]))])]])

(defn render-respuestas
  "Chapter 12 respuestas"
  []
  [:main.c12.respuestas.content
   [shared/fonetica-title "Capítulo 12 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]
     [ra2]]]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:li "Identifique la solución fonética preferida (fusión, diptongo/triptongo, sinéresis/sinalefa, hiato) y haga la transcripción fonética para las secuencias vocálicas en las siguientes palabras o frases."
    [shared/media-icon :r (rfe/href :fonetica.routes/c12-r) :same-session]
    [:ol.list.is-lower-alpha 
     [:li "com" [femp "ía"] ""]
     [:li "p" [femp "ea"] "tón"]
     [:li "b" [femp "oi"] "na"]
     [:li "roc" [femp "iái"] "s"]
     [:li "h" [femp "ue"] "co"]
     [:li "cr" [femp "ee"] "s"]
     [:li "alcanz" [femp "a a "] "s" [femp "u a"] "migo"]
     [:li "ven" [femp "ía a "] "las c" [femp "ua"] "tro"]
     [:li "l" [femp "o e"] "senc" [femp "ia"] "l"]
     [:li "v" [femp "a a i"] "r"]
     [:li "hac" [femp "ia e"] "l" [femp " o"] "r" [femp "ie"] "nte"]
     [:li "s" [femp "ie"] "t" [femp "e u o"] "cho"]
     [:li "d" [femp "ue"] "l" [femp "e e "] "hincha"]
     [:li "com" [femp "e y "] "s" [femp "e "] "va"]
     [:li "averig" [femp "üéi"] "s"]
     [:li "él" [femp " o"] "d" [femp "ia a e"] "lla"]
     [:li "energ" [femp "ía au"] "stral"]
     [:li "n" [femp"ihi"] "lista"]
     [:li "h" [femp "ie"] "dra"]
     [:li "f" [femp "ia"] "r" [femp "ía"] ""]
     [:li "est" [femp "á au"] "sente"]
     [:li "est" [femp "á a"] "gr" [femp "io"] ""]
     [:li "t" [femp "ú u"] "sas mucho"]
     [:li "habl" [femp "ó o"] "tro"]]]])

(defn a2
  "Actividades 2"
  []
  [:section.a2
   [:li "Indique la solución fonética preferida para las siguientes descripciones de secuencias vocálicas. Dé por lo menos cuatro ejemplos para cada fenómeno, dando tanto la ortografía como la transcripción fonética."
    [shared/media-icon :r (rfe/href :fonetica.routes/c12-r) :same-session]
    (let [rows [["dos vocales homólogas" "C12-A2-1ss.png"]
                ["una vocal alta y átona seguida de otra vocal" "C12-A2-2ss.png"]
                ["una vocal no alta seguida de una vocal alta y átona" "C12-A2-3ss.png"]
                ["una vocal alta y átona seguida de una vocal no alta seguida de una vocal alta y átona" "C12-A2-4ss.png"]
                ["dos vocales no altas o una vocal no alta seguida de una vocal alta y tónica dentro de una sola palabra" "C12-A2-5ss.png"]
                ["dos vocales no altas o una vocal no alta seguida de una vocal alta y tónica entre dos palabras" "C12-A2-6ss.png"]
                ["una vocal alta y tónica seguida de otra vocal" "C12-A2-7ss.png"]]]
      [:table.table.ftable
       [:thead
        [:tr
         [:th "Secuencia Vocálica"]
         [:th "Fórmula"]
         [:th "Solución fonética"]
         [:th "Ejemplos"]]]
       (into [:tbody]
             (for [[w i] rows]
               [:tr [:td w]
                [:td [:img {:src (shared/file i)}]]
                [:td] [:td]]))])]])

(defn actividades
  "Chapter 12 actividades"
  []
  [:section.actividades.content
   [:h2 "ACTIVIDADES"]
   [:ol.actividades
    [a1]
    [a2]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C12-M1.mp3")] "Ejemplos de fusión vocálica entre dos palabras (el Cuadro 12.1)."]
    [:li [shared/media-icon :audio (shared/file "C12-M2.mp3")] "Ejemplos de fusión/secuencia vocálica en la misma palabra."]
    [:li [shared/media-icon :audio (shared/file "C12-M3.mp3")] "Ejemplos de fusión/secuencia vocálica con tres vocales homólogas."]
    [:li [shared/media-icon :audio (shared/file "C12-M4.mp3")] "Ejemplos de golpe de glotis entre vocales homólogas en inglés."]
    [:li [shared/media-icon :audio (shared/file "C12-M5.mp3")] "Ejemplos de diptongos crecientes (el Cuadro 12.6)."]
    [:li [shared/media-icon :audio (shared/file "C12-M6.mp3")] "Ejemplos de diptongos crecientes con dos vocales altas."]
    [:li [shared/media-icon :audio (shared/file "C12-M7.mp3")] "Ejemplos de diptongos decrecientes (el Cuadro 12.9)."]
    [:li [shared/media-icon :audio (shared/file "C12-M8.mp3")] "Ejemplos de triptongos (el Cuadro 12.11)."]
    [:li [shared/media-icon :audio (shared/file "C12-M9.mp3")] "Contraste entre diptongos y vocales plenas en español."]
    [:li [shared/media-icon :audio (shared/file "C12-M10.mp3")] "Ejemplos de la aplicación de la regla del archifonema /J/."]
    [:li [shared/media-icon :audio (shared/file "C12-M11.mp3")] "Ejemplos de diptongación con las conjunciones {y} y {u} (el Cuadro 12.16)."]
    [:li [shared/media-icon :audio (shared/file "C12-M12.mp3")] "Ejemplos de la distinción entre vocales plenas y diptongos (el Cuadro 12.17)."]
    [:li [shared/media-icon :audio (shared/file "C12-M13.mp3")] "Ejemplos de sinéresis y sinalefa (el Cuadro 12.20)."]
    [:li [shared/media-icon :audio (shared/file "C12-M14.mp3")] "Ejemplos de sinéresis y sinalefa con dos vocales no altas (el Cuadro 12.21)."]
    [:li [shared/media-icon :audio (shared/file "C12-M15.mp3")] "Ejemplos de sinéresis y sinalefa con una vocal no alta y una vocal alta y tónica (el Cuadro 12.22)."]
    [:li [shared/media-icon :audio (shared/file "C12-M16.mp3")] "Ejemplos de sinéresis y sinalefa de la Fig. 12.23."]
    [:li [shared/media-icon :audio (shared/file "C12-M17.mp3")] "Ejemplos de combinaciones de tres o cuatro vocales en una sola sílaba."]
    [:li [shared/media-icon :audio (shared/file "C12-M18.mp3")] "Ejemplos de hiato natural."]
    [:li [shared/media-icon :audio (shared/file "C12-M19.mp3")] "La palabra {peor} pronunciada con diptongo, sinéresis y hiato."]
    [:li [shared/media-icon :audio (shared/file "C12-M20.mp3")] "Ejercicios de pronunciación: la fusión vocálica."]
    [:li [shared/media-icon :ej (shared/file "C12-M21.mp3")] "Ejercicios de pronunciación: los diptongos."]
    [:li [shared/media-icon :ej (shared/file "C12-M22.mp3")] "Ejercicios de pronunciación: los triptongos."]
    [:li [shared/media-icon :ej (shared/file "C12-M23.mp3")] "Ejercicios de pronunciación: la sinéresis/sinalefa."]
    [:li [shared/media-icon :ej (shared/file "C12-M24.mp3")] "Ejercicios de pronunciación: el hiato natural."]]])

(defn render
  "Chapter twelve view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 12 — Secuencias vocálicas"
    "E-Resources"]
   [:div.content.chapter-12
    [:section.e-resources
     [:h2 "E-Resources"]]
    [materiales]
    [actividades]]
   [shared/footer]])
