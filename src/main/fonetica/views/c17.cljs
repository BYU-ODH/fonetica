(ns fonetica.views.c17
  "Chapter 17"
  (:require [fonetica.views.components.shared :as shared :refer [ipa femp red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas activity 1"
  []
  [:section.ra1
   [:li "Las siguientes palabras/frases contienen secuencias consonánticas indicadas en negrilla.  Complete el siguiente cuadro con la transcripción de las palabras/frases e indique el tipo de secuencia consonántica."

    (let [headings ["Palabra/frase" "Transcripción"
                    [:div "Tipo de secuencia"
                     [:p.is-size-7 "(fusión, fusíón ambisilábica, grupo consonántico tautosilábico, secuencia consonántica heterosilábica)"]]]
          rows [[[:span "to" [:strong "s s"] "evera"] "[tÕseB×Ra]" "fusión"]
                [[:span "o" [:strong "bs"] "táculo"] "[oBstÒkulo]" "grupo consonántico tautosilábico"]
                [[:span "a" [:strong "st"] "illero"] "[astiJ×Ro]" "secuencia consonántica heterosilábica"]
                [[:span "pedico" [:strong "j j"] "ocoso"] "[peDikÕxokÕso]" "fusión"]
                [[:span "e" [:strong "ng"] "reír"] "[åNgReÔR]" "secuencia consonántica heterosilábica"]
                [[:span "a" [:strong "nd"] "ariego"] "[ÜªdaRjÓGo]" "secuencia consonántica heterosilábica"]
                [[:span "e" [:strong "br"] "io"] "[ÓBRjo]" "grupo consonántico tautosilábico"]
                [[:span "po" [:strong "str"] "imero"] "[po¸rim×Ro]" "secuencia consonántica heterosilábica"]
                [[:span "per" [:strong "sp"] "icaz"]"[pErspikÒs]" "secuencia consonántica heterosilábica"]
                [[:span "a" [:strong "pt"] "itud"] "[ap<titÖD]" "secuencia consonántica heterosilábica"]
                [[:span "in" [:strong "cr"] "ementar"] "[ÞNkRemåªtÒr]" "grupo consonántico tautosilábico"]
                [[:span "cu" [:strong "rs"] "i"] "[kÖRsi]" "secuencia consonántica heterosilábica"]
                [[:span "e" [:strong "l l"] "abio"] "[El:ÒBjo]" "fusión ambisilábica"]
                [[:span "u" [:strong "n n"] "ovio"] "[àn:ÕBjo]" "fusión ambisilábica"]
                [[:span "ca" [:strong "mp"] "estre"] "[kÜmp×stRe]" "secuencia consonántica heterosilábica"]
                [[:span "u" [:strong "n m"] "úsculo"] "[àm:Öskulo]" "fusión ambisilábica"]
                [[:span "a" [:strong "ds"] "cribir"] "[aDskRiBÔr]" "grupo consonántico tautosilábico"]
                [[:span "Ciuda" [:strong "d d"] "el Este"] "[sjuDÒDEl×ste]" "fusión"]
                [[:span "e" [:strong "x"] "imio"] "[EksÔmjo]" "secuencia consonántica heterosilábica"]
                [[:span "una flo" [:strong "r r"] "ara"] "[unaflÕrÒRa]" "fusión"]]]
      [:table
       [:thead (into [:tr]
                     (for [h headings]
                       [:th h]))]
       (into [:tbody]
             (for [[p tr ti] rows]
               [:tr [:td p]
                [:td.ipa.red tr]
                [:td.red ti]]))])]])



(defn render-respuestas
  "Chapter 17 respuestas"
  []
  [:main.c17.respuestas.content
   [shared/fonetica-title "Capítulo 17 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]
     ]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li "Las siguientes palabras/frases contienen secuencias consonánticas indicadas en negrilla.  Complete el siguiente cuadro con la transcripción de las palabras/frases e indique el tipo de secuencia consonántica."
    [shared/media-icon :r (rfe/href :fonetica.routes/c17-r) :same-session]
    (let [headings ["Palabra/frase" "Transcripción"
                    [:div "Tipo de secuencia"
                     [:p.is-size-7 "(fusión, fusíón ambisilábica, grupo consonántico tautosilábico, secuencia consonántica heterosilábica)"]]]
          rows [[[:span "tos severa"] "[tÕseB×Ra]" "fusión"]
                [[:span "obstáculo"] "[oBstÒkulo]" "grupo consonántico tautosilábico"]
                [[:span "astillero"] "[astiJ×Ro]" "secuencia consonántica heterosilábica"]
                [[:span "pedicoj jocoso"] "[peDikÕxokÕso]" "fusión"]
                [[:span "engreír"] "[åNgReÔR]" "secuencia consonántica heterosilábica"]
                [[:span "andariego"] "[ÜªdaRjÓGo]" "secuencia consonántica heterosilábica"]
                [[:span "ebrio"] "[ÓBRjo]" "grupo consonántico tautosilábico"]
                [[:span "postrimero"] "[po¸rim×Ro]" "secuencia consonántica heterosilábica"]
                [[:span "perspicaz"]"[pErspikÒs]" "secuencia consonántica heterosilábica"]
                [[:span "aptitud"] "[ap<titÖD]" "secuencia consonántica heterosilábica"]
                [[:span "incrementar"] "[ÞNkRemåªtÒr]" "grupo consonántico tautosilábico"]
                [[:span "cursi"] "[kÖRsi]" "secuencia consonántica heterosilábica"]
                [[:span "el labio"] "[El:ÒBjo]" "fusión ambisilábica"]
                [[:span "un novio"] "[àn:ÕBjo]" "fusión ambisilábica"]
                [[:span "campestre"] "[kÜmp×stRe]" "secuencia consonántica heterosilábica"]
                [[:span "un músculo"] "[àm:Öskulo]" "fusión ambisilábica"]
                [[:span "adscribir"] "[aDskRiBÔr]" "grupo consonántico tautosilábico"]
                [[:span "Ciudad del Este"] "[sjuDÒDEl×ste]" "fusión"]
                [[:span "eximio"] "[EksÔmjo]" "secuencia consonántica heterosilábica"]
                [[:span "una flor rara"] "[unaflÕrÒRa]" "fusión"]]]
      [:table
       [:thead (into [:tr]
                     (for [h headings]
                       [:th h]))]
       (into [:tbody]
             (for [[p tr ti] rows]
               [:tr [:td p]
                [:td]
                [:td]]))])]])

(defn actividades
  "Chapter 17 actividades"
  []
  [:section.actividades.content
   [:h2 "ACTIVIDADES"]
   [:ol.actividades
    [a1]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C17-M1.mp3")] "Secuencias de consonantes homólogas (el Cuadro 17.1)."]
    [:li [shared/media-icon :audio (shared/file "C17-M2.mp3")] "Fusión simple de consonantes homólogas."]
    [:li [shared/media-icon :audio (shared/file "C17-M3.mp3")] "El contraste entre consonantes simples y consonantes alargadas."]
    [:li [shared/media-icon :audio (shared/file "C17-M4.mp3")] "Consonantes homólogas en inglés."]
    [:li [shared/media-icon :audio (shared/file "C17-M5.mp3")] "Secuencias de fonemas oclusivos más /l/ en español."]
    [:li [shared/media-icon :audio (shared/file "C17-M6.mp3")] "Secuencias de fonemas oclusivos más /R/ en español."]
    [:li [shared/media-icon :audio (shared/file "C17-M7.mp3")] "Secuencias de consonantes tautosilábicas en la coda."]
    [:li [shared/media-icon :audio (shared/file "C17-M8.mp3")] "Secuencias de consonantes heterosilábicas en que la segunda consonante es obstruyente (el Cuadro 17.11)."]
    [:li [shared/media-icon :audio (shared/file "C17-M9.mp3")] "Secuencias de consonantes heterosilábicas en que la segunda consonante es sonorante (el Cuadro 17.19)."]
    [:li [shared/media-icon :ej (shared/file "C17-M10.mp3")] "Ejercicios de pronunciación: la fusión consonántica."]
    [:li [shared/media-icon :ej (shared/file "C17-M11.mp3")] "Ejercicios de pronunciación: secuencias consonánticas tautosilábicas."]
    [:li [shared/media-icon :ej (shared/file "C17-M12.mp3")] "Ejercicios de pronunciación: secuencias consonánticas heterosilábicas."]]])

(defn render
  "Chapter seventeen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 17 — Los fonemas laterales y vibrantes"
    "E-Resources"]
   [:div.content.chapter-17
    [materiales]
    [actividades]]
   [shared/footer]])
