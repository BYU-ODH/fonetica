(ns fonetica.views.c21
  "Chapter 21"
  (:require [fonetica.views.components.shared :as shared :refer [ipa fempr red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas activity 1"
  []
  [:section.ra1
   [:li [:div "Las siguientes grabaciones son de las transcripciones fonéticas hechas en el Capítulo 9.  Escuche las siguientes grabaciones e indique las pausas entre los grupos fónicos y los tonemas correspondientes."]

    (let [activities [
                      [["#1 {martina tenía los modales bruscos y la voz áspera también tenía fama de mal genio y en la cocina del abuelo todos sabían que no se le podía gastar bromas ni burlas} (“Envidia,”  " [:em "Historias de la Artámila"] " , Ana María Matute)"]
                       ["[maRtÔnatenÔalo¸moDÒle¸BRÖskos" [red "" [red "ð/"] ""] "ilaBÕsÒspERa" [red "ô\\"] " tÜmbjì ªtenÔafÒmaDemÒlxÓnjo" [red "ô/"] "jånlakosÔnaDElaBwÓlo" [red "ð/"] "tÕDosaBÔ Ün" [red "ð/"] "kenÕselepoDÔaGastÒRBrÕma¸niBÖRlas" [red "ô "] "]"]]
                      [["#2 {el deshielo se retrasaba y el sol se hacía pegajoso adhesivo a la piel a través de la niebla los del campo andaban de mal humor blasfemando seguramente no se les presentaban bien las cosas de la tierra} (“Los alambradores,” " [:em "Historias de la Artámila"] ", Ana María Matute)"]
                       ["[E¢dE¸JÓloseretRasÒBa" [red "ð/"] "jElsÕlse`asÔapeGaxÕso" [red "ò/"] "aDesÔBo`alap j×l " [red "ò/"] "atRaBÓ¸DelanjÓBla" [red "ô\\"] " lo¸DElkæmpo`ÜªdÒBÜªdemÒlumÕR" [red " /"] "blasfemæªdo" [red "ô\\"] "seGÖRamìªte" [red "ò/"] "nÕsepResåªtÒBÜmbjìn" [red "ð/"] "las kÕsa¸Delatj×ra" [red "ô "] "]"]]
                      [["#3 {el paraná corre allí en el fondo de una inmensa hoya cuyas paredes altas de cien metros encajonan fúnebremente el río desde las orillas bordeadas de negros bloques de basalto asciende el bosque negro también} (“A la deriva,” " [:em "Cuentos de amor, de locura y de muerte"] ", Horacio Quiroga)"]
                       ["[ElpaRanÒkÕre`aJÔ" [red "ð/"] "EnElféªdoDeUnaãm:ìnsa`ÕJa" [red "ô/"] "kÖJaspaR×D es" [red "ò/"] "Ò¢ta¸Desjìm:×tRos" [red "ò/"] "åNkaxÕnÜMfÖneBRemìªtElrÔo" [red "ô\\"] " dE¸ DelasoRÔJas" [red "ò/"] "boRDe`ÒDa¸DenÓGRo¸BlÕke¸DeBasÒ¢to" [red "ò/"] "asjìªdE lBÕske" [red "ô/"] "nÓGRotÜmbjìn" [red "ô "] "]"]]
                      [["#4 {no saludó al entrar yo estaba repasando sobre una bandana la mejor de mis navajas y cuando lo reconocí me puse a temblar pero él no se dio cuenta para disimular continué repasando la hoja la probé luego sobre la yema del dedo gordo y volví a mirarla contra la luz} (“Espuma y nada más,” " [:em "Cenizas para el viento y otras historias"] ", Hernando Téllez)"]
                       ["[nÕsaluDÕ`alåªtRÒR" [red "ô\\"] " ÌÕ`EstÒBarepasæªdosoBReUnaBÜªdÒna" [red "ð/ /"] "lamexÕRDemi¸naBÒxas" [red "ô\\"] " ikwÜªdolorekonosÔ" [red "ð/"] "mepÖse`at ÝmblÒR" [red "ô\\"] " pERo`×lnÕseDjÕkwìªta" [red "ô\\"] " paRaDisimulÒR" [red "ð/"] "kßªtinw Órepasæªdola`Õxa" [red "ô\\"] " lapRoBÓlwÓGosoBRelaJÓmaDE¢dÓDoGÕR Do" [red "ð/"] "iBolBÔamiRÒRlakßªtRalalÖs" [red "ô "] "]"]]
                      [["#5 {la condesa apareció en la puerta de la estancia donde se detuvo jadeante y sin fuerza con la muleta apartaba el blasonado portier rosarito se limpió los ojos y acudió velozmente la noble señora apoyó la diestra blanca y temblona en el hombro de su nieta y cobró aliento en un suspiro} (“Rosarito” " [:em "Femeninas: Seis historias amorosas"] ", Ramón María del Valle-Inclán)"]
                       ["[lakßªdÓsapaResjÕ" [red "ð/"] "ånlapw×RtaDela`Estænsja" [red "ð/"] "dßªdeseDetÖ BoxaDe`æªte" [red "ð/"] "isÞMfw×Rsa" [red "ô\\"] " kßnlamul×ta" [red "ð/"] "apaRtÒBa`ElBlason ÒDopoRtj×R" [red "ô\\"] " rosaRÔtoselÞmpjÕlosÕxos" [red "ð/"] "jakuDjÕBelÕ¸mìªt e" [red "ô\\"] " lanÕBlese¦ÕRa" [red "ð/"] "apoJÕlaDj×stRa" [red "ò/"] "blæNkaItåmblÕna" [red "ò/"] "EnEl émbRoDesunj×ta" [red "ô/"] "ikoBRÕ`aljìªto" [red "ð/"] "EnànsuspÔRo" [red "ô "] "]"]]
                      [["#6 {antes de que pudiera introducir la llave en la cerradura la puerta se abrió
apareció un indio amarillo en bata de casa con bufanda su aspecto no podía ser
más repulsivo despedía un olor a loción barata su cara polveada quería cubrir las
arrugas tenía la boca embarrada de lápiz labial mal aplicado y el pelo daba la
impresión de estar teñido} (“Chac Mool,” " [:em "Los días enmascarados"] ", Carlos Fuentes)"]
                       ["[Üªte¸DekepuDj×RaãªtRoDusÔRlaJÒBånlasEraDÖRa" [red "ð/"] "lapw×Rta se`aBRjÕ" [red "ô\\"] " apaResjÕUnèªdjo`amaRÔJo" [red "ò/"] "åmbÒtaDekÒsa" [red "ò/"] "kßmb ufæªda" [red "ô\\"] " swasp×ktonÕpoDÔas×RmÒ¸repulsÔBo" [red "ô/"] "dEspeDÔaUn olÕRalosjémbaRÒta" [red "ô/"] "sukÒRa" [red "ò/"] "polBe`ÒDa" [red "ò/"] "kERÔakuBRÔRlasarÖG as" [red "ô/"] "tenÔalaBÕka`åmbarÒDaDelÒpi¸laBjÒlmÒlaplikÒDo" [red "ð/"] "jElpÓl oDÒBalaãmpresjéªdEstÒRte¦ÔDo" [red "ô "] "]"]]

                      ] ]
      (into [:div.all-a1]
             (for [[a b] activities]
              [:div {:style {:margin-bottom "2em"}}
               (into [:div {:style {:margin-bottom "1em"}}] a)
               (into [:div.ipa.is-size-4 {:style {:line-break "anywhere"}}] b)
               ]))

      )]])

(defn render-respuestas
  "Chapter 21 respuestas"
  []
  [:main.c21.respuestas.content
   [shared/fonetica-title "Capítulo 21 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li [:div "Las siguientes grabaciones son de las transcripciones fonéticas hechas en el Capítulo 9.  Escuche las siguientes grabaciones e indique las pausas entre los grupos fónicos y los tonemas correspondientes."
         ;[shared/media-icon :audio] 
         [shared/media-icon :r (rfe/href :fonetica.routes/c21-r) :same-session]]
    (let [activities [
                      [["#1 {martina tenía los modales bruscos y la voz áspera también tenía fama de mal genio y en la cocina del abuelo todos sabían que no se le podía gastar bromas ni burlas} (“Envidia,”  " [:em "Historias de la Artámila"] " , Ana María Matute)" [shared/media-icon :audio (shared/file "C21-Int-1.mp3")]]
                       ["[maRtÔnatenÔalo¸moDÒle¸BRÖskosilaBÕsÒspERatÜmbjìªtenÔafÒmaDemÒlxÓnjojånlakosÔnaDElaBwÓlotÕDosaBÔÜn  kenÕselepoDÔaGastÒRBrÕma¸niBÖRlas]"]]
                      [["#2 {el deshielo se retrasaba y el sol se hacía pegajoso adhesivo a la piel a través de la niebla los del campo andaban de mal humor blasfemando seguramente no se les presentaban bien las cosas de la tierra} (“Los alambradores,” " [:em "Historias de la Artámila"] ", Ana María Matute)"
                        [shared/media-icon :audio (shared/file "C21-Int-2.mp3")]]
                       ["[E¢dE¸JÓloseretRasÒBajElsÕlse`asÔapeGaxÕsoaDesÔBo`alapj×latRaBÓ¸DelanjÓBlalo¸DElkæmpo`ÜªdÒBÜªdemÒlumÕRblasfemæªdoseGÖRamìªtenÕseResåªtÒBÜmbjìnlaskÕsa¸Delatj×ra]"]]
                      [["#3 {el paraná corre allí en el fondo de una inmensa hoya cuyas paredes altas de cien metros encajonan fúnebremente el río desde las orillas bordeadas de negros bloques de basalto asciende el bosque negro también} (“A la deriva,” " [:em "Cuentos de amor, de locura y de muerte"] ", Horacio Quiroga)"
                        [shared/media-icon :audio (shared/file "C21-Int-3.mp3")]]
                       ["[ElpaRanÒkÕre`aJÔEnElféªdoDeUnaãm:ìnsa`ÕJakÖJaspaR×DesÒ¢ta¸Desjìm:×tRosåNkaxÕnÜMfÖneBRemìªtElrÔodE¸DelasoRÔJasboRDe`ÒDa¸DenÓGRo¸BlÕke¸DeBasÒ¢toasjìªdElBÕskenÓGRotÜmbjìn]"]]
                      [["#4 {no saludó al entrar yo estaba repasando sobre una bandana la mejor de mis navajas y cuando lo reconocí me puse a temblar pero él no se dio cuenta para disimular continué repasando la hoja la probé luego sobre la yema del dedo gordo y volví a mirarla contra la luz} (“Espuma y nada más,” " [:em "Cenizas para el viento y otras historias"] ", Hernando Téllez)"
                        [shared/media-icon :audio (shared/file "C21-Int-4.mp3")]]
                       ["[nÕsaluDÕ`alåªtRÒRÌÕ`EstÒBarepasæªdosoBReUnaBÜªdÒnalamexÕRDemi¸naBÒxasikwÜªdolorekonosÔmepÖse`atÝmblÒRpERo`×lnÕseDjÕkwìªtapaRaDisimulÒRkßªtinwÓrepasæªdola`ÕxalapRoBÓlwÓGosoBRelaJÓmaDE¢dÓDoGÕRDoiBolBÔamiRÒRlakßªtRalalÖs]"]]
                      [["#5 {la condesa apareció en la puerta de la estancia donde se detuvo jadeante y sin fuerza con la muleta apartaba el blasonado portier rosarito se limpió los ojos y acudió velozmente la noble señora apoyó la diestra blanca y temblona en el hombro de su nieta y cobró aliento en un suspiro} (“Rosarito” " [:em "Femeninas: Seis historias amorosas"] ", Ramón María del Valle-Inclán)" [shared/media-icon :audio (shared/file "C21-Int-5.mp3")]]
                       ["[lakßªdÓsapaResjÕånlapw×RtaDela`EstænsjadßªdeseDetÖBoxaDe`æªteisÞMfw×Rsakßnlamul×taapaRtÒBa`ElBlasonÒDopoRtj×RrosaRÔtoselÞmpjÕlosÕxosjakuDjÕBelÕ¸mìªtelanÕBlese¦ÕRaapoJÕlaDj×stRablæNkaItåmblÕnaEnElémbRoDesunj×taikoBRÕ`aljìªtoEnànsuspÔRo]"]]
                      [["#6 {antes de que pudiera introducir la llave en la cerradura la puerta se abrió apareció un indio amarillo en bata de casa con bufanda su aspecto no podía ser más repulsivo despedía un olor a loción barata su cara polveada quería cubrir las arrugas tenía la boca embarrada de lápiz labial mal aplicado y el pelo daba la impresión de estar teñido} (“Chac Mool,” " [:em "Los días enmascarados"] ", Carlos Fuentes)" [shared/media-icon :audio (shared/file "C21-Int-6.mp3")]]
                       ["[Üªte¸DekepuDj×RaãªtRoDusÔRlaJÒBånlasEraDÖRalapw×Rtase`aBRjÕapaResjÕUnèªdjo`amaRÔJoåmbÒtaDekÒsakßmbufæªdaswasp×ktonÕpoDÔas×RmÒ¸repulsÔBodEspeDÔaUnolÕRalosjémbaRÒtasukÒRapolBe`ÒDakERÔakuBRÔRlasarÖGastenÔalaBÕka`åmbarÒDaDelÒpi¸laBjÒlmÒlaplikÒDojElpÓloDÒBalaãmpresjéªdEstÒRte¦ÔDo]"]]

                      ] ]
      (into [:div.all-a1 {:style {:margin-top "2em"}}]
            (for [[a b] activities]
              [:div {:style {:margin-bottom "2em"}}
               (into [:div {:style {:margin-bottom "1em"}}] a)
               (into [:div.ipa.is-size-4 {:style {:line-break "anywhere"}}] b)
               ]))

      )]]
  )

(defn actividades
  "Chapter 21 actividades"
  []
  [:section.actividades.content
   [:h2 "ACTIVIDADES"]
   [:ol.actividades
    [a1]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C21-M1.mp3")] "Cambios tonales en oraciones que comunican significados distintos."]
    [:li [shared/media-icon :audio (shared/file "C21-M2.mp3")] "Posibles divisiones de oraciones en grupos fónicos."]
    [:li [shared/media-icon :audio (shared/file "C21-M3.mp3")] "Grupos fónicos con pausa final."]
    [:li [shared/media-icon :audio (shared/file "C21-M4.mp3")] "Grupos fónicos enumerativos."]
    [:li [shared/media-icon :audio (shared/file "C21-M5.mp3")] "Grupos fónicos explicativos."]
    [:li [shared/media-icon :audio (shared/file "C21-M6.mp3")] "Grupos fónicos con modificadores adverbiales antepuestos."]
    [:li [shared/media-icon :audio (shared/file "C21-M7.mp3")] "Cláusulas explicativas en contraste con cláusulas especificativas."]
    [:li [shared/media-icon :audio (shared/file "C21-M8.mp3")] "Frases explicativas en contraste con frases especificativas."]
    [:li [shared/media-icon :audio (shared/file "C21-M9.mp3")] "Oraciones que demuestran la propiedad asociativa lingüística."]
    [:li [shared/media-icon :audio (shared/file "C21-M10.mp3")] "Grupos fónicos optativos."]
    [:li [shared/media-icon :audio (shared/file "C21-M11.mp3")] "Grupos fónicos asistemáticos."]
    [:li [shared/media-icon :audio (shared/file "C21-M12.mp3")] "Oraciones en que cambian de significado según su entonación."]
    [:li [shared/media-icon :audio (shared/file "C21-M13.mp3")] "Oraciones ejemplares para la transcripción TOBI."]
    [:li [shared/media-icon :audio (shared/file "C21-M14.mp3")] "Oraciones ejemplares para los tonemas del español."]
    [:li [shared/media-icon :audio (shared/file "C21-M15.mp3")] "Oraciones ejemplares para los tonemas del inglés."]
    [:li [shared/media-icon :audio (shared/file "C21-M16.mp3")] "La entonación de oraciones enunciativas de un solo grupo fónico."]
    [:li [shared/media-icon :audio (shared/file "C21-M17.mp3")] "La entonación de oraciones enunciativas de dos grupos fónicos."]
    [:li [shared/media-icon :audio (shared/file "C21-M18.mp3")] "La entonación de oraciones enunciativas de tres grupos fónicos o más."]
    [:li [shared/media-icon :audio (shared/file "C21-M19.mp3")] "Series con conjunciones y sin ellas."]
    [:li [shared/media-icon :audio (shared/file "C21-M20.mp3")] "Cláusulas y frases explicativas y casos de hipérbaton."]
    [:li [shared/media-icon :audio (shared/file "C21-M21.mp3")] "Modificaciones adverbiales de oración."]
    [:li [shared/media-icon :audio (shared/file "C21-M22.mp3")] "Frases y locuciones adversativas."]
    [:li [shared/media-icon :audio (shared/file "C21-M23.mp3")] "Comentarios parentéticos."]
    [:li [shared/media-icon :audio (shared/file "C21-M24.mp3")] "Citas enunciativas directas e indirectas."]
    [:li [shared/media-icon :audio (shared/file "C21-M25.mp3")] "Vocativos en las enunciativas."]
    [:li [shared/media-icon :audio (shared/file "C21-M26.mp3")] "Preguntas SÍ/NO."]
    [:li [shared/media-icon :audio (shared/file "C21-M27.mp3")] "Preguntas SÍ/NO con una respuesta implícita."]
    [:li [shared/media-icon :audio (shared/file "C21-M28.mp3")] "Preguntas con palabras interrogativas."]
    [:li [shared/media-icon :audio (shared/file "C21-M29.mp3")] "Preguntas con palabras interrogativas con cortesía."]
    [:li [shared/media-icon :audio (shared/file "C21-M30.mp3")] "Preguntas repetidas."]
    [:li [shared/media-icon :audio (shared/file "C21-M31.mp3")] "Preguntas disyuntivas."]
    [:li [shared/media-icon :audio (shared/file "C21-M32.mp3")] "Preguntas ratificadas."]
    [:li [shared/media-icon :audio (shared/file "C21-M33.mp3")] "Preguntas indirectas."]
    [:li [shared/media-icon :audio (shared/file "C21-M34.mp3")] "Vocativos en las interrogativas."]
    [:li [shared/media-icon :audio (shared/file "C21-M35.mp3")] "Preguntas con valor afectivo."]
    [:li [shared/media-icon :audio (shared/file "C21-M36.mp3")] "Preguntas con más de un grupo fónico."]
    [:li [shared/media-icon :audio (shared/file "C21-M37.mp3")] "Las oraciones imperativas."]
    [:li [shared/media-icon :audio (shared/file "C21-M38.mp3")] "Vocativos en las imperativas."]
    [:li [shared/media-icon :audio (shared/file "C21-M39.mp3")] "Las expresiones exclamativas."]
    [:li [shared/media-icon :audio (shared/file "C21-M40.mp3")] "El dialecto mexicano norteño y el dialectos argentino porteño."]
    [:li [shared/media-icon :ej] "Ejercicio de pronunciación de la entonación."
     [:div.is-size-7
      "*Los ejercicios son de las siguientes obras literarias:"
      [:div "#1 “Noventa días,” " [:em "Cuentos"] ", Alfonso Hernández Catá" 
       [shared/media-icon :audio (shared/file "C21-M41-1.mp3")]]
      [:div "#2 “Londres: La cocina de la huelga,” Julio Camba" 
       [shared/media-icon :audio (shared/file "C21-M41-2.mp3")]]
      [:div "#3 " [:em "La zapatera prodigiosa"] ", Federico García Lorca" 
       [shared/media-icon :audio (shared/file "C21-M41-3.mp3")]]
      [:div "#4 " [:em "La zapatera prodigiosa"] ", Federico García Lorca" 
       [shared/media-icon :audio (shared/file "C21-M41-4.mp3")]]
      [:div "#5 “¡Mi mismo nombre!”, " [:em "Cuentos alegres"] ", Luis Taboada" 
       [shared/media-icon :audio (shared/file "C21-M41-5.mp3")]]
      [:div "#6 “El tesoro,” " [:em "Historias de la Artámila"] ", Ana María Matute" 
       [shared/media-icon :audio (shared/file "C21-M41-6.mp3")]]]]]])

(defn render
  "Chapter nineteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 21 — La entonación"
    "E-Resources"]
   [:div.content.chapter-21
    [materiales]
    [actividades]]
   [shared/footer]])
