(ns fonetica.views.c18
  "Chapter 18"
  (:require [fonetica.views.components.shared :as shared :refer [ipa femp red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas activity 1"
  []
  [:section.ra1
   [:li
    [:div {:style {:display "inline"}} "Transcriba las siguientes oraciones separando las sílabas fonosintácticas mediante puntos.  Ejemplo: El otro día fuimos al evento: "]
    [:div.r.ipa.red {:style {:display "inline" :margin-left "5em"}} "[E.lÕ.tRo.DÔ.a.fwÔ.mo.sa.le.Bìª.to]"]
    (let [rows [["Le vi a Marcos en la plaza." "[le.BÔ.a.mÒR.ko.sån.la.pla.sa]"]
                ["Estaba con Nico el italiano." "[Es.tÒ.Ba.kß`n:`Ô.ko`E.li.ta.ljÒ.no]"]
                ["Ellos querían hablar a solas." "[Ó.Jos.kE.RÔ.a.na.BlÒ.Ra.sÕ.las]"]
                ["Escribí unas cartas a Ana." "[Es.kRi.BÔ.u.nas.kÒr.ta.sÒ:.na]"]
                ["Los soldados salieron de la estación." "[lo.so¢.dÒ.Do.sa.lj×.Roª.de.la`Es.ta.sjén]"]
                ["Tenían una sed dolorosa." "[te.nÔ.a.nu.na.s×.Do.lo.RÕ.sa]"]
                ["Dicen que es de ella el hilo." "[dÔ.sEN.k×¸.DÓ:.Ja`E.lÔ.lo]"]
                ["Acusaron al niño de holgazán." "[a.ku.sÒ.Ro.nal.nÔ.¦o.De`ol.Ga.sÒn]"]
                ["La niña aprendió a ser muy industriosa." "[la.nÔ.¦a.pRåª.djÕ`a.s×r.mwÔª.dus.trjÕ.sa]"]
                ["El leguleyo no quiso ser razonable." "[E`l:`e.Gu.lÓ.Jo.no.kÔ.so.s×.ra.so.nÒ.Ble]"]
                ["Los hijos de Joaquín fueron de mal en peor. " "[lo.sÔ.xo¸.De.xo`a.kÔM.fw×.Roª.de.mÒ.låm.pe`Õr]"]
                ["Voy a consultarlo con la almohada. " "[bÕ.ja.kßn.su¢.tÒr.lo.kßn.lal.mo`Ò.Da]"]]]
      (into [:ol.list.is-lower-alpha]
            (for [[a b] rows]
              [:li [:div.grid2 [:div.l a] [:div.r.red.ipa b]]])))]])



(defn render-respuestas
  "Chapter 18 respuestas"
  []
  [:main.c18.respuestas.content
   [shared/fonetica-title "Capítulo 18 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]
     ]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li
    [:div {:style {:display "inline"}} "Transcriba las siguientes oraciones separando las sílabas fonosintácticas mediante puntos."
     [shared/media-icon :r (rfe/href :fonetica.routes/c18-r) :same-session]
     " Ejemplo: El otro día fuimos al evento: "]
    [:div.r.ipa.red {:style {:display "inline" :margin-left "5em"}} "[E.lÕ.tRo.DÔ.a.fwÔ.mo.sa.le.Bìª.to]"]
    (let [rows [["Le vi a Marcos en la plaza." "[le.BÔ.a.mÒR.ko.sån.la.pla.sa]"]
                ["Estaba con Nico el italiano." "[Es.tÒ.Ba.kß`n:`Ô.ko`E.li.ta.ljÒ.no]"]
                ["Ellos querían hablar a solas." "[Ó.Jos.kE.RÔ.a.na.BlÒ.Ra.sÕ.las]"]
                ["Escribí unas cartas a Ana." "[Es.kRi.BÔ.u.nas.kÒr.ta.sÒ:.na]"]
                ["Los soldados salieron de la estación." "[lo.so¢.dÒ.Do.sa.lj×.Roª.de.la`Es.ta.sjén]"]
                ["Tenían una sed dolorosa." "[te.nÔ.a.nu.na.s×.Do.lo.RÕ.sa]"]
                ["Dicen que es de ella el hilo." "[dÔ.sEN.k×¸.DÓ:.Ja`E.lÔ.lo]"]
                ["Acusaron al niño de holgazán." "[a.ku.sÒ.Ro.nal.nÔ.¦o.De`ol.Ga.sÒn]"]
                ["La niña aprendió a ser muy industriosa." "[la.nÔ.¦a.pRåª.djÕ`a.s×r.mwÔª.dus.trjÕ.sa]"]
                ["El leguleyo no quiso ser razonable." "[E`l:`e.Gu.lÓ.Jo.no.kÔ.so.s×.ra.so.nÒ.Ble]"]
                ["Los hijos de Joaquín fueron de mal en peor. " "[lo.sÔ.xo¸.De.xo`a.kÔM.fw×.Roª.de.mÒ.låm.pe`Õr]"]
                ["Voy a consultarlo con la almohada. " "[bÕ.ja.kßn.su¢.tÒr.lo.kßn.lal.mo`Ò.Da]"]]]
      (into [:ol.list.is-lower-alpha]
            (for [[a _] rows]
              [:li [:div.l a]])))]])

(defn actividades
  "Chapter 18 actividades"
  []
  [:section.actividades.content
   [:h2 "ACTIVIDADES"]
   [:ol.actividades
    [a1]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C18-M1.mp3")] "El silabeo fonético de {general} en inglés y español."]
    [:li [shared/media-icon :audio (shared/file "C18-M2.mp3")] "El silabeo fonético de “El otro día fuimos al evento” (el Cuadro 18.18)."]
    [:li [shared/media-icon :audio (shared/file "C18-M3.mp3")] "El silabeo—Regla 1: el contraste entre el silabeo del inglés y del español con una consonante intervocálica (el Cuadro 18.19)."]
    [:li [shared/media-icon :audio (shared/file "C18-M4.mp3")] "El silabeo—Regla 2: el silabeo del español con dos consonantes intervocálicas."]
    [:li [shared/media-icon :audio (shared/file "C18-M5.mp3")] "El silabeo—Regla 2 y Regla 3: el silabeo del español con dos, tres o cuatro consonantes intervocálicas (el Cuadro 18.20)."]
    [:li [shared/media-icon :audio (shared/file "C18-M6.mp3")] "El silabeo fonético de “El pan y el agua” (el Cuadro 18.21)."]
    [:li [shared/media-icon :audio (shared/file "C18-M7.mp3")] "El silabeo fonético de palabras."]
    [:li [shared/media-icon :ej (shared/file "C18-M8.mp3")] "El silabeo fonético de un texto (el Cuadro 18.27)."]
    [:li [shared/media-icon :ej (shared/file "C18-M9.mp3")] "Ejercicios de pronunciación: el silabeo."]]])

(defn render
  "Chapter eighteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 18 — Respuestas a las actividades"
    "E-Resources"]
   [:div.content.chapter-18
    [materiales]
    [actividades]]
   [shared/footer]])
