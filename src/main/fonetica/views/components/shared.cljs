(ns fonetica.views.components.shared
  "Shared components that may be used on multiple views"
  (:require [reagent.core :as r]
            [goog.dom :as dom]
            [goog.string :as gstr]
            [fonetica.views.components.modal :as modal]
            [reitit.frontend.easy :as rfe])
  (:import goog.ui.AnimatedZippy))

(defn accordion [{:keys [header-text content-body]}]
  (let [header-id (gstr/createUniqueString)
        content-id (gstr/createUniqueString)]
    (r/create-class
     {:display-name "zippy"
      :reagent-render (fn [id] [:div.zippy-container
                                [:div {:id (str "wrap-" header-id)}
                                 [:h1.title.is-5 {:id header-id} header-text]
                                 [:div{:id content-id} content-body]]])
      :component-did-mount #(AnimatedZippy. (dom/$ header-id)
                                            (dom/$ content-id))})))

(defn fonetica-title
  "Title Hero for the page"
  [text & [subtext]]
  [:section.hero
   [:div.container.box
    [:h1
     text]
    (when-let [s subtext]
      [:h3.subtext s])]])

(def media-type-filenames
  "Filenames for the given media-types"
  {:airplane "Airplane-icon.png"
   :audio "audio-icon.png"
   :cellphone "Cellphone-icon.png"
   :ej "EJ-icon.png"
   :ejb "EJ-icon-bold.png"
   :envelope "Envelope-icon.png"
   :ex "EX-icon.png"
   :exb "EX-icon-bold.png"
   :image "image-icon.png"
   :imageb "image-icon-bold.png"
   :lightbulb "Lightbulb-icon.png"
   :m-w "M-W-icon.png"
   :mouse "Mouse-icon.png"
   :r "R-icon.png"
   :text "text-icon.png"
   :text-ej "text-EJ-icon.png"
   :text-fat "text-icon-fat.png"
   :video "video-icon.png"
   :videob "video-icon-bold.png"
   :wrench "Wrench-icon.png"})

(defn media-icon
  "Render a media icon and its attached link"
  [icon-type href & [to-page?]]
  (let [base "/Files/"
        file-name (media-type-filenames icon-type)
        file-src (str base file-name)
        opt-map (cond->
                    {:href href
                     :on-click
                     (fn [e]
                       (.preventDefault e)
                       (modal/populate-modal href))}
                  to-page? (dissoc :on-click))]
    [:a.ficon opt-map
     [:img.ficon {:src file-src}]]))

(defn file
  "get the filepath for `filename`"
  [file-name]
  (rfe/href :fonetica.routes/files {:file-name file-name}))

(defn item-grid
  "a table-like grid"
  [items]
  (into [:div.photogrid]
        (for [i items]
           [:div.image-container {"border-style" :solid
                                  "border-width" "1px"
                                  "border-color" "black"}
            i])))

(defn footer
  "Return to Chapter Index on every page"
  []
  [:div.footer
   [:a {:href (rfe/href :fonetica.routes/table-of-contents)} "RETURN TO CHAPTER INDEX"]])

(defn ipa
  "Render `s` as ipa font"
  [s]
  [:span.ipa s])

(defn femp
  "Emphasize a portion mid-word"
  [s]
  [:span.femp s])

(defn fempr
  "Emphasize a portion mid-word, underline and red"
  [s]
  [:span.femp.red s])

(defn render-ej
  "Chapter 5 ej helper"
  [{:keys [body index title]}]
  [:main.c5.ej.content
   [:div.identifier index]
   [fonetica-title title]
   body])

(defn red
  "red span on `s`"
  [s]
  [:span.red s])

(defn alt-from-filename 
  "Generate alt text based on the non-idenifier parts of the filename, eg Suit from C1-M1-Suit.jpg"
  [s] (second (re-matches #".*?M.+?-(.*?)\..*" s)))

(def populate-modal modal/populate-modal)
(def clear-modal modal/clear-modal)
