(ns fonetica.views.components.modal
  (:require [reagent.core :as r]
            [goog.string.path :as goop]))

(defonce MODAL-CONTENTS (r/atom nil))

(def media-types
  "Media types by the five file-types we see in our application"
  {"jpg" :image
   "png" :image
   "gif" :image
   "mp3" :audio
   "mp4" :video})

(defn media-for-string
  "Render the appropriate ligtbox thing depending on the extensions of string `media-string`"
  [media-string]
  (let [ext (goop/extension media-string)]
    (case (media-types ext)
      :image [:img {:src media-string}]
      :audio [:audio {:controls true
                      :src media-string}]
      :video [:video {:controls true
                      :src media-string}]
      media-string)))

(defn render-media-content
  "Render the DOM structure for a desired media player"
  [media] ;(def media-path "http://localhost:8080/Files/C1-M2-whistle.mp3")
  (if-not (string? media)
    media
    (media-for-string media)))

(defn populate-modal
  "Put `content` into the modal"
  [content]
  (reset! MODAL-CONTENTS (render-media-content content)))

(defn clear-modal
  []
  (reset! MODAL-CONTENTS nil))

(defn render-modal
  "when `@MODAL-CONTENTS` has anything, render the modal with those"
  []
  (when-let [mc @MODAL-CONTENTS]
    [:div.modal.is-active
     [:div.modal-background {:on-click clear-modal}]
     [:div.modal-content
      mc]
     [:button.modal-close.is-large {:aria-label "close"
                                    :on-click clear-modal}]]))
