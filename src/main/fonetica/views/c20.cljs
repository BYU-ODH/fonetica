(ns fonetica.views.c20
  "Chapter 20"
  (:require [fonetica.views.components.shared :as shared :refer [ipa fempr red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas activity 1"
  []
  [:section.ra1
   [:li
    [:div "Indique cómo se puede enfatizar el elemento subrayado de las siguientes oraciones."]
    (let [rows [[[:div "Juan " [:u "le"] " dio la plata."]
                 "Juan le dio la plata a él."]
                [[:div "" [:u "María"] " festejó demasiado."]
                 "Festejó demasiado María."]
                [[:div "Juanito se comió " [:u "el postre"] "."]
                 "El postre se lo comió Juanito."]
                [[:div "El estudiante es " [:u "inteligente"] "."]
                 "El estudiante es muy inteligente. El estudiante es inteligentísimo."]
                [[:div "Martín bailó " [:u "con Isabel"] "."]
                 "Fue con Isabel que Martín bailó."]
                [[:div "Paco le " [:u "regaló"] " una rosa a Maripili."]
                 "Paco sí le regaló una rosa a Maripili."]
                [[:div "El hoyo fue " [:u "muy profundo"] "."]
                 "El hoyo fue profundísimo. El hoyo fue reprofundo."]
                [[:div "Fueron en " [:u "su"] " coche."]
                 "Fueron en el coche de ellos."]
                [[:div "El perro comió " [:u "el helado"] "."]
                 "El helado lo comió el perro."]
                [[:div "Llegaron al " [:u "mero"] " centro de la ciudad."]
                 "Llegaron al mero mero centro de la ciudad."]
                


                ]]
      (into [:ol.list.is-lower-alpha]
            (for [[i r] rows]
              [:li [:div.grid2
                    [:div.l i]
                    [:div.r.red r]]])))]])

(defn render-respuestas
  "Chapter 20 respuestas"
  []
  [:main.c20.respuestas.content
   [shared/fonetica-title "Capítulo 20 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
      [:li
       [:div "Indique cómo se puede enfatizar el elemento subrayado de las siguientes oraciones."
        [shared/media-icon :r (rfe/href :fonetica.routes/c20-r) :same-session]]
       (let [rows [[[:div "Juan " [:u "le"] " dio la plata."]
                 "Juan le dio la plata a él."]
                [[:div "" [:u "María"] " festejó demasiado."]
                 "Festejó demasiado María."]
                [[:div "Juanito se comió " [:u "el postre"] "."]
                 "El postre se lo comió Juanito."]
                [[:div "El estudiante es " [:u "inteligente"] "."]
                 "El estudiante es muy inteligente. El estudiante es inteligentísimo."]
                [[:div "Martín bailó " [:u "con Isabel"] "."]
                 "Fue con Isabel que Martín bailó."]
                [[:div "Paco le " [:u "regaló"] " una rosa a Maripili."]
                 "Paco sí le regaló una rosa a Maripili."]
                [[:div "El hoyo fue " [:u "muy profundo"] "."]
                 "El hoyo fue profundísimo. El hoyo fue reprofundo."]
                [[:div "Fueron en " [:u "su"] " coche."]
                 "Fueron en el coche de ellos."]
                [[:div "El perro comió " [:u "el helado"] "."]
                 "El helado lo comió el perro."]
                [[:div "Llegaron al " [:u "mero"] " centro de la ciudad."]
                 "Llegaron al mero mero centro de la ciudad."]]]
      (into [:ol.list.is-lower-alpha]
            (for [[i] rows]
              [:li [:div
                    [:div.l i]]])))]])

(defn actividades
  "Chapter 20 actividades"
  []
  [:section.actividades.content
   [:h2 "ACTIVIDADES"]
   [:ol.actividades
    [a1]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C20-M1.mp3")] "La duración consonántica en inglés (el Cuadro 20.1)."]
    [:li [shared/media-icon :audio (shared/file "C20-M2.mp3")] "La duración consonántica en español (el Cuadro 20.2)."]
    [:li [shared/media-icon :audio (shared/file "C20-M3.mp3")] "El ritmo en la poesía inglesa."]
    [:li [shared/media-icon :audio (shared/file "C20-M4.mp3")] "El ritmo en la poesía española."]
    [:li [shared/media-icon :audio (shared/file "C20-M5.mp3")] "El ritmo del inglés."]
    [:li [shared/media-icon :audio (shared/file "C20-M6.mp3")] "El énfasis sintáctico del español en contraste con el énfasis fonético del inglés."]
    [:li [shared/media-icon :audio (shared/file "C20-M7.mp3")] "El énfasis en la sílaba en español."]
    [:li [shared/media-icon :audio (shared/file "C20-M8.mp3")] "El énfasis fonético del inglés comparado con el énfasis sintáctico del español (el Cuadro 20.4)."]
    [:li [shared/media-icon :ej (shared/file "C20-M9.mp3")] "Ejercicios de pronunciación: la duración."]
    [:li [shared/media-icon :ej (shared/file "C20-M10.mp3")] "Ejercicios de pronunciación: el ritmo."]]])

(defn render
  "Chapter nineteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 20 — La duración, el ritmo y el énfasis"
    "E-Resources"]
   [:div.content.chapter-20
    [materiales]
    [actividades]]
   [shared/footer]])
