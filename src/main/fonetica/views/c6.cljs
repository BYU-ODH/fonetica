(ns fonetica.views.c6
  "Chapter 6&"
  (:require [fonetica.views.components.shared :as shared :refer [ipa]]
            [reitit.frontend.easy :as rfe]))

(defn render-respuestas
  "Chapter 6 respuestas"
  []
  [:main.c6.respuestas.container.content
   [shared/fonetica-title "Capítulo 6 — Respuestas a las actividades"]
   [:section.respuestas
    [:ol.list
     [:li.a1
      [:ol.list.is-lower-alpha
       [:li "onda inarmónica"]
       [:li "onda armónica compuesta"]
       [:li "onda armónica simple"]
       [:li "onda cuasiarmónica"]]]
     [:li.a2
      [:ol.list.is-lower-alpha
       [:li "duración"]
       [:li "frecuencia/tono"]
       [:li "timbre/cualidad vocálica"]
       [:li "volumen/amplitud"]]]
     [:li.a3
      [:ol.list.is-lower-alpha
       [:li [:div
             [:div "sonograma de banda estrecha"]
             [:div "Se pueden ver los armónicos y medir el tono fundamental."]
             [:div "x= tiempo, y= frecuencia, z (negrura)=amplitud"]]]
       [:li [:div
             [:div "forma de onda"]
             [:div "Se pueden ver la amplitud, el tipo de onda y la duración."]
             [:div "x=tiempo, y=amplitud"]]]
       [:li [:div
             [:div "sonograma de banda ancha"]
             [:div "Se pueden ver los formantes, si el sonido es sonoro/sordo y medir el tono fundamental."]
             [:div "x= tiempo, y= frecuencia, z (negrura)=amplitud"]]]
       [:li [:div
             [:div "sección espectrográfica"]
             [:div "Se puede ver la amplitud de los formantes en un tiempo fijo."]
             [:div "x=frecuencia, y=amplitud"]]]]]
     [:li.a4 "“En boca cerrada, no entran moscas.”"
      [:img.image {:src (shared/file "C6-Respuestas-A4-En-boca-cerrada-ss.png")}]]]]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:p "1. Identifique el tipo de onda encontrada en las siguientes representaciones:"
    [shared/media-icon :r (rfe/href :fonetica.routes/c6-r) :same-session]]
   [:img.image {:src (shared/file "C6-A1-ss.png")}]])

(defn a2
  "Actividades 2"
  []
  [:section.a2
   [:p "2. Identifique la propiedad acústica que diferencia las dos ondas en cada uno de los siguientes pares de formas de onda. (Vea las páginas 89–91.)"
    [shared/media-icon :r (rfe/href :fonetica.routes/c6-r) :same-session]]
   [:ol.list.is-lower-alpha
    [:li [:img.image {:src (shared/file "C6-A2a-CompDuraciónMerged.jpg")}]]
    [:li [:img.image {:src (shared/file "C6-A2b-CompTonoMerged.jpg")}]]
    [:li [:img.image {:src (shared/file "C6-A2c-CompTimbreMerged.jpg")}]]
    [:li [:img.image {:src (shared/file "C6-A2d-CompVolumenMerged.jpg")}]]]])

(defn a3
  "Actividades 3"
  []
  [:section.a3
   [:p "3. Identifique el tipo de análisis que produjo cada una de las siguientes representaciones. ¿Qué es lo que se puede examinar en cada una de ellas? ¿Qué se representa en cada eje?"
    [shared/media-icon :r (rfe/href :fonetica.routes/c6-r) :same-session]]
   [:ol.list.is-lower-alpha
    [:li [:img.image {:src (shared/file "C6-A3a-narrow-spectrogram.jpg")}]]
    [:li [:img.image {:src (shared/file "C6-A3b-waveform.png")}]]
    [:li [:img.image {:src (shared/file "C6-A3c-wide-spectrogram.jpg")}]]
    [:li [:img.image {:src (shared/file "C6-A3d-sección.png")}]]]])

(defn a4
  "Actividades 4"
  []
  [:section.a4
   [:p "4. Divida el siguiente sonograma en segmentos, indicando qué segmentos son vocales y qué son consonantes. Para las vocales, trata de identificar qué vocal es según sus formantes. Para las consonantes, trata de determinar el modo de articulación, el lugar de articulación y el estado de las cuerdas vocales, y así identificar la consonante. ¿Puede descubrir lo que se dijo?"
    [shared/media-icon :r (rfe/href :fonetica.routes/c6-r) :same-session]]
   [:img.image {:src (shared/file "C6-A4-ss.png")}]])

(defn actividades
  "Chapter 6  actividades"
  []
   [:section.actividades.content
    [:h2 "ACTIVIDADES"]
    [a1]
    [a2]
    [a3]
    [a4]])

(defn render
  "Chapter six view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 6 — La fonética acústica"
    "E-Resources"]
   [:div.content.chapter-6
    [:section.materiales
     [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
     [:ol
      [:li [shared/media-icon :video (shared/file "C6-M1.mp4")] "Video de una onda transmitida por una cuerda."]
      [:li [shared/media-icon :video (shared/file "C6-M2.mp4")] "Video de una onda transmitida por el agua en un charco."]
      [:li [shared/media-icon :video (shared/file "C6-M3.mp4")] "Video del movimiento de un péndulo."]
      [:li [shared/media-icon :audio (shared/file "C6-M4.mp3")] "La vocal [a] en volumen bajo y alto."]
      [:li [shared/media-icon :audio (shared/file "C6-M5.mp3")] "La vocal [a] en tono bajo y alto."]
      [:li [shared/media-icon :audio (shared/file "C6-M6.mp3")] "Las vocales [a] y [e] en el mismo tono y volumen."]
      [:li [shared/media-icon :audio (shared/file "C6-M7.mp3")] "La vocal [a] con duración breve y larga."]
      [:li [shared/media-icon :audio (shared/file "C6-M8.mp3")] "Las vocales [a e i o u]."]
      [:li [shared/media-icon :audio (shared/file "C6-M9.mp3")] "La fricativa alveolar sorda y sonora en " (ipa "[×stamÔ¸ma].")]
      [:li [shared/media-icon :audio (shared/file "C6-M10.mp3")] "“La fonética acústica es fascinante.”"]
      [:li [shared/media-icon :audio (shared/file "C6-M11.mp3")] "Las secuencias [si se sa so su]."]
      [:li [shared/media-icon :audio (shared/file "C6-M12.mp3")] "Secuencias que demuestran los distintos modos de articulación de las consonantes."]
      [:li [shared/media-icon :audio (shared/file "C6-M13.mp3")] "Secuencias que demuestran los distintos lugares de articulación de las consonantes oclusivas."]
      [:li [shared/media-icon :audio (shared/file "C6-M14.mp3")] "Secuencias que demuestran los distintos lugares de articulación de las consonantes fricativas."]
      [:li [shared/media-icon :audio (shared/file "C6-M15.mp3")] "Secuencias que demuestran la diferencia entre sonidos sordos y sonoros."]
      [:li [shared/media-icon :audio (shared/file "C6-M16.mp3")] "“De músico, poeta y loco, todos tenemos un poco.”"]]]
    [actividades]]
   
   [shared/footer]])
