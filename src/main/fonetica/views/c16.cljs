(ns fonetica.views.c16
  "Chapter 16"
  (:require [fonetica.views.components.shared :as shared :refer [ipa femp red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas activity 1"
  []
  [:section.ra1
   [:li "Escriba el símbolo del alófono descrito por los siguientes rasgos."

    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li [:div.grid2
             [:div.l 
              [:div "vibrante múltiple"]
              [:div "alveolar"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa "[r]"]]]
       [:li [:div.grid2
             [:div.l 
              [:div "lateral"]
              [:div "dental"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa "[¢]"]]]
       [:li [:div.grid2
             [:div.l 
              [:div "lateral"]
              [:div "alveolar"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa "[l]"]]]
       [:li [:div.grid2
             [:div.l 
              [:div "lateral"]
              [:div "palatal"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa "[Y]"]]]
       
       ]
      
      [:div.r
       [:li [:div.grid2
             [:div.l 
              [:div "vibrante simple"]
              [:div "alveolar"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa "[R]"]]]

       [:li [:div.grid2
             [:div.l 
              [:div "lateral"]
              [:div "palatalizada"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa "[ù]"]]]

       [:li [:div.grid2
             [:div.l 
              [:div "fricativa acanalada"]
              [:div "alveolar"]
              [:div "sorda"]]
             [:div.r.is-size-3.ipa2 "[‚]"]]]

       [:li [:div.grid2
             [:div.l 
              [:div "lateral"]
              [:div "interdental"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa "[¡]"]]]]]]]])

(defn ra2
  "Respuestas activity 2"
  []
  [:section.ra2
   [:li "Indique los rasgos fonéticos para los siguientes alófonos."

    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[R]"]
          [:div.r 
           [:div "vibrante simple"]
           [:div "alveolar"]
           [:div "sonora"]]]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[¡]"]
          [:div.r 
           [:div "lateral"]
           [:div "interdental"]
           [:div "sonora"]]]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[Y]"]
          [:div.r 
           [:div "lateral"]
           [:div "palatal"]
           [:div "sonora"]]]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[r]"]
          [:div.r 
           [:div "vibrante múltiple"]
           [:div "alveolar"]
           [:div "sonora"]]]]]
       
       ]
      [:div.r
       
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[ù]"]
          [:div.r 
           [:div "lateral"]
           [:div "palatalizada"]
           [:div "sonora"]]]]]

       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[l]"]
          [:div.r 
           [:div "lateral"]
           [:div "alveolar"]
           [:div "sonora"]]]]]

       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa2.is-size-3 "[ƒ]"]
          [:div.r 
           [:div "fricativa acanalada"]
           [:div "alveolar"]
           [:div "sonora"]]]]]

       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[¢]"]
          [:div.r 
           [:div "lateral"]
           [:div "dental"]
           [:div "sonora"]]]]]

       ]]]]])

(defn ra3
  "Respuestas activity 3"
  []
  [:section.ra3
   [:li "Haga una cadena fonética, cambiando un solo rasgo a la vez."
    [:div "Las respuestas pueden variar; solo hay que asegurarse de que se cambie un solo rasgo a la vez."]]])

(defn ra4
  "Respuestas activity 4"
  []
  [:section.ra4
   [:li "El fonema /l/ tiene cuatro alófonos: " [ipa "[¡ ¢ l ù]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."

    [:ol.list.is-lower-alpha.is-size-5
     [:div.grid2
      [:div.l
       [:li
        [:div.ipa "[a" [red "¢"] "tÓsa]"]]
       [:li
        [:div.ipa "[E" [red "l"] "sÔGlo]"]]
       [:li
        [:div.ipa "[E" [red "ù"] "ÊÒko]"]]
       [:li
        [:div.ipa "[E" [red "¡"] "TapÒto]"]]
       [:li
        [:div.ipa "[aBRÔ" [red "l"] "]"]]
       [:li
        [:div.ipa "[a" [red "l"] "fÒ" [red "l"] "fa]"]]

       ]
      [:div.r
       [:li
        [:div.ipa "[a" [red "¢"] "tÒr]"]]
       [:li
        [:div.ipa "[E" [red "ù"] "Ì×rmo]"]]
       [:li
        [:div.ipa "[E" [red "l"] "nÓne]"]]
       [:li
        [:div.ipa "[×" [red "ù"] "Êe]"]]
       [:li
        [:div.ipa "[a" [red "l"] "kæªtaRa]"]]
       [:li
        [:div.ipa "[pÒ" [red "¢"] "ta]"]]
       ]]]]])
    
(defn ra5
  "Respuestas activity 5"
  []
  [:section.ra5
   [:li "El archifonema /R/ tiene varios alófonos: " [ipa "[r R ƒ X]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    [:div "El símbolo " [red "∗"] " indica que el sonido puede ser cualquiera de los siguientes sonidos: " [ipa "[r R ƒ X]"]]

    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:div.line-item 
        [:li
         [:div.ipa "[kÜªtÒ" [red "R"] "ÕpERa]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[pa" [red "∗"] "tÔ" [red "∗"] "]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[kom×" [red "∗"] "kÒ" [red "∗"] "ne]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[po" [red "∗"] "faBÕ" [red "∗"] "]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[pa" [red "∗"] "tikulÒ" [red "∗"] "]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[EstÒ" [red "R"] "atRasÒDo]"]]]
       ]
      [:div.r
       [:div.line-item 
        [:li
         [:div.ipa "[a" [red "∗"] "BolÓDa]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[po" [red "R"] "onÕ" [red "∗"] "]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[desj×" [red "∗"] "to]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[mol×" [red "R"] "E¢tRÔGo]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[apE" [red "∗"] "tÖRa]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[ra¸GÒ" [red "∗"] "se]"]]]
       ]
      ]

     ]]])

(defn ra6
  "Respuestas activity 6"
  []
  [:section.ra6
   [:li "Transcriba las siguientes palabras/frases fonéticamente:"
    (let [rows [["pronto" "[pRéªto]"]
                ["algo" "[ÒlGo]"]
                ["sarna" "[sÒrna] [sÒRna]"]
                ["bulla" "[bÖJa] [bÖYa]"]
                ["ajetreo" "[axetRÓ`o]"]
                ["honrar" "[ßnrÒr]"]
                ["abrirá" "[aBriRÒ]"]
                ["los reyes" "[lo¸rÓJes]"]
                ["clérigo" "[kl×RiGo]"]
                ["el rey es" "[elrÓj×s]"]
                ["ridículo" "[riDÔkulo]"]
                ["aullar" "[aUJÒr] [aUYÒr]"]
                ["alba" "[ÒlBa]"]
                ["periférico" "[pERif×Riko]"]
                ["portero" "[port×Ro] [poRt×Ro]"]
                ["lazrar" "[la¸rÒr]"]
                ["cotorrear" "[kotore`Òr]"]
                ["olla" "[Õja] [ÕYa]"]
                ["hielo" "[ÌÓlo]"]
                ["prórroga" "[pRÕroGa]"]]]
      (into [::ol.list.is-lower-alpha]
            (for [[a b] rows]
              [:div.list-item
               [:li
                [:div.grid2
                 [:div a]
                 [:div.ipa.red b]]]])))]])

(defn render-respuestas
  "Chapter 16 respuestas"
  []
  [:main.c16.respuestas.content
   [shared/fonetica-title "Capítulo 16 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]
     [ra5]
     [ra6]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li "Escriba el símbolo del alófono descrito por los siguientes rasgos."
    [shared/media-icon :r (rfe/href :fonetica.routes/c16-r) :same-session]

    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li [:div.grid2
             [:div.l 
              [:div "vibrante múltiple"]
              [:div "alveolar"]
              [:div "sonora"]]
             ]]
       [:li [:div.grid2
             [:div.l 
              [:div "lateral"]
              [:div "dental"]
              [:div "sonora"]]
             ]]
       [:li [:div.grid2
             [:div.l 
              [:div "lateral"]
              [:div "alveolar"]
              [:div "sonora"]]
             ]]
       [:li [:div.grid2
             [:div.l 
              [:div "lateral"]
              [:div "palatal"]
              [:div "sonora"]]
             ]]
       
       ]
      
      [:div.r
       [:li [:div.grid2
             [:div.l 
              [:div "vibrante simple"]
              [:div "alveolar"]
              [:div "sonora"]]
             ]]

       [:li [:div.grid2
             [:div.l 
              [:div "lateral"]
              [:div "palatalizada"]
              [:div "sonora"]]
             ]]

       [:li [:div.grid2
             [:div.l 
              [:div "fricativa acanalada"]
              [:div "alveolar"]
              [:div "sorda"]]
             ]]

       [:li [:div.grid2
             [:div.l 
              [:div "lateral"]
              [:div "interdental"]
              [:div "sonora"]]
             ]]
       
       
       ]
      ]]]])

(defn a2
  "Activity 2"
  []
  [:section.a2
   [:li "Indique los rasgos fonéticos para los siguientes alófonos."
    [shared/media-icon :r (rfe/href :fonetica.routes/c16-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[R]"]
          ]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[¡]"]
          ]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[Y]"]
          ]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[r]"]
          ]]]
       
       ]
      [:div.r
       
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[ù]"]
          ]]]

       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[l]"]
          ]]]

       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa2.is-size-3 "[ƒ]"]
          ]]]

       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa.is-size-3 "[¢]"]
          ]]]

       ]]]]])

(defn a3
  "Activity 3"
  []
  [:section.a3
   [:li "Haga una cadena fonética, cambiando un solo rasgo a la vez."
    [shared/media-icon :r (rfe/href :fonetica.routes/c16-r) :same-session]
    [:ol.list.is-lower-alpha
     [:li [:span.ipa "[r] > [ ] > [ ] > [ ]"]]
     [:li [:span.ipa "[Y] > [ ] > [ ] > [ ]"]]
     [:li [:span.ipa "[l] > [ ] > [ ] > [ ]"]]
     [:li [:span.ipa "[R] > [ ] > [ ] > [ ]"]]]]])

(defn a4
  "Activity 4"
  []
  [:section.a4
   [:li "El fonema /l/ tiene cuatro alófonos: " [ipa "[¡ ¢ l ù]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    [shared/media-icon :r (rfe/href :fonetica.routes/c16-r) :same-session]

    [:ol.list.is-lower-alpha.is-size-5
     [:div.grid2
      [:div.l
       [:li
        [:div.ipa "[a tÓsa]"]]
       [:li
        [:div.ipa "[E sÔGlo]"]]
       [:li
        [:div.ipa "[E ÊÒko]"]]
       [:li
        [:div.ipa "[E TapÒto]"]]
       [:li
        [:div.ipa "[aBRÔ ]"]]
       [:li
        [:div.ipa "[a fÒ fa]"]]

       ]
      [:div.r
       [:li
        [:div.ipa "[a tÒr]"]]
       [:li
        [:div.ipa "[e Ì×rmo]"]]
       [:li
        [:div.ipa "[e nÓne]"]]
       [:li
        [:div.ipa "[× Êe]"]]
       [:li
        [:div.ipa "[a kæªtaRa]"]]
       [:li
        [:div.ipa "[pÒ ta]"]]]]]]])
    
(defn a5
  "Activity 5"
  []
  [:section.a5
   [:li "El archifonema /R/ tiene varios alófonos: " [ipa "[r R ƒ X]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
        [shared/media-icon :r (rfe/href :fonetica.routes/c16-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:div.line-item 
        [:li
         [:div.ipa "[kÜªtÒ ÕpERa]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[pa tÔ ]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[com× kÒ ne]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[po faBÕ ]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[pa tikulÒ ]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[EstÒ atRasÒDo]"]]]
       ]
      [:div.r
       [:div.line-item 
        [:li
         [:div.ipa "[a BolÓDa]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[po onÕ ]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[desj× to]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[mol× E¢tRÔGo]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[apE tÖRa]"]]]
       [:div.line-item 
        [:li
         [:div.ipa "[ra¸GÒ se]"]]]
       ]
      ]

     ]]])

(defn a6
  "Activity 6"
  []
  [:section.a6
   [:li "Transcriba las siguientes palabras/frases fonéticamente:"
    [shared/media-icon :r (rfe/href :fonetica.routes/c16-r) :same-session]
    (let [rows [["pronto" "[pRéªto]"]
                ["algo" "[ÒlGo]"]
                ["sarna" "[sÒrna] [sÒRna]"]
                ["bulla" "[bÖJa] [bÖYa]"]
                ["ajetreo" "[axetRÓ`o]"]
                ["honrar" "[ßnrÒr]"]
                ["abrirá" "[aBriRÒ]"]
                ["los reyes" "[lo¸rÓJes]"]
                ["clérigo" "[kl×RiGo]"]
                ["el rey es" "[elrÓj×s]"]
                ["ridículo" "[riDÔkulo]"]
                ["aullar" "[aUJÒr] [aUYÒr]"]
                ["alba" "[ÒlBa]"]
                ["periférico" "[pERif×Riko]"]
                ["portero" "[port×Ro] [poRt×Ro]"]
                ["lazrar" "[la¸rÒr]"]
                ["cotorrear" "[cotore`Òr]"]
                ["olla" "[Õja] [ÕYa]"]
                ["hielo" "[ÌÓlo]"]
                ["prórroga" "[pRÕroGa]"]]]
      (into [::ol.list.is-lower-alpha]
            (for [[a _] rows]
              [:div.list-item
               [:li
                [:div a]]])))]])

(defn actividades
  "Chapter 16 actividades"
  []
  [:section.actividades.content
   [:h2 "ACTIVIDADES"]
   [:ol.actividades
    [a1]
    [a2]
    [a3]
    [a4]
    [a5]
    [a6]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C16-M1.mp3")] "La oposición entre los fonemas laterales /l " [ipa "Y"] "/."]
    [:li [shared/media-icon :audio (shared/file "C16-M2.mp3")] "La fonotáctica de los fonemas laterales /l " [ipa "Y"] "/."]
    [:li [shared/media-icon :audio (shared/file "C16-M3.mp3")] "La distribución complementaria del fonema /l/ del español (el Cuadro 16.2)."]
    [:li [shared/media-icon :audio (shared/file "C16-M4.mp3")] "Variantes dialectales del fonema /l/."]
    [:li [shared/media-icon :audio (shared/file "C16-M5.mp3")] "La aplicación de la regla de distribución complementaria del fonema /l/ de inglés."]
    [:li [shared/media-icon :audio (shared/file "C16-M6.mp3")] "La oposición entre los fonemas vibrantes /" [ipa "R"] " r/."]
    [:li [shared/media-icon :audio (shared/file "C16-M7.mp3")] "La fonotáctica de los fonemas vibrantes y el archifonema vibrante /" [ipa "R"] " r R/ (el Cuadro 16.15)."]
    [:li [shared/media-icon :audio (shared/file "C16-M8.mp3")] "La comparación entre la palabra española {cara} " [ipa "[kÒRa]"] " y la palabra portuguesa {cada} " [ipa "[kÒdA]"] "."]
    [:li [shared/media-icon :audio (shared/file "C16-M9.mp3")] "El vibrante simple en inglés y español."]
    [:li [shared/media-icon :audio (shared/file "C16-M10.mp3")] "Las variantes dialectales del fonema /r/."]
    [:li [shared/media-icon :audio (shared/file "C16-M11.mp3")] "El vibrante múltiple en sus varias posiciones fonotácticas."]
    [:li [shared/media-icon :audio (shared/file "C16-M12.mp3")] "La aplicación de la regla de distribución mixta del archifonema /R/ (el Cuadro 16.27)."]
    [:li [shared/media-icon :audio (shared/file "C16-M13.mp3")] "Variantes dialectales del archifonema /R/ ante consonantes."]
    [:li [shared/media-icon :audio (shared/file "C16-M14.mp3")] "Variantes dialectales del archifonema /R/ ante pausa."]
    [:li [shared/media-icon :ej (shared/file "C16-M15.mp3")] "Ejercicios de pronunciación: el fonema /l/."]
    [:li [shared/media-icon :ej (shared/file "C16-M16.mp3")] "Ejercicios de pronunciación: los vibrantes /" [ipa "R"] " r R/."]]])

(defn render
  "Chapter sixteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 16 — Los fonemas laterales y vibrantes"
    "E-Resources"]
   [:div.content.chapter-16
    [materiales]
    [actividades]]
   [shared/footer]])
