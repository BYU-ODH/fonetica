(ns fonetica.views.base
  "Container of the main view which facilitates CLJS app re-rendering and routing."
  (:require [fonetica.routes :refer [current-view]]))

(defn main-view
  "Placeholder to render the main view"
  []
  (@current-view))
