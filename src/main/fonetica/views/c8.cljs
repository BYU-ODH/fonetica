(ns fonetica.views.c8
  "Chapter 8"
  (:require [fonetica.views.components.shared :as shared :refer [ipa]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas Activity 1"
  []
  [:li.respuestas-a1 "Escriba el símbolo del fonema descrito por los siguientes rasgos."
   [:ol.list.is-lower-alpha
    [:div.grid2
     [:li
      [:div.grid2
       [:div 
        [:div "oclusivo"]
        [:div "bilabial"]
        [:div "sonoro"]]
       [:div.ipa.is-size-4 "/b/"]]]
     
     [:li
      [:div.grid2
       [:div
        [:div "nasal"]
        [:div "palatal"]
        [:div "sonoro"]]
       [:div.ipa.is-size-4 "/¦/"]]]

     [:li
      [:div.grid2
       [:div
        [:div "fricativo"]
        [:div "velar"]
        [:div "sordo"]]
       [:div.ipa.is-size-4 "/x/"]]]
     [:li
      [:div.grid2
       [:div
        [:div "fricativo"]
        [:div "alveolar"]
        [:div "sordo"]]
       [:div.ipa.is-size-4 "/s/"]]]

     [:li
      [:div.grid2
       [:div
        [:div "lateral"]
        [:div "alveolar"]
        [:div "sonoro"]]
       [:div.ipa.is-size-4 "/l/"]]]
     [:li
      [:div.grid2
       [:div
        [:div "oclusivo"]
        [:div "bilabial"]
        [:div "sordo"]]
       [:div.ipa.is-size-4 "/p/"]]]

     [:li
      [:div.grid2
       [:div
        [:div "oclusivo"]
        [:div "dental"]
        [:div "sordo"]]
       [:div.ipa.is-size-4 "/t/"]]]
     [:li
      [:div.grid2
       [:div
        [:div "anterior"]
        [:div "medio"]]
       [:div.ipa.is-size-4 "/e/"]]]]]])

(defn ra2
  "Respuestas Activity 2"
  []
  [:li.respuestas-a2 "Indique los rasgos fonológicos para los siguientes fonemas."
   [:ol.list.is-lower-alpha
    [:div.grid2
     [:li
      [:div.grid2
       [:div.ipa.is-size-4 "/m/"]
       [:div.r 
        [:div "nasal"]
        [:div "bilabial"]
        [:div "sonoro"]]]]

     [:li
      [:div.grid2
       [:div.ipa.is-size-4 "/f/"]
       [:div.r 
        [:div "fricativo"]
        [:div "labiodental"]
        [:div "sordo"]]]]

     [:li
      [:div.grid2
       [:div.ipa.is-size-4 "/k/"]
       [:div.r 
        [:div "oclusivo"]
        [:div "velar"]
        [:div "sordo"]]]]
     [:li
      [:div.grid2
       [:div.ipa.is-size-4 "/n/"]
       [:div.r 
        [:div "nasal"]
        [:div "alveolar"]
        [:div "sonoro"]]]]

     [:li
      [:div.grid2
       [:div.ipa.is-size-4 "/a/"]
       [:div.r 
        [:div "central"]
        [:div "abierto"]]]]
     [:li
      [:div.grid2
       [:div.ipa.is-size-4 "/R/"]
       [:div.r 
        [:div "vibrante simple"]
        [:div "alveolar"]
        [:div "sonoro"]]]]

     [:li
      [:div.grid2
       [:div.ipa.is-size-4 "/d/"]
       [:div.r 
        [:div "oclusivo"]
        [:div "dental"]
        [:div "sonoro"]]]]
     [:li
      [:div.grid2
       [:div.ipa.is-size-4 "/g/"]
       [:div.r 
        [:div "oclusivo"]
        [:div "velar"]
        [:div "sonoro"]]]]]]])

(defn ra3
  "Respuestas Activity 3"
  []
  [:li.respuestas-a3 "Encuentre un par mínimo que compruebe las siguientes oposiciones. "
   [:span.red "Las respuestas pueden variar; aquí solo se da un ejemplo para cada uno."]
   [:ol.list.is-lower-alpha
    [:div
     [:li 
      [:div.grid2   [:div.l.is-size-4 (ipa "/s/") "~" (ipa "/t/")] "{paso} {pato}"]]
     [:li 
      [:div.grid2   [:div.l.is-size-4 (ipa "/l/") "~" (ipa "/R/")] [:div.r "{palo} {paro}"]]]
     [:li 
      [:div.grid2   [:div.l.is-size-4 (ipa "/b/") "~" (ipa "/J/")] [:div.r "{haba} {halla}"]]]
     [:li 
      [:div.grid2   [:div.l.is-size-4 (ipa "/x/") "~" (ipa "/g/")] [:div.r "{ajo} {hago}"]]]
     [:li 
      [:div.grid2   [:div.l.is-size-4 (ipa "/n/") "~" (ipa "/¦/")] [:div.r "{panal} {pañal}"]]]
     [:li 
      [:div.grid2   [:div.l.is-size-4 (ipa "/p/") "~" (ipa "/b/")] [:div.r "{pala} {bala}"]]]
     [:li 
      [:div.grid2   [:div.l.is-size-4 (ipa "/e/") "~" (ipa "/o/")] [:div.r "{pelo} {polo}"]]]
     [:li 
      [:div.grid2   [:div.l.is-size-4 (ipa "/Î/") "~" (ipa "/r/")] [:div.r "{chico} {rico}"]]]
     [:li 
      [:div.grid2   [:div.l.is-size-4 (ipa "/i/") "~" (ipa "/e/")] [:div.r "{mi} {me}"]]]
     [:li 
      [:div.grid2   [:div.l.is-size-4 (ipa "/k/") "~" (ipa "/f/")] [:div.r "{caja} {faja}"]]]]]])

(defn ra4
  "Respuestas Activity 4"
  []
  [:li.respuestas-a4 "Los siguientes datos fonéticos representan neutralizaciones. Para cada caso, identifique los
fonemas que se neutralizan, el tipo de neutralización y el resultado de la neutralización."
   [:ol.list.is-lower-alpha
    [:li [:div  (ipa "[àmbæNko] [kÒma] [ànlÒGo] [kÒna]")]
     [:p "Demuestra la neutralización parcial/sincrónica entre " (ipa "/m/ y /n/") " que resulta en el archifonema /N/."]]
    
    [:li [:div  (ipa "[kÒTa] [kÒsa]") " {caza casa} y [kÒsa] {caza casa}"]
     [:p "Demuestra la neutralización total/diacrónica/histórica entre " (ipa "/T/ y /s/") " que resulta en el fonema /s/ y la pérdida del fonema " (ipa "/T/") ". El fenómeno se llama " [:em "seseo."]]]
    
    [:li [:div (ipa "[estuDjÒR] [cÕRo] [estuDjÒr] [cÕro]")]
     [:p "Demuestra la neutralización parcial/sincrónica entre " (ipa "/R/ y /r/") " que resulta en el archifonema /R/."]]
    
    [:li [:div (ipa "[ÒJa] [ÒYa] {haya halla} y [ÒJa]") " {haya halla}"]
     [:p "Demuestra la neutralización total/diacrónica/histórica entre " (ipa "/J/ y /Y/") " que resulta en el fonema " (ipa "/j/") " y la pérdida del fonema " (ipa "/Y/") ". El fenómeno se llama " [:em "yeísmo."]]]]])

(defn render-respuestas
  "Chapter 8 respuestas"
  []
  [:main.c8.respuestas.content
   [shared/fonetica-title "Capítulo 8 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]]]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:li "Escriba el símbolo del fonema descrito por los siguientes rasgos." [shared/media-icon :r (rfe/href :fonetica.routes/c8-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:li 
       [:div "oclusivo"]
       [:div "bilabial"]
       [:div "sonoro"]]
      [:li
       [:div "fricativo"]
       [:div "velar"]
       [:div "sordo"]]
      [:li 
       [:div "lateral"]
       [:div "alveolar"]
       [:div "sonoro"]]
      [:li 
       [:div "fricativo"]
       [:div "alveolar"]
       [:div "sordo"]]
      [:li 
       [:div "oclusivo"]
       [:div "dental"]
       [:div "sordo"]]
      [:li 
       [:div "oclusivo"]
       [:div "bilabial"]
       [:div "sordo"]]
      [:li 
       [:div "nasal"]
       [:div "palatal"]
       [:div "sonoro"]]
      [:li 
       [:div "medio"]
       [:div "anterior"]]]]]])

(defn a2
  "Actividades 2"
  []
  [:section.a2
   [:li "Escriba el símbolo del fonema descrito por los siguientes rasgos." [shared/media-icon :r (rfe/href :fonetica.routes/c8-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:li (ipa "/m/")]
      [:li (ipa "/f/")]
      [:li (ipa "/k/")]
      [:li (ipa "/n/")]
      [:li (ipa "/a/")]
      [:li (ipa "/R/")]
      [:li (ipa "/d/")]
      [:li (ipa "/g/")]]]]])

(defn a3
  "Actividades 3"
  []
  [:section.a3
   [:li "Encuentre un par mínimo que compruebe las siguientes oposiciones." [shared/media-icon :r (rfe/href :fonetica.routes/c8-r) :same-session]
    [:ol.list.is-lower-alpha
     [:li (ipa "/s/") "~" (ipa "/t/")]
     [:li (ipa "/l/") "~" (ipa "/R/")]
     [:li (ipa "/b/") "~" (ipa "/J/")]
     [:li (ipa "/x/") "~" (ipa "/g/")]
     [:li (ipa "/n/") "~" (ipa "/¦/")]
     [:li (ipa "/p/") "~" (ipa "/b/")]
     [:li (ipa "/e/") "~" (ipa "/o/")]
     [:li (ipa "/Î/") "~" (ipa "/r/")]
     [:li (ipa "/i/") "~" (ipa "/e/")]
     [:li (ipa "/k/") "~" (ipa "/f/")]]]])

(defn a4
  "Actividades 4"
  []
  [:section.a4
   [:li "Encuentre un par mínimo que compruebe las siguientes oposiciones." [shared/media-icon :r (rfe/href :fonetica.routes/c8-r) :same-session]
    [:ol.list.is-lower-alpha
     [:li (ipa "[àmbæNko] [kÒma] [ànlÒGo] [kÒna]")]
     [:li (ipa "[kÒTa] [kÒsa]") " {caza casa} y " (ipa "[kÒsa]") " {caza casa}"]
     [:li (ipa "[estuDjÒR] [cÕRo] [estuDjÒr] [cÕro]")]
     [:li (ipa "[ÒJa] [ÒYa] {haya halla} y [ÒJa]") " {haya halla}"]]]])

(defn actividades
  "Chapter 8 actividades"
  []
   [:section.actividades.content
    [:h2 "ACTIVIDADES"]
    [:ol.actividades
     [a1]
     [a2]
     [a3]
     [a4]]])

(defn fuentes
  "The fuentes section"
  []
  [:section.fuentas
   [:h3 "FUENTES DE LOS EJERCICIOS DE TRANSCRIPCIÓN"]

   [:ol
    [:li "“Envidia,” " [:em "Historias de la Artámila"] " Ana María Matute"]
    [:li "“Los alambradores,” " [:em "Historias de la Artámila"] " Ana María Matute"]
    [:li "“A la deriva,” " [:em "Cuentos de amor"] " de locura y de muerte, Horacio Quiroga"]
    [:li "“La fotografía,” " [:em "Después del temporal"] " Enrique Amorim"]
    [:li "“Rosarito,” " [:em "Femeninas: Seis historias amorosas"] " Ramón María del Valle-Inclán"]
    [:li "“Chac Mool,” " [:em "Los días enmascarados"] " Carlos Fuentes"]
    [:li [:em "Platero y yo"] ", Juan Ramón Jiménez"]
    [:li [:em "San Manuel Bueno, mártir"] ", Miguel de Unamuno"]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C8-M1.mp3")] "Ejemplos de pares mínimos."]
    [:li [shared/media-icon :audio (shared/file "C8-M2.mp3")] "Oposiciones consonánticas basadas en el modo de articulación."]
    [:li [shared/media-icon :audio (shared/file "C8-M3.mp3")] "Oposiciones consonánticas basadas en el lugar de articulación."]
    [:li [shared/media-icon :audio (shared/file "C8-M4.mp3")] "Oposiciones consonánticas basadas en el estado de las cuerdas vocales."]
    [:li [shared/media-icon :audio (shared/file "C8-M5.mp3")] "Oposiciones consonánticas del fonema /Ê/."]
    [:li [shared/media-icon :audio (shared/file "C8-M6.mp3")] "Oposiciones vocálicas basadas en el modo de articulación."]
    [:li [shared/media-icon :audio (shared/file "C8-M7.mp3")] "Oposiciones vocálicas basadas en el lugar de articulación."]
    [:li [shared/media-icon :audio (shared/file "C8-M8.mp3")] "Oposiciones vocálicas del fonema /a/."]
    [:li [shared/media-icon :audio (shared/file "C8-M9.mp3")] "Oposición entre /m/ y /n/ en posición inicial de sílaba."]
    [:li [shared/media-icon :audio (shared/file "C8-M10.mp3")] "Neutralización entre las consonantes nasales en posición final de sílaba."]
    [:li [shared/media-icon :audio (shared/file "C8-M11.mp3")] "Oposición entre /R/ y /r/ en posición inicial de sílaba."]
    [:li [shared/media-icon :audio (shared/file "C8-M12.mp3")] "Neutralización entre las consonantes vibrantes en posición final de sílaba."]
    [:li [shared/media-icon :audio (shared/file "C8-M13.mp3")] "Distinción y seseo."]
    [:li [shared/media-icon :audio (shared/file "C8-M14.mp3")] "Distinción y yeísmo."]]])

(defn render
  "Chapter eight view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 8 — La relación entre fonemas: la oposición y la neutralización"
    "E-Resources"]
   [:div.content.chapter-8
    [fuentes]
    [materiales]
    [actividades]]
   
   [shared/footer]])
