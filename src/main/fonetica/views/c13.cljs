(ns fonetica.views.c13
  "Chapter 13"
  (:require [fonetica.views.components.shared :as shared :refer [ipa femp red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas Actividades 1"
  []
  [:section.a1
   [:li "Escriba el símbolo del alófono descrito por los siguientes rasgos."
    
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:div.list-item [:li [:div.grid2
                             [:div.l [:div "fricativa"]
                              [:div "velar"]
                              [:div "sonora"]]
                             [:div.ipa.is-size-4 "[G]"]]]]
       
       [:div.list-item [:li [:div.grid2
                             [:div.l 
                              [:div "oclusiva"]
                              [:div "bilabial"]
                              [:div "sorda"]]
                             [:div.ipa.is-size-4 "[p]"]]]]
       
       [:div.list-item [:li [:div.grid2
                             [:div.l 
                              [:div "oclusiva"]
                              [:div "dental"]
                              [:div "sonora"]]
                             [:div.ipa.is-size-4 "[d]"]]]]
       
       [:div.list-item [:li [:div.grid2
                             [:div.l 
                              [:div "oclusiva"]
                              [:div "velar"]
                              [:div "sorda"]]
                             [:div.ipa.is-size-4 "[k]"]]]]]
      [:div.r
       [:div.list-item [:li [:div.grid2
                             [:div.l 
                              [:div "fricativa"]
                              [:div "bilabial"]
                              [:div "sonora"]]
                             [:div.ipa.is-size-4 "[B]"]]]]
       [:div.list-item [:li [:div.grid2
                             [:div.l
                              [:div "oclusiva"]
                              [:div "dental"]
                              [:div "sorda"]]
                             [:div.ipa.is-size-4 "[t]"]]]]
       [:div.list-item [:li [:div.grid2
                             [:div.l 
                              [:div "fricativa"]
                              [:div "interdental"]
                              [:div "sonora"]]
                             [:div.ipa.is-size-4 "[D]"]]]]
       [:div.list-item [:li [:div.grid2
                             [:div.l [:div "oclusiva"]
                              [:div "bilabial"]
                              [:div "sonora"]]
                             [:div.ipa.is-size-4 "[b]"]]]]]]]]])

(defn ra2
  "Respuestas Actividades 2"
  []
  [:section.a2
   [:li "Indique los rasgos fonéticos para los siguientes alófonos."
    
    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {:gap "1fl"}}
      [:div.l
       [:div.list-item [:li
                        [:div.grid2
                         [ipa "[g]"]
                         [:div.r2
                          [:div "oclusiva"]
                          [:div "velar"]
                          [:div "sonora"]]]]]
       [:div.list-item [:li
                        [:div.grid2
                         [ipa "[D]"]
                         [:div.r2
                          [:div "fricativa"]
                          [:div "interdental"]
                          [:div "sonora"]]]]]
       [:div.list-item [:li
                        [:div.grid2
                         [ipa "[B]"]
                         [:div.r2
                          [:div "fricativa"]
                          [:div "bilabial"]
                          [:div "sonora"]]]]]
       [:div.list-item [:li
                        [:div.grid2
                         [ipa "[d]"]
                         [:div.r2
                          [:div "oclusiva"]
                          [:div "dental"]
                          [:div "sonora"]]]]]
       ]
      [:div.r
       [:div.list-item [:li
                        [:div.grid2
                         [ipa "[G]"]
                         [:div.r2
                          [:div "fricativa"]
                          [:div "velar"]
                          [:div "sonora"]]]]]
       [:div.list-item [:li
                        [:div.grid2
                         [ipa "[b]"]
                         [:div.r2
                          [:div "oclusiva"]
                          [:div "bilabial"]
                          [:div "sonora"]]]]]
       [:div.list-item [:li
                        [:div.grid2
                         [ipa "[t]"]
                         [:div.r2
                          [:div "oclusiva"]
                          [:div "dental"]
                          [:div "sorda"]]]]]
       [:div.list-item [:li
                        [:div.grid2
                         [ipa "[k]"]
                         [:div.r2
                          [:div "oclusiva"]
                          [:div "velar"]
                          [:div "sorda"]]]]]]]]]])

(defn ra3
  "Respuestas Actividades 3"
  []
  [:section.a3
   [:li "Haga una cadena fonética, cambiando un solo rasgo a la vez."
    [:div {:style {:margin-top "1em"}}
     "Las respuestas pueden variar; solo hay que asegurarse de que se cambie un solo rasgo a la vez."]]])

(defn ra4
  "Respuestas Actividades 4"
  []
  [:section.a4
   [:li "Para las siguientes configuraciones de VOT (inicio de vibración de las cuerdas vocales), identifique los sonidos y el idioma de los sonidos así producidos."
    [:ol.list.is-lower-alpha
     [:div.list-item
      [:li [:div.grid2
            [:img {:src (shared/file "C13-A4a.jpg")}]
            [:div.red "Los sonidos oclusivos sordos [p t k] del español."]]]]
     [:div.list-item
      [:li [:div.grid2
            [:img {:src (shared/file "C13-A4b.jpg")}]
            [:div.red "Los sonidos oclusivos sonoros [b d g] del inglés."]]]]
     [:div.list-item
      [:li [:div.grid2
            [:img {:src (shared/file "C13-A4c.jpg")}]
            [:div.red "Los sonidos oclusivos sordos [p t k] del inglés."]]]]
     [:div.list-item
      [:li [:div.grid2
            [:img {:src (shared/file "C13-A4d.jpg")}]
            [:div.red "Los sonidos oclusivos sonoros [b d g] del español."]]]]]]])

(defn ra5
  "Respuestas Actividades 5"
  []
  [:section.a5
   [:li "El fonema /b/ tiene dos alófonos: " [ipa "[b]"] " y " [ipa "[B]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li.ipa "[Ò" [red "B"] "siDe]"]
       [:li.ipa "[a" [red "B"] "Ò" [red "B"] "as]"]
       [:li.ipa "[" [red "b"] "aIla" [red "B"] "]"]
       [:li.ipa "[Em" [red "b"] "ÔDja]"]
       [:li.ipa "[tæm" [red "b"] "Óno]"]
       [:li.ipa "[o" [red "B"] "iGÒ a]"]]
      [:div.r
       [:li.ipa "[a" [red "B"] "Ôl]"]
       [:li.ipa "[tinjÓ" [red "B"] "las]"]
       [:li.ipa "[×l×¸" [red "B"] "jìm" [red "b"] "Óno]"]
       [:li.ipa "[kæm" [red "b"] "jo]"]
       [:li.ipa "[kala" [red "B"] "Òsas]"]
       [:li.ipa "[×Resum" [red "b"] "kalÒo]"]]]]]])

(defn ra6
  "Respuestas Actividades 6"
  []
  [:section.a6
   [:li "El fonema /d/ tiene dos alófonos: " [ipa "[d]"] " y " [ipa "[D]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li.ipa "[ie" [red "d"] "`alisÒ" [red "D"] "o]"]
       [:li.ipa "[Þª" [red "d"] "ustrjÒl]"]
       [:li.ipa "[e" [red "D"] "ukÒ" [red "D"] "o]"]
       [:li.ipa "[" [red "d"] "Õªe" [red "d"] "e]"]
       [:li.ipa "[" [red "d"] "e" [red "D"] "Õªe" [red "d"] "]"]
       [:li.ipa "[a¢kÒ¢" [red "d"] "e]"]]
      [:div.r
       [:li.ipa "[" [red "d"] "Ó" [red "D"] "os]"]
       [:li.ipa "[" [red "d"] "e" [red "D"] "ikÒ" [red "D"] "o]"]
       [:li.ipa "[tÕ¢" [red "d"] "o]"]
       [:li.ipa "[alGÖª" [red "d"] "Ôa]"]
       [:li.ipa "[" [red "d"] "i" "putÒ" [red "D"] "o]"]
       [:li.ipa "[alre" [red "D"] "e" [red "D"] "Õr]"]]]]]])

(defn ra7
  "Respuestas Actividades 7"
  []
  [:section.a7
   [:li "El fonema /g/ tiene dos alófonos: " [ipa "[g]"] " y " [ipa "[G]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    
    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {:gap 0}}
      [:div.l
       [:li.ipa "[kßN" [red "g"] "Re" [red "G"] "Òr]"]
       [:li.ipa "[a" [red "G"] "Riku¢tÖRa]"]]
      [:div.r
       [:li.ipa "[" [red "g"] "Ræªde]"]
       [:li.ipa "[lÞN" [red "g"] "wÔsta]"]]
      [:div.l2
       [:li.ipa "[a" [red "G"] "was×Ro]"]
       [:li.ipa "[rEk<tÜN" [red "g"] "ulÒr]"]
       [:li.ipa "[Ò" [red "G"] "ila]"]
       [:li.ipa "[aBolìN" [red "g"] "o]"]]
      [:div.r2
       [:li.ipa "[" [red "g"] "ÒR" [red "G"] "aRas]"]
       [:li.ipa "[plj×" [red "G"] "o]"]
       [:li.ipa "[a" [red "G"] "RidÖlse]"]
       [:li.ipa "[" [red "g"] "inÓ`o]"]]]]]])

(defn ra8
  "Respuestas Actividades 8"
  []
  [:section.a8
   [:li "Transcriba las siguientes palabras/frases fonéticamente:"
    
    [:ol.list.is-lower-alpha
     [:div.grid2
      
      [:div.l
       [:li 
        [:div.grid2 
         [:div.l "técnico"] 
         [:div.r.red.ipa "[t×k<niko]"]]]
       [:li 
        [:div.grid2 
         [:div.l "novato"] 
         [:div.r.red.ipa "[noBÒto]"]]]
       [:li 
        [:div.grid2 
         [:div.l "aséptico"] 
         [:div.r.red.ipa "[as×p<tiko]"]]]
       [:li 
        [:div.grid2 
         [:div.l "peldaño"] 
         [:div.r.red.ipa "[pE¢dÒ¦o]"]]]
       [:li 
        [:div.grid2 
         [:div.l "algorítmico"] 
         [:div.r.red.ipa "[alGoRÔt<miko]"]]]
       [:li 
        [:div.grid2 
         [:div.l "agente"] 
         [:div.r.red.ipa "[ax×ªte]"]]]
       [:li 
        [:div.grid2 
         [:div.l "albaricoque"] 
         [:div.r.red.ipa "[alBaRikÕke]"]]]
       [:li 
        [:div.grid2 
         [:div.l "quesadilla"] 
         [:div.r.red.ipa "[kesaDÔJa]"]]]
       [:li 
        [:div.grid2 
         [:div.l "avestruz"] 
         [:div.r.red.ipa "[aBestRÖs]"]]]
       [:li 
        [:div.grid2 
         [:div.l "etnocéntrico"] 
         [:div.r.red.ipa "[×t<nosìªtRiko]"]]]]
      [:div.r
       [:li 
        [:div.grid2 
         [:div.l "panegírico"] 
         [:div.r.red.ipa "[panexÔRiko]"]]]
       [:li 
        [:div.grid2 
         [:div.l "actor"] 
         [:div.r.red.ipa "[ak<tÕr]"]]]
       [:li 
        [:div.grid2 
         [:div.l "obvio"] 
         [:div.r.red.ipa "[ÕBjo]"]]]
       [:li 
        [:div.grid2 
         [:div.l "atlántico"] 
         [:div.r.red.ipa "[at<lÒªtiko]"]]]
       [:li 
        [:div.grid2 
         [:div.l "septiembre"] 
         [:div.r.red.ipa "[s×p<tjìmbRe]"]]]
       [:li 
        [:div.grid2 
         [:div.l "taladro"] 
         [:div.r.red.ipa "[talÒDRo]"]]]
       [:li 
        [:div.grid2 
         [:div.l "ogro"] 
         [:div.r.red.ipa "[ÕGRo]"]]]
       [:li 
        [:div.grid2 
         [:div.l "peripecia"] 
         [:div.r.red.ipa "[pERipÓsja]"]]]
       [:li 
        [:div.grid2 
         [:div.l "abstracto"] 
         [:div.r.red.ipa "[aBstRÒk<to]"]]]
       [:li 
        [:div.grid2 
         [:div.l "precoz"] 
         [:div.r.red.ipa "[pRekÕs]"]]]]]]]])

(defn render-respuestas
  "Chapter 13 respuestas"
  []
  [:main.c13.respuestas.content
   [shared/fonetica-title "Capítulo 13 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]
     [ra5]
     [ra6]
     [ra7]
     [ra8]]]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:li "Escriba el símbolo del alófono descrito por los siguientes rasgos."
    [shared/media-icon :r (rfe/href :fonetica.routes/c13-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {:gap 0}}
      [:li [:div 
            [:div "fricativa"]
            [:div "velar"]
            [:div "sonora"]]]
      [:li [:div 
            [:div "oclusiva"]
            [:div "bilabial"]
            [:div "sorda"]]]
      
      [:div.l
       [:li [:div 
             [:div "oclusiva"]
             [:div "dental"]
             [:div "sonora"]]]
       
       [:li [:div 
             [:div "oclusiva"]
             [:div "velar"]
             [:div "sorda"]]]
       [:li [:div 
             [:div "fricativa"]
             [:div "bilabial"]
             [:div "sonora"]]]]
      [:div.r
       [:li [:div 
             [:div "oclusiva"]
             [:div "dental"]
             [:div "sorda"]]]
       
       [:li [:div 
             [:div "fricativa"]
             [:div "interdental"]
             [:div "sonora"]]]
       [:li [:div 
             [:div "oclusiva"]
             [:div "bilabial"]
             [:div "sonora"]]]]]]]])

(defn a2
  "Actividades 2"
  []
  [:section.a2
   [:li "Indique los rasgos fonéticos para los siguientes alófonos."
    [shared/media-icon :r (rfe/href :fonetica.routes/c13-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {:gap 0}}
      [:div.l
       [:li [ipa "[g]"]]
       [:li [ipa "[D]"]]
       [:li [ipa "[B]"]]
       [:li [ipa "[d]"]]]
      [:div.r
       [:li [ipa "[G]"]]
       [:li [ipa "[b]"]]
       [:li [ipa "[t]"]]
       [:li [ipa "[k]"]]]]]]])

(defn a3
  "Actividades 3"
  []
  [:section.a3
   [:li "Haga una cadena fonética, cambiando un solo rasgo a la vez."
    [shared/media-icon :r (rfe/href :fonetica.routes/c13-r) :same-session]
    [:ol.list.is-lower-alpha
     [:li.ipa "[D] > [ ] > [ ] > [ ]"]
     [:li.ipa "[p] > [ ] > [ ] > [ ]"]
     [:li.ipa "[B] > [ ] > [ ] > [ ]"]
     [:li.ipa "[g] > [ ] > [ ] > [ ]"]]]])

(defn a4
  "Actividades 4"
  []
  [:section.a4
   [:li "Para las siguientes configuraciones de VOT (inicio de vibración de las cuerdas vocales), identifique los sonidos y el idioma de los sonidos así producidos."
    [shared/media-icon :r (rfe/href :fonetica.routes/c13-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.list-item
      [:li [:img {:src (shared/file "C13-A4a.jpg")}]]]
     [:div.list-item
      [:li [:img {:src (shared/file "C13-A4b.jpg")}]]]
     [:div.list-item
      [:li [:img {:src (shared/file "C13-A4c.jpg")}]]]
     [:div.list-item
      [:li [:img {:src (shared/file "C13-A4d.jpg")}]]]]]])

(defn a5
  "Actividades 5"
  []
  [:section.a5
   [:li "El fonema /b/ tiene dos alófonos: " [ipa "[b]"] " y " [ipa "[B]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    [shared/media-icon :r (rfe/href :fonetica.routes/c13-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li [ipa "[Ò siDe]"]]
       [:li [ipa "[a lÒ as]"]]
       [:li [ipa "[ aIla a]"]]
       [:li [ipa "[Em ÔDja]"]]
       [:li [ipa "[tæm wÓno]"]]
       [:li [ipa "[o liGÒ a]"]]]
      [:div.r
       [:li [ipa "[a RÔl]"]]
       [:li [ipa "[tinjÓ las]"]]
       [:li [ipa "[×l×¸ jìm wÓno]"]]
       [:li [ipa "[kæm jo]"]]
       [:li [ipa "[kala Òsas]"]]
       [:li [ipa "[×Resum akalÒo]"]]]]]]])

(defn a6
  "Actividades 6"
  []
  [:section.a6
   [:li "El fonema /d/ tiene dos alófonos: " [ipa "[d]"] " y " [ipa "[D]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    [shared/media-icon :r (rfe/href :fonetica.routes/c13-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li.ipa "[i e`alisÒ o]"]
       [:li.ipa "[Þª ustrjÒl]"]
       [:li.ipa "[e ukÒ o]"]
       [:li.ipa "[ Õª e]"]
       [:li.ipa "[ e Õª e]"]
       [:li.ipa "[a¢kÒ¢ e]"]]
      [:div.r
       [:li.ipa "[ Ó os]"]
       [:li.ipa "[ e ikÒ o]"]
       [:li.ipa "[tÕ¢ o]"]
       [:li.ipa "[alGÖª Ôa]"]
       [:li.ipa "[ iputÒ o]"]
       [:li.ipa "[alre e Õr]"]]]]]])

(defn a7
  "Actividades 7"
  []
  [:section.a7
   [:li "El fonema /g/ tiene dos alófonos: " [ipa "[g]"] " y " [ipa "[G]"] ". Llene el espacio en blanco de las siguientes transcripciones con el símbolo correcto según la regla."
    [shared/media-icon :r (rfe/href :fonetica.routes/c13-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {:gap 0}}
      [:div.l
       [:li.ipa "[kßN Re Òr]"]
       [:li.ipa "[a Riku¢tÖRa]"]
       [:li.ipa "[ Ræªde]"]]
      [:div.r
       [:li.ipa "[lÞN wÔsta]"]
       [:li.ipa "[a was×Ro]"]
       [:li.ipa "[rEk<tÜN ulÒr]"]]
      [:div.l2
       [:li.ipa "[Ò ila]"]
       [:li.ipa "[aBolìN o]"]
       [:li.ipa "[ ÒR aRas]"]]
      [:div.r2
       [:li.ipa "[plj× o]"]
       [:li.ipa "[a RidÖlse]"]
       [:li.ipa "[ inÓ`o]"]]]]]])

(defn a8
  "Actividades 8"
  []
  [:section.a8
   [:li "Transcriba las siguientes palabras/frases fonéticamente:"
    [shared/media-icon :r (rfe/href :fonetica.routes/c13-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li "técnico"] 
       [:li "novato"] 
       [:li "aséptico"] 
       [:li "peldaño"] 
       [:li "algorítmico"] 
       [:li "agente"] 
       [:li "albaricoque"] 
       [:li "quesadilla"] 
       [:li "avestruz"] 
       [:li "etnocéntrico"]]
      [:div.r
       [:li "panegírico"]
       [:li "actor"]
       [:li "obvio"]
       [:li "atlántico"]
       [:li "septiembre"]
       [:li "taladro"]
       [:li "ogro"]
       [:li "peripecia"]
       [:li "abstracto"]
       [:li "precoz"]]]]]])

(defn actividades
  "Chapter 13 actividades"
  []
  [:section.actividades.content
   [:h2 "ACTIVIDADES"]
   [:ol.actividades
    [a1]
    [a2]
    [a3]
    [a4]
    [a5]
    [a6]
    [a7]
    [a8]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C13-M1.mp3")] "Ejemplos de fonemas oclusivos en posición inicial e intervocálica."]
    [:li [shared/media-icon :audio (shared/file "C13-M2.mp3")] "Ejemplos de oclusivos sordos en posición final de sílaba interior de palabra."]
    [:li [shared/media-icon :audio (shared/file "C13-M3.mp3")] "Ejemplos de la neutralización de fonemas oclusivos en posición implosiva."]
    [:li [shared/media-icon :video (shared/file "C13-M4.mp4")] "Video de la articulación de la oclusiva sorda sin aspiración y con ella."]
    [:li [shared/media-icon :audio (shared/file "C13-M5.mp3")] "La pronunciación de {papa} en español y de {pie} en inglés."]
    [:li [shared/media-icon :audio (shared/file "C13-M6.mp3")] "La pronunciación de {ven} en español y de {Ben} en inglés."]
    [:li [shared/media-icon :audio (shared/file "C13-M7.mp3")] "La pronunciación del alófono fricativo " [ipa "[D]"] " en tres contextos diferentes."]
    [:li [shared/media-icon :audio (shared/file "C13-M8.mp3")] "La pronunciación de variantes inglesas del fonema /p/."]
    [:li [shared/media-icon :audio (shared/file "C13-M9.mp3")] "La pronunciación de variantes inglesas del fonema /t/."]
    [:li [shared/media-icon :audio (shared/file "C13-M10.mp3")] "La pronunciación de la palabra {natural} en inglés y en español."]
    [:li [shared/media-icon :audio (shared/file "C13-M11.mp3")] "La interferencia del inglés en la pronunciación del fonema /t/ intervocálico."]
    [:li [shared/media-icon :audio (shared/file "C13-M12.mp3")] "La realización del fonema /k/ en inglés."]
    [:li [shared/media-icon :audio (shared/file "C13-M13.mp3")] "La realización de la " [ipa "[b]"] " oclusiva en español."]
    [:li [shared/media-icon :audio (shared/file "C13-M14.mp3")] "La realización de la " [ipa "[B]"] " fricativa en español."]
    [:li [shared/media-icon :audio (shared/file "C13-M15.mp3")] "La realización de la " [ipa "[B]"] " fricativa y la " [ipa "[B]"] " cuasifricativa en español."]
    [:li [shared/media-icon :audio (shared/file "C13-M16.mp3")] "La realización de la " [ipa "[d]"] " oclusiva en español."]
    [:li [shared/media-icon :audio (shared/file "C13-M17.mp3")] "La realización de la " [ipa "[D]"] " fricativa en español."]
    [:li [shared/media-icon :audio (shared/file "C13-M18.mp3")] "La realización de la " [ipa "[D]"] " fricativa y la " [ipa "[D]"] " cuasifricativa en español."]
    [:li [shared/media-icon :audio (shared/file "C13-M19.mp3")] "La interferencia del inglés en la pronunciación del fonema /d/ intervocálico."]
    [:li [shared/media-icon :audio (shared/file "C13-M20.mp3")] "La realización de la " [ipa "[g]"] " oclusiva en español."]
    [:li [shared/media-icon :audio (shared/file "C13-M21.mp3")] "La realización de la " [ipa "[G]"] " fricativa en español."]
    [:li [shared/media-icon :audio (shared/file "C13-M22.mp3")] "La realización de la " [ipa "[G]"] " fricativa y la " [ipa "[G]"] " cuasifricativa en español."]
    [:li [shared/media-icon :audio (shared/file "C13-M23.mp3")] "Consejos prácticos para la pronunciación de la " [ipa "[G]"] " fricativa en español."]
    [:li [shared/media-icon :ej (shared/file "C13-M24.mp3")] "Ejercicios de pronunciación: los fonemas oclusivos sordos /p t k/."]
    [:li [shared/media-icon :ej (shared/file "C13-M25.mp3")] "Ejercicios de pronunciación: los fonemas oclusivos sonoros /b d g/."]
    [:li [shared/media-icon :ej (shared/file "C13-M26.mp3")] "Ejercicios de pronunciación: los fonemas oclusivos dentales /t d/."]]])

(defn render
  "Chapter thirteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 13 — Los fonemas oclusivos"
    "E-Resources"]
   [:div.content.chapter-13
    [:section.e-resources
     [:h2 "E-Resources"]]
    [materiales]
    [actividades]]
   [shared/footer]])
