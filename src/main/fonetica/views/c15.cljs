(ns fonetica.views.c15
  "Chapter 15"
  (:require [fonetica.views.components.shared :as shared :refer [ipa femp red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Actividades 1"
  []
  [:section.ra1
   [:li "Escriba el símbolo del alófono descrito por los siguientes rasgos."

    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li [:div.grid2
             [:div.l [:div "nasal"]
              [:div "velar"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa
              "[N]"]]]
       [:li [:div.grid2
             [:div 
              [:div "nasal"]
              [:div "dental"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa
              "[ª]"
              ]]]
       [:li [:div.grid2
             [:div 
              [:div "nasal"]
              [:div "alveolar"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa
              "[n]"
              ]]]
       [:li [:div.grid2
             [:div 
              [:div "nasal"]
              [:div "bilabial"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa
              "[m]"
              ]]]]
      
      [:div.r
       [:li [:div.grid2
             [:div 
              [:div "nasal"]
              [:div "labiodental"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa
              "[M]"
              ]]]
       [:li [:div.grid2
             [:div 
              [:div "nasal"]
              [:div "palatal"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa
              "[¦]"
              ]]]
       [:li [:div.grid2
             [:div 
              [:div "nasal"]
              [:div "interdental"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa
              "[©]"
              ]]]
       [:li [:div.grid2
             [:div 
              [:div "nasal"]
              [:div "palatalizada"]
              [:div "sonora"]]
             [:div.r.is-size-3.ipa
              "[ø]"
              ]]]
       
       ]
      ]]]])

(defn ra2
  "Actividades 2"
  []
  [:section.ra2
   [:li "Indique los rasgos fonéticos para los siguientes alófonos."

    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {:gap 0}}
      [:div.l
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa "[ø]"]
          [:div.r
           [:div "nasal"]
           [:div "palatalizada"]
           [:div "sonora"]]]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa 
           "[M]"]
          [:div.r
           [:div "nasal"]
           [:div "labiodental"]
           [:div "sonora"]]]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa 
           "[m]"]
          [:div.r
           [:div "nasal"]
           [:div "bilabial"]
           [:div "sonora"]]]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa 
           "[n]"]
          [:div.r
           [:div "nasal"]
           [:div "alveolar"]
           [:div "sonora"]]]]]
       ]
      [:div.r
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa 
           "[ª]"]
          [:div.r
           [:div "nasal"]
           [:div "dental"]
           [:div "sonora"]]]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa 
           "[¦]"]
          [:div.r
           [:div "nasal"]
           [:div "palatal"]
           [:div "sonora"]]]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa 
           "[©]"]
          [:div.r
           [:div "nasal"]
           [:div "interdental"]
           [:div "sonora"]]]]]
       [:div.line-item
        [:li
         [:div.grid2
          [:div.ipa 
           "[N]"]
          [:div.r
           [:div "nasal"]
           [:div "velar"]
           [:div "sonora"]]]]]
       ]]]]])

(defn ra3
  "Actividades 3"
  []
  [:section.ra3
   [:li "Haga una cadena fonética, cambiando un solo rasgo a la vez."
    [:div "Las respuestas pueden variar; solo hay que asegurarse de que se cambie un solo rasgo a la vez."]]])

(defn ra4
  "Actividades 4"
  []
  [:section.ra4
   [:li "El archifonema /N/ tiene siete alófonos: " [ipa "[m M © ª n ø N]"] ". Llene el espacio en blanco de las
siguientes transcripciones con el símbolo correcto según la regla."

    [:ol.list.is-lower-alpha.is-size-5
     [:div.grid2
      [:div.l
       [:div.line-item [:li.ipa "[å" [red "M"] "fÒtico]"]]
       [:div.line-item [:li.ipa "[xì" [red "ª"] "te]"]]
       [:div.line-item [:li.ipa "[å" [red "n"] "lÖto]"]]
       [:div.line-item [:li.ipa "[å" [red "ø"] "ÊÔle]"]]
       [:div.line-item [:li.ipa "[a" [red "N"] "gulÒr]"]]
       [:div.line-item [:li.ipa "[å" [red "m"] "beneswÓla]"]]]
      [:div.r
       [:div.line-item [:li.ipa "[pakistÒ" [red "n"] "]"]]
       [:div.line-item [:li.ipa "[bjÓnE" [red "n"] "iseBÒ" [red "n"]"]"]]
       [:div.line-item [:li.ipa "[ké" [red "ø"] "Ìuxe]"]]
       [:div.line-item [:li.ipa "[å" [red "m"] "bexesÓr]"]]
       [:div.line-item [:li.ipa "[å" [red "N"] "xaulÒr]"]]
       [:div.line-item [:li.ipa "[å" [red "n"] "sartÒr]"]]]]]]])
    
(defn ra5
  "Actividades 5"
  []
  [:section.ra5
   [:li "Transcriba las siguientes palabras/frases fonéticamente:"

    [:ol.list.is-lower-alpha
     [:div.line-item.grid2
      [:li "mate"]
      [:div.ipa.red "[mÒte]"]]
     [:div.line-item.grid2
      [:li "ananás"]
      [:div.ipa.red "[ananÒs]"]]
     [:div.line-item.grid2
      [:li "amante"]
      [:div.ipa.red "[amæªte]"]]
     [:div.line-item.grid2
      [:li "ñandú"]
      [:div.ipa.red "[¦ÜªdÖ]"]]
     [:div.line-item.grid2
      [:li "mañana"]
      [:div.ipa.red "[ma¦Òna]"]]
     [:div.line-item.grid2
      [:li "álbum"]
      [:div.ipa.red "[ÒlBun]"]]
     [:div.line-item.grid2
      [:li "neblina"]
      [:div.ipa.red "[neBlÔna]"]]
     [:div.line-item.grid2
      [:li "amparo"]
      [:div.ipa.red "[ÜmpÒRo]"]]
     [:div.line-item.grid2
      [:li "ancho"]
      [:div.ipa.red "[æøÊo]"]]
     [:div.line-item.grid2
      [:li "infante"]
      [:div.ipa.red "[ÞMfæªte]"]]
     [:div.line-item.grid2
      [:li "anzuelo"]
      [:div.ipa.red "[ÜnswÓlo] or [Ü©TwÓlo]"]]
     [:div.line-item.grid2
      [:li "mamón"]
      [:div.ipa.red "[mamén]"]]
     [:div.line-item.grid2
      [:li "ninfa"]
      [:div.ipa.red "[nèMfa]"]]
     [:div.line-item.grid2
      [:li "número"]
      [:div.ipa.red "[nÖmERo]"]]
     [:div.line-item.grid2
      [:li "emblema"]
      [:div.ipa.red "[ÝmblÓma]"]]
     [:div.line-item.grid2
      [:li "columna"]
      [:div.ipa.red "[kolêmna]"]]
     [:div.line-item.grid2
      [:li "riñón"]
      [:div.ipa.red "[ri¦én]"]]
     [:div.line-item.grid2
      [:li "infección"]
      [:div.ipa.red "[ÞMfEksjén]"]]
     [:div.line-item.grid2
      [:li "narigón"]
      [:div.ipa.red "[naRiGén]"]]
     [:div.line-item.grid2
      [:li "menso"]
      [:div.ipa.red "[mçnso]"]]]]])

(defn render-respuestas
  "Chapter 15 respuestas"
  []
  [:main.c15.respuestas.content
   [shared/fonetica-title "Capítulo 15 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]
     [ra5]]]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:li "Escriba el símbolo del alófono descrito por los siguientes rasgos."
    [shared/media-icon :r (rfe/href :fonetica.routes/c15-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:li [:div [:div "nasal"]
             [:div "velar"]
             [:div "sonora"]]]
       [:li [:div 
             [:div "nasal"]
             [:div "dental"]
             [:div "sonora"]]]
       [:li [:div 
             [:div "nasal"]
             [:div "alveolar"]
             [:div "sonora"]]]
       [:li [:div 
             [:div "nasal"]
             [:div "bilabial"]
             [:div "sonora"]]]]
      
      [:div.r
       [:li [:div 
             [:div "nasal"]
             [:div "labiodental"]
             [:div "sonora"]]]
       [:li [:div 
             [:div "nasal"]
             [:div "palatal"]
             [:div "sonora"]]]
       [:li [:div 
             [:div "nasal"]
             [:div "interdental"]
             [:div "sonora"]]]
       [:li [:div 
             [:div "nasal"]
             [:div "palatalizada"]
             [:div "sonora"]]]
       
       ]
      ]]]])

(defn a2
  "Actividades 2"
  []
  [:section.a2
   [:li "Indique los rasgos fonéticos para los siguientes alófonos."
    [shared/media-icon :r (rfe/href :fonetica.routes/c15-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2 {:style {:gap 0}}
      [:div.l
       [:li [ipa "[ø]"]]
       [:li [ipa "[M]"]]]
      [:div.r
       [:li [ipa "[m]"]]
       [:li [ipa "[n]"]]]
      [:div.l
       [:li [ipa "[ª]"]]
       [:li [ipa "[¦]"]]]
      [:div.r
       [:li [ipa "[©]"]]
       [:li [ipa "[N]"]]]]]]])

(defn a3
  "Actividades 3"
  []
  [:section.a3
   [:li "Haga una cadena fonética, cambiando un solo rasgo a la vez."
    [shared/media-icon :r (rfe/href :fonetica.routes/c15-r) :same-session]
    [:ol.list.is-lower-alpha
     [:li.ipa "[¦] > [ ] > [ ] > [ ]"]
     [:li.ipa "[N] > [ ] > [ ] > [ ]"]
     [:li.ipa "[©] > [ ] > [ ] > [ ]"]
     [:li.ipa "[m] > [ ] > [ ] > [ ]"]]]])

(defn a4
  "Actividades 4"
  []
  [:section.a4
   [:li "El archifonema /N/ tiene siete alófonos: " [ipa "[m M © ª n ø N]"] ". Llene el espacio en blanco de las
siguientes transcripciones con el símbolo correcto según la regla."
    [shared/media-icon :r (rfe/href :fonetica.routes/c15-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.grid2
      [:div.l
       [:div.line-item [:li.ipa "[å fÒtico]"]]
       [:div.line-item [:li.ipa "[xì te]"]]
       [:div.line-item [:li.ipa "[å lÖto]"]]
       [:div.line-item [:li.ipa "[å ÊÔle]"]]
       [:div.line-item [:li.ipa "[a gulÒr]"]]
       [:div.line-item [:li.ipa "[å beneswÓla]"]]]
      [:div.r
       [:div.line-item [:li.ipa "[pakistÒ ]"]]
       [:div.line-item [:li.ipa "[bjÓnE iseBÒ ]"]]
       [:div.line-item [:li.ipa "[ké Ìuxe]"]]
       [:div.line-item [:li.ipa "[å bexesÓr]"]]
       [:div.line-item [:li.ipa "[å xaulÒr]"]]
       [:div.line-item [:li.ipa "[å sartÒr]"]]]]]]])
    
(defn a5
  "Actividades 5"
  []
  [:section.a5
   [:li "Transcriba las siguientes palabras/frases fonéticamente:"
    [shared/media-icon :r (rfe/href :fonetica.routes/c15-r) :same-session]
    [:ol.list.is-lower-alpha
     [:div.line-item
      [:li "mate"]]
     [:div.line-item
      [:li "ananás"]]
     [:div.line-item
      [:li "amante"]]
     [:div.line-item
      [:li "ñandú"]]
     [:div.line-item
      [:li "mañana"]]
     [:div.line-item
      [:li "álbum"]]
     [:div.line-item
      [:li "neblina"]]
     [:div.line-item
      [:li "amparo"]]
     [:div.line-item
      [:li "ancho"]]
     [:div.line-item
      [:li "infante"]]
     [:div.line-item
      [:li "anzuelo"]]
     [:div.line-item
      [:li "mamón"]]
     [:div.line-item
      [:li "ninfa"]]
     [:div.line-item
      [:li "número"]]
     [:div.line-item
      [:li "emblema"]]
     [:div.line-item
      [:li "columna"]]
     [:div.line-item
      [:li "riñón"]]
     [:div.line-item
      [:li "infección"]]
     [:div.line-item
      [:li "narigón"]]
     [:div.line-item
      [:li "menso"]]]]])

(defn actividades
  "Chapter 15 actividades"
  []
  [:section.actividades.content
   [:h2 "ACTIVIDADES"]
   [:ol.actividades
    [a1]
    [a2]
    [a3]
    [a4]
    [a5]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C15-M1.mp3")] "Los tres fonemas nasales " [ipa "/m n ¦/"] "."]
    [:li [shared/media-icon :audio (shared/file "C15-M2.mp3")] "La realización del fonema " [ipa "/m/"] "."]
    [:li [shared/media-icon :audio (shared/file "C15-M3.mp3")] "La realización del fonema " [ipa "/n/"] "."]
    [:li [shared/media-icon :audio (shared/file "C15-M4.mp3")] "Contraste entre {canyon} en inglés y {cañón} en español."]
    [:li [shared/media-icon :audio (shared/file "C15-M5.mp3")] "Los alófonos del archifonema " "/N/" " (el Cuadro 15.12)."]
    [:li [shared/media-icon :ej (shared/file "C15-M6.mp3")] "Ejercicios de pronunciación: el fonema " [ipa "/¦/"] "."]
    [:li [shared/media-icon :ej (shared/file "C15-M7.mp3")] "Ejercicios de pronunciación: el archifonema " "/N/" "."]]])

(defn render
  "Chapter fifteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 15 — Los fonemas nasales"
    "E-Resources"]
   [:div.content.chapter-15
    [materiales]
    [actividades]]
   [shared/footer]])
