(ns fonetica.views.c2.respuestas
  (:require [fonetica.views.components.shared :as shared :refer [ipa femp]]))

(defn a1a1
  "The first activity of the respuestas"
  []
  (let [rows [["s $  $$" "los, $in, pien$$an " "s"]
              ["z" "vazias" "z"]
              ["nn" "sennor" "ñ"]
              ["ཅ" "eཅaua" "st"]
              ["&" "&  (conjunción)" "e, i, y"]
              ["D" "Delos" "D"]
              ["E" "Exien" "E"]
              ["M" "Meçio" "M"]
              ["R" "Ruy" "R"]
              ["Lx" "Lx" "LX (60)"]]]
    [:section.a1-1 {:style {:margin-bottom "2em"}}
     [:p "1. Grafemas/letras del texto paleográfico diferentes de los de hoy en día."]
     [:table.is-bordered.table.is-hoverable.ftable.comp-table
      [:thead
       [:tr 
        [:th  "Grafema arcaico"]
        [:th "Ejemplo"]
        [:th "Grafema moderno"]]]
      (into [:tbody]
            (for [[gr ex mo] rows]
              [:tr 
               [:td.cid gr]
               [:td.cid ex]
               [:td.spanish mo]]))]])
  )

(defn a1a2
  "Activity 2 of the respuestas"
  []
  (let [rows [[[:span "l y ll "]
               [:span "tanto" (ipa "[l]") " cuanto" (ipa "[Y]") ""]
               [:span "dolor" (ipa "[l]") "; pielles" (ipa "[l]") "; lorando" (ipa "[Y]") "; allí [Y] "]]
              [[:span "V y u "]
               [:span "tanto" (ipa "[v]") " cuanto" (ipa "[u]") ""]
               [:span "vasallo" (ipa "[v]") "; vços" (ipa "[u]") "; uazias" (ipa "[v]") "; mucho" (ipa "[u]") ""]]
              [[:span "c y ç  "]
               [:span "tanto" (ipa "[t`s]") " cuanto" (ipa "[k]") ""]
               [:span "bocas" (ipa "[k]") "; albricia" (ipa "[t`s]") "; meçio" (ipa "[t`s]") ""]]
              [[:span "y e i "]
               [:span "tanto" (ipa "[i]") " cuanto" (ipa "[Ô]") ""]
               [:span "cuydados" (ipa "[i]") "; myo" (ipa "[Ô]") "; sin" (ipa "[i]") "; uazias" (ipa "[Ô]") ""]]
              [[:span "i"]
               [:span "tanto" (ipa "[i]") " cuanto" (ipa "[Z]") ""]
               [:span "sin" (ipa "[i]") "; oios" (ipa "[Z]") ""]]]]
    [:section.a1-2  {:style {:margin-bottom "2em"}}
     [:p "2. Vacilaciones grafémicas y fonéticas en los textos."]
     [:table.table.is-bordered.ftable.is-hoverable
      [:thead
       [:tr 
        [:th "Grafemas"]
        [:th "Sonidos"]
        [:th "Ejemplo"]
        ]]
      (into [:tbody]
            (for [[gr ex mo] rows]
              [:tr 
               [:td.cid gr]
               [:td.spanish ex]
               [:td.cid mo]]))]]))

(defn a1a3
  "Respuestas activity 3"
  []
  (let [rows [
              [[:span "[s8]" [shared/media-icon :audio (shared/file "C2-Respuestas-Sound-1.mp3")]]
               [:span "lo" (femp "s") ", " (femp "s") "o" (femp "s") ", ojo" (femp "s") ", pien" (femp "ss") "an, va"
                (femp "ss")
                "allo, ovie"
                (femp "ss")
                "e*"]]
              [[:span "[Z]" [shared/media-icon :audio (shared/file "C2-Respuestas-Sound-2.mp3")]]
               [:span "o" (femp "j") "os, mu" (femp "gi") "eres, agui" (femp "j") "ar, corne" (femp "j") "a"]]
              [[:span "[Y]" [shared/media-icon :audio (shared/file "C2-Respuestas-Sound-3.mp3")]]
               [:span (femp "ll") "orando, a" (femp "ll") "í, vassa" (femp "ll") "o"]]
              [[:span "[v]" [shared/media-icon :audio (shared/file "C2-Respuestas-Sound-4.mp3")]]
               [:span "torna" (femp "v") "a, esta" (femp "v") "alos, a" (femp "v") "ie, o" (femp "v") "ieron, " (femp "v") "eer, " (femp "v") "arones"]]
              [[:span "[t`s]" [shared/media-icon :audio (shared/file "C2-Respuestas-Sound-5.mp3")]]
               [:span "cabe" (femp "ç") "a, " (femp "Ç") "id, u" (femp "ç") "os, me" (femp "ç") "io, albri" (femp "c") "ia"]]
              [[:span "[d`z]" [shared/media-icon :audio (shared/file "C2-Respuestas-Sound-6.mp3")]]
               [:span "" "va" (femp "z") "ias, di" (femp "z") "ian, ra" (femp "z") "one"]]
              [[:span "[z8]" [shared/media-icon :audio (shared/file "C2-Respuestas-Sound-7.mp3")]]
               [:span "me" (femp "s") "urado, buge" (femp "s") "es, burge" (femp "s") "as* "]]
              [[:span "[S]" [shared/media-icon :audio (shared/file "C2-Respuestas-Sound-8.mp3")]]
               [:span "e" (femp "x") "ida, e" (femp "x") "ien"]]]]
    [:section.a1-3  {:style {:margin-bottom "2em"}}
     [:p "3. Sonidos arcaicos que no aparecen en la mayoría de los dialectos modernos del español."]
     [:table.table.is-bordered.ftable.is-hoverable
      [:thead
       [:tr 
        [:th "Sonido"]
        [:th "Ejemplo"]]]
      (into [:tbody]
            (for [[gr ex] rows]
              [:tr 
               [:td.ipa gr]
               [:td.spanish ex]]))]
     [:p "*Estos sonidos se incluyen aquí porque su pronunciación era diferente de la mayoría de los dialectos de hoy. Los detalles se explican en el Capítulo 14."]]))

(defn a1a4
  "Respuestas Activity 4"
  []
  [:section.a1-4
   [:p "4. Grafemas (letras) en la edición crítica de Menéndez Pidal que representan a sonidos diferentes de los de hoy."]
   [:div.grid-center
    [:div.grid2
     [:div.l
      [:div "fabló (" [:em "hoy:"] " habló)"]
      [:div "pielles (" [:em "hoy:"] " pieles)"]
      [:div "tornava (" [:em "hoy:"] " tornaba) [etc.]"]
      [:div "burgeses (" [:em "hoy:"] " burgueses)"]
      [:div "sessaenta (" [:em "hoy:"] " sesenta)"]
      [:div "sospiró (" [:em "hoy:"] " suspiró)"]]
     [:div.r
      [:div "buelto (" [:em "hoy:"] " vuelto)"]
      [:div "Bivar (" [:em "hoy:"] " Vivar)"]
      [:div "grand (" [:em "hoy:"] " gran)"]
      [:div "Castiella (" [:em "hoy:"] " Castilla)"]
      [:div "veer (" [:em "hoy:"] " ver)"]]]]])

(defn a1a5
  "Respuestas Activity 5"
  []
  [:section.a1-5
    [:p "5. Palabras del poema con formas morfológicas arcaicas y sus formas modernas equivalentes."]
    [:div.grid-center 
     [:div.grid2
      [:div.l
       [:div  "auie (" [:em "hoy:"] " había)"]
       [:div "ovieron (" [:em "hoy:"] " oyeron)"]
       [:div "entrove (" [:em "hoy:"] " entró)"]]
      [:div.r
       [:div "sue (" [:em "hoy:"] " su)"]
       [:div "oviesse (" [:em "hoy:"] " hubiese)]"]]]]])

(defn a1a6
  "Respuestas Activity 6"
  []
   [:section.a1-6
    [:p "6. Estructuras sintácticas arcaicas del poema y sus estructuras modernas equivalentes."]
    [:div.grid-center {:style {:line-height "0.2em"}}
     [:p "los sos oios (" [:em "hoy:"] " sus ojos)a "]
     [:p "estavalos catando (" [:em "hoy:"] " los estaba catando o estaba catándolos)"]
     [:p "echados somos (" [:em "hoy:"] " estamos echados)"]
     [:p "exien lo veer (" [:em "hoy:"] " salieron a verlo)"]]])

(defn a1a7
  "Respuestas Activity 7"
  []
  [:section.a1-7
   [:p "7. Palabras del poema que ya no se usan o que han sufrido un cambio semántico."]
   [:div.grid-center
    [:div.grid2
     [:div.l
      [:div "tornar (" [:em "hoy:"] " voltear)"]
      [:div "catar (" [:em "hoy:"] " mirar)"]
      [:div "uço (" [:em "hoy:"] " puerta, postigo)"]
      [:div "ca (" [:em "hoy:"] " porque)"]
      [:div "alcándara (" [:em "hoy:"] " percha, varal)"]
      [:div "adtor (" [:em "hoy:"] " gavilán)"]
      [:div "exida (" [:em "hoy:"] " salida)"]]
     [:div.r
      [:div "diestra (" [:em "hoy:"] " derecha)"]
      [:div "siniestra (" [:em "hoy:"] " izquierda)"]
      [:div "albricias (" [:em "hoy:"] " saludos)"]
      [:div "finiestra (" [:em "hoy:"] " ventana)"]
      [:div "tiesta (" [:em "hoy:"] " cabeza)"]
      [:div "auer (" [:em "hoy:"] " tener)"]
      [:div "ser (" [:em "hoy:"] " estar)"]]]]])

(defn a1
  "The first Respuestas activity"
  []
  [:section.a1
   [:h4 "Actividad 1"] ;; TODO should be styled underlined, italic
   [a1a1]
   [a1a2]
   [a1a3]
   [a1a4]
   [a1a5]
   [a1a6]
   [a1a7]])

(defn a2
  "The second activity, which is only a table"
  []
  [:section.a2
   [:h4 "Actividad 2"]
   (let [rows
         [[[:span "En Costa Rica el nombre de la república se pronuncia comúnmente con un sonido " (ipa "[P]") " semejante al del inglés en {rich}"]
           [:span "fonética"]
           [:span "dialectología"]]
          [[:span "Los niños pequeños muchas veces dicen {yo sabo} en vez de {yo sé}"]
           [:span "morfología"]
           [:span "adquisición de lenguaje"]]
          [[:span "En la región argentina de La Plata la pronunciación de {calle}, que era con " (ipa "[Z]") " (como en " [:em "azure"] " del inglés), pasó a ser con " (ipa "[S]") " (como en " [:em "she"] " del inglés). El cambió comenzó entre las mujeres."]
           [:span "fonética"]
           [:span "sociolingüïstica"]]
          [[:span "En Puerto Rico la pregunta “¿Qué quieres tú?” se expresa comúnmente como “¿Qué tú quieres?”."]
           [:span "sintaxis"]
           [:span "dialectología"]]
          [[:span "En el texto del Cid se usa la palabra “auer” (haber) para expresar posesión. Hoy en día se usa “tener”."]
           [:span "semántica"]
           [:span "historia de la lengua"]]
          [[:span "El inglés tiene un verbo copulativo principal: " [:em "to be"] ". El español tiene dos: " [:em "ser y estar"] ". ¿Cómo se correlacionan?"]
           [:span "semántica"]
           [:span "lingüística aplicada"]]]]
     [:table.table.is-bordered ;; needs black lines
      [:thead [:tr
               [:td "Fenómeno"]
               [:td "Campo básico"]
               [:td "Campo de otra perspectiva"]]]
      (into [:tbody]
            (for [[f b p] rows]
              [:tr
               [:td f]
               [:td.red b]
               [:td.red p]]))])])

(defn render
  "Page of all the respuestas things"
  []
  [:main.c2.respuestas.container.content
   [shared/fonetica-title "Capítulo 2 — Respuestas a las actividades"]
   [a1]
   [a2]])
