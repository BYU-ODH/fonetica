(ns fonetica.views.c1
  "Chapter 1"
  (:require [reitit.frontend.easy :as rfe]
            [fonetica.views.components.shared :as shared]))

(defn c1-m1
  "Renderer for the Material 1 view for chapter 1"
  []
  [:main.container.content
   [shared/fonetica-title "Capítulo 1: Nota 1"]
   [:div.is-size-4 "Fotos de diferentes comunicaciones no verbales. ¿Qué se comunica en cada uno de ellas?"]
   (let [files  ["C1-M1-Smile.jpg"
                 "C1-M1-Frowning.jpg"
                 "C1-M1-Winking_v1.jpg"
                 "C1-M1-Suit.jpg"
                 "C1-M1-Wedding-Band.jpg"
                 "C1-M1-Thumbs-Up.jpg"
                 "C1-M1-pointing.jpg"
                 "C1-M1-Hug_v2.jpg"
                 "C1-M1-Eyeroll_v2.jpg"]]
     (into [:div.photogrid]
           (for [file files]
             [:div.image-container {"border-style" :solid
                                    "border-width" "2px"
                                    "border-color" "black"}
              [:img {:width "200px"
                     :alt (shared/alt-from-filename file)
                     :src (shared/file file)}]]
             ))
        )])

(defn render
  "Chapter one view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 1 — La comunicación humana"
    "E-Resources"]
   [:div.content.chapter-1
    [:section
     [:p {:class "materiales"} "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
     [:ol  
      [:li
       [shared/media-icon :image (rfe/href :fonetica.routes/c1-m1) :in-session]
       "Fotos de diferentes comunicaciones no verbales."]
      [:li
       [shared/media-icon :audio (shared/file "C1-M2-whistle.mp3")]
       "El silbato de árbitro en un partido atlético."]
      [:li 
       [shared/media-icon :audio (shared/file "C1-M3-clicks.mp3")]
       "El chasquido vaquero."]
      [:li 
       [shared/media-icon :audio (shared/file "C1-M4-que-fantastico.mp3")]
       "Exclamaciones que demuestran diferencias paralingüísticas."]
      [:li
       [shared/media-icon :video (shared/file "C1-M5-¡Ven-acá!.mp4")]
       "Video de un gesto que invita a un niño que venga."]
      [:li
       [shared/media-icon :image (shared/file "C1-M6-Two-Count.jpg")]
       "Foto de un cliente que quiere dos boletos."]
      [:li
       [shared/media-icon :video (shared/file "C1-M7-Conversation-Cues.mp4")]
       "Video de reguladores en una conversación."]
      [:li 
       [shared/media-icon :image (shared/file "C1-M8-Furious_v1.jpg")]
       "Foto de una exhibición de sentimiento de una persona furiosa."]
      [:li 
       [shared/media-icon :audio (shared/file "C1-M9-libro.mp3")]
       "Emisión acústica de la palabra " 
       [:i "libro"]"."]
      [:li [shared/media-icon :video (shared/file "C1-M10-Signing-Book.mp4")]
       "Movimiento físico en LSM de la palabra " 
       [:i "libro"]"."]
      ]]
    [:section
     [:h2 {:class "actividades"} "ACTIVIDADES"]
     [:section.a1
      [:p.left "1.¿Los siguientes ejemplos de comunicación son verbales o no verbales? Justifique su clasificación e identifique la vía de transmisión. (Haga clic en el icono para ver/escuchar el ejemplo.)"]
      [shared/item-grid [[shared/media-icon :video (shared/file "C1-A1-Lecture.mp4")]
                         [shared/media-icon :audio (shared/file "C1-A1-2-bronx-cheer.mp3")]
                         [:img {:src (shared/file "C1-quijote.jpg")
                                :alt "quijote.jpg" :class "WPImageStyle" :width "156" :height "92"}]
                         [:img {:src (shared/file "C1-sleep_v1.jpg")
                                :alt "sleep_v1.jpg" :class "WPImageStyle" :width "123" :height "92"}]
                         [:img {:src (shared/file "C1-plane.jpg")
                                :alt "plane.jpg" :class "WPImageStyle" :width "95" :height "92"}]
                         [:img {:src (shared/file "C1-heartpuertorico.jpg")
                                :alt "heartpuertorico.jpg" :class "WPImageStyle" :width "175" :height "70"}]
                         [:img {:src (shared/file "C1-handshake.jpg")
                                :alt "handshake.jpg" :class "WPImageStyle" :width "139" :height "92"}]
                         [shared/media-icon :video (shared/file "C1-A1-Signing.mp4")]
                         [:img {:src (shared/file "C1-A1-9chart.gif")
                                :alt "c1a1-9chart.gif" :class "WPImageStyle" :width "123" :height "92"}]]]]
     [:section.a2
      [:p.left "2. ¿Cuáles de los siguientes ejemplos son palabras del español? Justifique su identificación.
(Haga clic en el icono para escuchar el ejemplo.)"]
      [shared/item-grid [[:span "arena"]
                         [shared/media-icon :audio (shared/file "C1-A2-2-arriba.mp3")]
                         [shared/media-icon :audio (shared/file "C1-A2-3-psst.mp3")]
                         [shared/media-icon :audio (shared/file "C1-A2-4-quiquiriqui.mp3")]
                         [:span "splanot"]
                         [:span "rating"]
                         [:span "modela"]
                         [shared/media-icon :audio (shared/file "C1-A2-8-rooster-crowing.mp3")]
                         [:span "aquena"]]]]
     [:section.a3
      [:p.left "3. ¿Cuál es el chiste de la siguiente tira cómica?"]
      [:img {:src (shared/file "C1-A3.jpg")
             :alt "c1a3.jpg" :class "WPImageStyle" :width "600" :height "160"}]]
     
     [:section.a4
      [:p.left "4. ¿Cuáles son situaciones que favorecen la comunicación verbal por vía oral? ¿Cuáles son\nsituaciones que favorecen la comunicación verbal por vía escrita? Justifique sus\nevaluaciones."]]

     [:section.a5
      [:p.left "5. Identifique el aspecto del modelo de comunicación y la vía de transmisión de los siguientes aspectos de un evento comunicativo."]
      [shared/item-grid [[:img {:src (shared/file "C1-writing_v2.jpg")
                                :alt "writing_v2.jpg" :class "WPImageStyle" :width "173" :height "115"}]
                         [:img {:src (shared/file "C1-talking_v2.jpg")
                                :alt "talking_v2.jpg" :class "WPImageStyle" :width "139" :height "115"}]
                         [:img {:src (shared/file "C1-listening_v1.jpg")
                                :alt "listening_v1.jpg" :class "WPImageStyle" :width "115" :height "138"}]
                         [:img {:src (shared/file "C1-signing_v3.jpg")
                                :alt "signing_v3.jpg" :class "WPImageStyle" :width "115" :height "138"}]
                         [:img {:src (shared/file "C1-watchingsigning_v2.jpg")
                                :alt "watchingsigning_v2.jpg" :class "WPImageStyle" :width "175" :height "115"}]
                         [:img {:src (shared/file "C1-reading.jpg")
                                :alt "reading.jpg" :class "WPImageStyle" :width "116" :height "138"}]]]]
     [:section.a6
      [:p.left "6.¿Qué quieren decir los siguientes iconos? ¿Para qué se pueden usar?"]
      [shared/item-grid [ [:img {:src (shared/file "C1-iconlightbulb.png")
                                :alt "iconlightbulb.png" :class "WPImageStyle" :width "49" :height "55"}]
                         [:img {:src (shared/file "C1-iconenvelope.png")
                                :alt "iconenvelope.png" :class "WPImageStyle" :width "49" :height "36"}]
                         [:img {:src (shared/file "C1-iconwrench.png")
                                :alt "iconwrench.png" :class "WPImageStyle" :width "56" :height "55"}]
                         [:img {:src (shared/file "C1-iconmouse.png")
                                :alt "iconmouse.png" :class "WPImageStyle" :width "37" :height "55"}]
                         [:img {:src (shared/file "C1-iconcellphone.png")
                                :alt "iconcellphone.png" :class "WPImageStyle" :width "76" :height "55"}]
                         [:img {:src (shared/file "C1-iconm-w.png")
                                :alt "iconm-w.png" :class "WPImageStyle" :width "55" :height "55"}]]]
      [:p "¿Cuáles son otros ejemplos de iconos y qué representan?"]]
     
     [:section.a7
      [:p.left "7.¿Qué quieren decir los siguientes gestos?"]
      [shared/item-grid [[:img {:src (shared/file "C1-ojo.jpg")
                                :alt "ojo.jpg" :class "WPImageStyle" :width "112" :height "138"}]
                         [:img {:src (shared/file "C1-callgesture.jpg")
                                :alt "callgesture.jpg" :class "WPImageStyle" :width "139" :height "138"}]
                         [:img {:src (shared/file "C1-tres.jpg")
                                :alt "tres.jpg" :class "WPImageStyle" :width "106" :height "138"}]
                         [:img {:src (shared/file "C1-M1-Thumbs-Up.jpg")
                                :alt "C1-M1-Thumbs-Up.jpg" :class "WPImageStyle" :width "166" :height "103"}]
                         [:img {:src (shared/file "C1-M1-pointing.jpg")
                                :alt "C1-M1-pointing.jpg" :class "WPImageStyle" :width "166" :height "110"}]
                         [:img {:src (shared/file "C1-bowing.jpg")
                                :alt "bowing.jpg" :class "WPImageStyle" :width "92" :height "138"}]]]
      [:p "¿Cuáles son otros ejemplos de gestos y qué representan? ¿Cuáles cambian de acuerdo con la sociedad en que se producen?"]]
     [:section.a8
      [:p.left "8.¿Qué quieren decir los siguientes signos acústicos?"]
      [shared/item-grid [[shared/media-icon :audio (shared/file "C1-A8-1-wolf-whistle.mp3")]
                         [shared/media-icon :audio (shared/file "C1-A8-2-whew!.mp3")]
                         [shared/media-icon :audio (shared/file "C1-A8-3-tsk!tsk!.mp3")]
                         [shared/media-icon :audio (shared/file "C1-A8-4-psst.mp3")]
                         [shared/media-icon :audio (shared/file "C1-A8-5-ahem!.mp3")]
                         [shared/media-icon :audio (shared/file "C1-A8-6-kiss-kiss.mp3")]]]
      [:p "¿Cuáles son otros ejemplos de signos acústicos y qué representan?"]]
     ]
    ]
   [shared/footer]])
