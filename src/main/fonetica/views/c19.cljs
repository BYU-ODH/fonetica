(ns fonetica.views.c19
  "Chapter 19"
  (:require [fonetica.views.components.shared :as shared :refer [ipa fempr red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas activity 1"
  []
  [:section.ra1
   [:li
    [:div "Escucha las siguientes palabras en la grabación. Para cada palabra, subraye cada vocal tónica y escriba el acento ortográfico caso sea necesario."]
    (let [lrows [[:div "acent" [fempr "ú"] "o"]
                 [:div "ansi" [fempr "o"] "sam" [fempr "e"] "nte"]
                 [:div "arru" [fempr "i"] "na"]
                 [:div "" [fempr "á"] "spid"]
                 [:div "b" [fempr "ú"] "ho"]
                 [:div "c" [fempr "á"] "ntara"]
                 [:div "car" [fempr "á"] "cter"]
                 [:div "decimos" [fempr "é"] "ptimo"]
                 [:div "d" [fempr "é"] "nnoslo"]
                 [:div "di" [fempr "o"] ""]
                 [:div "esdr" [fempr "ú"] "jula"]
                 [:div "estudi" [fempr "á"] "is"]]
          rrows [[:div "fel" [fempr "i"] "z"]
                 [:div "fr" [fempr "í"] "am" [fempr "e"] "nte"]
                 [:div "fu" [fempr "e"] ""]
                 [:div "her" [fempr "o"] "ico"]
                 [:div "hu" [fempr "é"] "sped"]
                 [:div "jesu" [fempr "i"] "tico"]
                 [:div "l" [fempr "á"] "grima"]
                 [:div "l" [fempr "a"] "piz"]
                 [:div "per" [fempr "í"] "odo"]
                 [:div "proh" [fempr "í"] "bo"]
                 [:div "puntapi" [fempr "é"] ""]
                 [:div "rev" [fempr "é"] "s"]]]
      [:ol.list.is-lower-alpha
       (conj [:div.grid2]
             (into [:div.l]
                   (for [i lrows]
                     [:li i]))
             (into [:div.r]
                   (for [i rrows]
                     [:li i])))])]])

(defn ra2
  "Respuestas activity 2"
  []
  [:section.ra2
   [:li
    [:div "Escucha las siguientes oraciones en la grabación. Para cada oración, subraye cada vocal tónica y escriba el acento ortográfico caso sea necesario."]
    (let [rows [[:div "Ju" [fempr "a"] "n s" [fempr "í"] " lo h" [fempr "i"] "zo."]
                [:div "N" [fempr "o"] " h" [fempr "a"] " termin" [fempr "a"] "do a" [fempr "ú"] "n."]
                [:div "¡S" [fempr "é"] " bu" [fempr "e"] "no!"]
                [:div "Tom" [fempr "a"] "mos t" [fempr "é"] " a las tr" [fempr "e"] "s."]
                [:div "N" [fempr "o"] " imp" [fempr "o"] "rta cu" [fempr "á"] "nto d" [fempr "é"] "."]
                [:div "Es" [fempr "o"] " n" [fempr "o"] " se h" [fempr "a"] "ce aqu" [fempr "í"] "."]
                [:div "N" [fempr "o"] " h" [fempr "a"] "bles con aqu" [fempr "e"] "l."]
                [:div "Le pagu" [fempr "é"] " cuanto le deb" [fempr "í"] "a."]
                [:div "¿Ad" [fempr "ó"] "nde v" [fempr "a"] "s?"]
                [:div "Se d" [fempr "e"] "be tra" [fempr "e"] "r 10 ó m" [fempr "á"] "s."]]]
      [:ol.list.is-lower-alpha
       (into [:div.grid2]
             (for [i rows]
               [:li i])
             )])]])


(defn render-respuestas
  "Chapter 19 respuestas"
  []
  [:main.c19.respuestas.content
   [shared/fonetica-title "Capítulo 19 — Respuestas a las actividades"]
   [:section.actividades
    [:h2 "ACTIVIDADES"]
    [:ol.actividades.list
     [ra1]
     [ra2]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
      [:li
       [:div {:style {:display "inline"}} "Escucha las siguientes palabras en la grabación. Para cada palabra, subraye cada vocal tónica y escriba el acento ortográfico caso sea necesario."]
       [:span
        [shared/media-icon :audio (shared/file "C19-A1.mp3")]
        [shared/media-icon :r (rfe/href :fonetica.routes/c19-r) :same-session]]
       (let [lrows [[:div "acentúo"]
                 [:div "ansiosamente"]
                 [:div "arruina"]
                 [:div "áspid"]
                 [:div "búho"]
                 [:div "cántara"]
                 [:div "carácter"]
                 [:div "decimoséptimo"]
                 [:div "dénnoslo"]
                 [:div "dio"]
                 [:div "esdrújula"]
                 [:div "estudiáis"]]
          rrows [[:div "feliz"]
                 [:div "fríamente"]
                 [:div "fue"]
                 [:div "heroico"]
                 [:div "huésped"]
                 [:div "jesuitico"]
                 [:div "lágrima"]
                 [:div "lapiz"]
                 [:div "período"]
                 [:div "prohíbo"]
                 [:div "puntapié"]
                 [:div "revés"]]]
      [:ol.list.is-lower-alpha
       (conj [:div.grid2]
             (into [:div.l]
                   (for [i lrows]
                     [:li i]))
             (into [:div.r]
                   (for [i rrows]
                     [:li i])))])]])
(defn a2
  "Activity 2"
  []
  [:section.a2
   [:li
    [:div {:style {:display "inline"}} "Escucha las siguientes palabras en la grabación. Para cada palabra, subraye cada vocal tónica y escriba el acento ortográfico caso sea necesario."]
    [:span
     [shared/media-icon :audio (shared/file "C19-A2.mp3")]
     [shared/media-icon :r (rfe/href :fonetica.routes/c19-r) :same-session]]
    (let [rows [[:div "Juan sí lo hizo."]
                [:div "No ha terminado aún."]
                [:div "¡Sé bueno!"]
                [:div "Tomamos té a las tres."]
                [:div "No importa cuánto dé."]
                [:div "Eso no se hace aquí."]
                [:div "No hables con aquel."]
                [:div "Le pagué cuanto le debía."]
                [:div "¿Adónde vas?"]
                [:div "Se debe traer 10 ó más."]]]
      [:ol.list.is-lower-alpha
       (into [:div.grid2 {:style {:gap 0}}]
             (for [i rows]
               [:li i]))])]])


(defn actividades
  "Chapter 19 actividades"
  []
  [:section.actividades.content
   [:h2 "ACTIVIDADES"]
   [:ol.actividades
    [a1]
    [a2]]])

(defn materiales
  "Materials"
  []
  [:section.materiales
   [:h3 "MATERIALES EN LÍNEA SEGÚN LAS REFERENCIAS DEL LIBRO"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C19-M1.mp3")] "Acento diferencial en inglés y español."]
    [:li [shared/media-icon :audio (shared/file "C19-M2.mp3")] "English multiplication."]
    [:li [shared/media-icon :audio (shared/file "C19-M3.mp3")] "La relativa duración de sílabas átonas y tónicas en inglés (el Cuadro 19.2)."]
    [:li [shared/media-icon :audio (shared/file "C19-M4.mp3")] "La relativa duración de sílabas átonas y tónicas en español (el Cuadro 19.3)."]
    [:li [shared/media-icon :audio (shared/file "C19-M5.mp3")] "La palabra inglesa content en dos oraciones (el Cuadro 19.5)."]
    [:li [shared/media-icon :audio (shared/file "C19-M6.mp3")] "La palabra española camino en dos oraciones (el Cuadro 19.6)."]
    [:li [shared/media-icon :audio (shared/file "C19-M7.mp3")] "Las palabras españolas camino y caminó en dos oraciones (el Cuadro 19.7)."]
    [:li [shared/media-icon :audio (shared/file "C19-M8.mp3")] "Las palabras españolas cántara, cantara y cantará (el Cuadro 19.9)."]
    [:li [shared/media-icon :audio (shared/file "C19-M9.mp3")] "Palabras con acento contrastivo en inglés."]
    [:li [shared/media-icon :audio (shared/file "C19-M10.mp3")] "Palabras con acento contrastivo en español."]
    [:li [shared/media-icon :audio (shared/file "C19-M11.mp3")] "Variabilidad en las sílabas tónicas del inglés."]
    [:li [shared/media-icon :audio (shared/file "C19-M12.mp3")] "La tonicidad de palabras como que, etc."]
    [:li [shared/media-icon :audio (shared/file "C19-M13.mp3")] "La tonicidad en números."]
    [:li [shared/media-icon :audio (shared/file "C19-M14.mp3")] "La tonicidad en los adverbios terminados en -mente."]
    [:li [shared/media-icon :ej (shared/file "C19-M15.mp3")] "Ejercicios de pronunciación: la posición del acento fonético."]]])

(defn render
  "Chapter nineteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 19 — El acento"
    "E-Resources"]
   [:div.content.chapter-19
    [materiales]
    [actividades]]
   [shared/footer]])
