(ns fonetica.views.c2
  "Chapter 2"
  (:require [fonetica.views.components.shared :as shared :refer [ipa femp]]
            [fonetica.views.c2.respuestas :as respuestas ]
            [reitit.frontend.easy :as rfe]))

(def render-respuestas
  "Render function for the routing to avoid one more inclusion"
  respuestas/render)

(defn actividad-1 []
  [:section.actividad-1
   [:h3 "Actividad 1"]
   [:p 
    "La mejor manera de analizar el lenguaje, sea por vía oral o por vía escrita, es a través de los\ncampos básicos de la lingüística. Como ejemplo de cómo esos campos se pueden aplicar a un\ntexto, se presenta aquí el siguiente trozo literario, que viene del " 
    [:em "Cantar de Mío Cid"]", un poema\népico del siglo XII, escrito en el español arcaico."]
   [:p 
    "A la izquierda se presenta el texto paleográfico" 
    [:sup "1"]" en estilo semidiplomático; a la derecha, la\nedición crítica del famoso lingüísta Ramón Menéndez Pidal con letras modernas, pero\nconservando las estructuras del idioma antiguo. Se escogió el texto para demostrar cómo el\nidioma ha cambiado a través de los años y cómo no ha cambiado con respecto a los sistemas\nbásicos del lenguaje: la fonética/ fonología, la morfología, la sintaxis y la semántica. OBS: A\npesar de su vejez, todavía se puede leer este texto del español, lo que sería imposible decir de un\ntexto inglés de la misma época."]
   [:br]
   [:table.table.is-fullwidth.comp
    [:tbody
     [:tr 
      [:td.cid "Delos $os oios tan fuerte mientre lorando,"] 
      [:td.spanish "De los sos ojos tan fuertemientre llorando,"]] 
     [:tr 
      [:td.cid "To®naua la cabeça &amp; e%aua los catando;"] 
      [:td.spanish "tornava la cabeça i estávalos catando."]] 
     [:tr 
      [:td.cid "Vio puertas abiertas &amp; vços $in cannados,"] 
      [:td.spanish "Vío puertas abiertas e uços sin cañados,"]] 
     [:tr 
      [:td.cid "Alcandaras uazias $in pielles &amp; $in mantos,"] 
      [:td.spanish "alcándaras vázias sin pielles e sin mantos"]] 
     [:tr 
      [:td.cid "E $in falcones &amp; $in adto®es mudados."] 
      [:td.spanish "e sin falcones e sin adtores mudados."]] 
     [:tr 
      [:td.cid "Ûo$piro mÿo çid, ca mucho auie grandes cuydados."] 
      [:td.spanish "Sospiró mio Çid, ca mucho avie grandes cuidados."]] 
     [:tr 
      [:td.cid "Fablo mÿo çid bien &amp; tan me$urado:"] 
      [:td.spanish "Fabló mio Çid bien e tan mesurado:"]] 
     [:tr 
      [:td.cid "\"¡Grado ati, $enno® padre, que e%as en alto!"] 
      [:td.spanish "«grado a tí, señor padre, que estás en alto!"]] 
     [:tr 
      [:td.cid "E%o me an buelto mÿos enemigos malos\"."]
      [:td.spanish "Esto me an buelto mios enemigos malos»."]]
     [:tr
      [:td.cid "Alli pien$$an de aguiiar, alli $ueltan las #iendas."]
      [:td.spanish [:p "Allí pienssan de aguijar, allí sueltan las riendas."]]] 
     [:tr 
      [:td.cid "Ala Exida de biuar, ouieron la co®neia die%ra;"]
      [:td.spanish "A la exida de Bivar, ovieron la corneja diestra,"]] 
     [:tr 
      [:td.cid "E entrando a burgos, ouieron la $inie%ra."] 
      [:td.spanish "e entrando a Burgos oviéronla siniestra."]] 
     [:tr 
      [:td.cid "Meçio mÿo çid los ombros &amp; en grameo la tie%a:"]
      [:td.spanish "Meçió mio Çid los ombros y engrameó la tiesta:"]] 
     [:tr 
      [:td.cid "\"¡Albricia, albarffanez, ca echados $omos de tierra!\"."
       [:p "Myo çid #uÿ diaz po® burgos en traua,"]] 
      [:td.spanish "«albricia, Álbar Fáñez, ca echados somos de tierra!»"
       [:br]
       [:em "«Mas a grand ondra tornaremos a Castiella»."]
       [:p "Mio Çid Roy Díaz por Burgos entróve,"]]] 
     [:tr 
      [:td.cid "En $u conpanna Lx pendones (leuaua);"] 
      [:td.spanish "En sue conpaña sessaenta pendones;"]] 
     [:tr 
      [:td.cid "Exien lo uer mugieres &amp; uarones,"]
      [:td.spanish "exien lo veer mugieres e varones,"]] 
     [:tr 
      [:td.cid "Burge$es &amp; burge$as po® las finie%ras $on \n [ pue%as],"]
      [:td.spanish "burgeses e burgesas por las finiestras sone,"]] 
     [:tr 
      [:td.cid "Plo®ando delos oios, ¡ tanto auÿen el dolo®!"]
      [:td.spanish "plorando de los ojos, tanto avien el dolore,"]] 
     [:tr 
      [:td.cid "Delas $us bocas todos dizian una #azon:"]
      [:td.spanish "De las sus bocas todos dizían una razóne:"]] 
     [:tr 
      [:td.cid "\"¡Dios, que buen va$$alo, $i ouie$$e buen Senno®!\"."]
      [:td.spanish "«Dios, qué buen vassallo, si oviesse buen señore!»"]]]]
   [:p 
    [:sup "1"]"La fuente utilizada es “Cid”, producida por Juan-José Marcos (juanjmarcos@gmail.com)."]])

(defn ortografia []
  [:ol.page2
   [:section.ortografia
    [:h4 [:em "Fonética/Fonología/Ortografía"]]
    [:li "En el texto paleográfico, ¿cuáles son algunos de los grafemas o letras con formas diferentes\nde los de hoy? "
     [shared/media-icon :r (rfe/href :fonetica.routes/c2-r) :same-session]]
    [:li
     "Como no había diccionarios en la época medieval que rigieran la forma “correcta” para escribir las palabras, la escritura era variable sin una ortografía fija. Hay ejemplos en que la misma palabra se deletreaba de maneras distintas, y ejemplos en que el mismo grafema se usaba para sonidos distintos. Este fenómeno resultó como consecuencia de dos procesos: primero por motivos fonéticos y segundo por motivos históricos. Fonéticamente, a veces es posible representar los sonidos de determinada palabra mediante letras diferentes. Por ejemplo: " 
     [:em "ondra"]" frente a " 
     [:em "honra"]". Históricamente, a veces se empleaba una ortografía arcaica que representaba sonidos que hasta en la época ya no se pronunciaban o se pronunciaban de una manera ya diferente. Por ejemplo: " 
     [:em "plorando"]" frente a " 
     [:em "llorando"]". En el texto de la edición paleográfica, ¿cuáles son ejemplos de esas vacilaciones ortográficas? " 
     [shared/media-icon :r (rfe/href :fonetica.routes/c2-r) :same-session]]

    [:li
     "Haga clic en el icono para escuchar el trozo en el español arcaico." [shared/media-icon :audio (shared/file "C2-El-Cid.mp3")]
     " ¿Cuáles son algunos de los sonidos diferentes de los del español moderno que Ud. conoce?" [shared/media-icon :r (rfe/href :fonetica.routes/c2-r) :same-session]]

    [:li
     "¿Cuáles son algunos de los grafemas (letras) en la edición crítica de Menéndez Pidal que representan a sonidos diferentes de los de hoy? " 
     [shared/media-icon :r (rfe/href :fonetica.routes/c2-r) :same-session]]]

   [:section.morfologia
    [:h4 [:em "Morfología"]]
    [:li
     "¿Cuáles son palabras en los textos que tienen formas morfológicas diferentes de las del español de hoy? Por ejemplo: " 
     [:em "fuerte mientre"]" vs. " 
     [:em "fuertemente"]". " 
     [shared/media-icon :r (rfe/href :fonetica.routes/c2-r) :same-session]]]
   [:section.sintaxis
    [:h4 [:em "Sintaxis"]]
    [:li
     "¿Cuáles son estructuras sintácticas que son diferentes de las del español de hoy? " 
     [shared/media-icon :r (rfe/href :fonetica.routes/c2-r) :same-session]]]
   [:section.semantica 
    [:h4 [:em "Semántica"]]
    [:li
     "¿Cuáles son palabras usadas en el poema que ya no se usan o que han sufrido un cambio semántico? " 
     [shared/media-icon :r (rfe/href :fonetica.routes/c2-r) :same-session]]]])

(defn actividad-2 []
  [:section.actividad-2
   [:h3 
    "Actividad 2"]
   [:p 
    "Complete el siguiente cuadro indicando el campo básico y el campo lingüístico de las otras\nperspectivas que se emplearía para estudiar los siguientes fenómenos: " 
    [shared/media-icon :r (rfe/href :fonetica.routes/c2-r) :same-session]]
   
   [:table.table.is-bordered
    [:thead
     [:tr 
      [:th {:width "40%"} "Fenómeno"]
      [:th "Campo básico"]
      [:th "Campo de otra perspectiva"]]]
    [:tbody
     [:tr 
      [:td
       [:p 
        [:span "En Costa Rica el nombre de la república se pronuncia comúnmente con un sonido " 
         [:span.ipa "[P]"]
         "semejante al del inglés en {rich}"]]]
      [:td]
      [:td]]
     [:tr 
      [:td [:p "Los niños pequeños muchas veces dicen\n{yo sabo} en vez de {yo sé}"]]
      [:td]
      [:td]]
     [:tr 
      [:td
       [:p 
        "En la región argentina de La Plata la\npronunciación de {calle}, que era con " 
        [:span.ipa "[Z]"]
        "(como en " [:em "azure"] " del inglés), pasó a ser con" 
        [:span.ipa "[S]"] "(como en " [:em "she"]" del inglés). El cambió\ncomenzó entre las mujeres. "]]
      [:td]
      [:td]]
     [:tr 
      [:td
       [:p 
        "En Puerto Rico la pregunta “¿Qué quieres tú?” se expresa comúnmente como “¿Qué tú quieres?”."]]
      [:td]
      [:td]]
     [:tr 
      [:td 
       [:p 
        "En el texto del Cid se usa la palabra “auer” (haber) para expresar posesión. Hoy en día se usa “tener”."]]
      [:td]
      [:td]]
     [:tr 
      [:td
       [:p 
        "El inglés tiene un verbo copulativo principal: " 
        [:em "to be"]". El español tiene dos: " 
        [:em "ser"]" y" 
        [:em "estar"]". ¿Cómo se correlacionan?"]]
      [:td]
      [:td]]]]

   ])

(defn actividades
  "super section for all activities"
  []
  [:section.actividades
   [:h2 "ACTIVIDADES"]
   [actividad-1]
   [ortografia]
   [actividad-2]]
  )

(defn render
  "Chapter two view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 2 — La lingüística"
    "E-Resources"]
   [:div.content.chapter-2
    [actividades]]
   [shared/footer]
   ])
