(ns fonetica.routes
  (:require [reagent.core :as r]
            [reitit.frontend :as rf]
            [reitit.frontend.easy :as rfe]
            [reitit.frontend.history :as rfh]
            [fonetica.views.toc :as toc]
            [fonetica.views.c1 :as c1]
            [fonetica.views.c2 :as c2]
            [fonetica.views.c3 :as c3]
            [fonetica.views.c4 :as c4]
            [fonetica.views.c5 :as c5]
            [fonetica.views.c6 :as c6]
            [fonetica.views.c7 :as c7]
            [fonetica.views.c8 :as c8]
            [fonetica.views.c9 :as c9]
            [fonetica.views.c10 :as c10]
            [fonetica.views.c11 :as c11]
            [fonetica.views.c12 :as c12]
            [fonetica.views.c13 :as c13]
            [fonetica.views.c14 :as c14]
            [fonetica.views.c15 :as c15]
            [fonetica.views.c16 :as c16]
            [fonetica.views.c17 :as c17]
            [fonetica.views.c18 :as c18]
            [fonetica.views.c19 :as c19]
            [fonetica.views.c20 :as c20]
            [fonetica.views.c21 :as c21]
            [clojure.string :as str]
            [goog.uri.utils :as gu]
            [goog.string.path :as goop]))

(defn this-location []
  (str (.-location js/window)))

(def embedded-path-name
  "The subdir path if we are being hosted under another URL, like spanport.byu.edu/fonetica"
  "/fonetica")

(defn hosted?
  "Determine whether our app is hosted as a subdir, or exists as a subdomain."
  []
  (str/starts-with?
   (gu/getPath this-location)
   embedded-path-name))

(defn base-route 
  "The base route, changing by whether we are hosted or not"
  []
  (if (hosted?)
    (str embedded-path-name "/")
    "/"))

(defn get-resource-path
  "Get a given filenames path, which depends upon whether we are hosted"
  [filename]
  (let [file-dir "Files"]
    (str (base-route) file-dir "/" filename)))

(defn default-view
  "To show before routes are loaded"
  []
  [:h1 "Loading..."])

(defonce current-view (r/atom default-view) )

(def routes
  (rf/router
   ["/"
    ["" {:name ::table-of-contents
         :view #'toc/render}]
    ["c/"
     ["1/"
      [""
       {:name ::chapter-1
        :view #'c1/render}]
      ["m1/"
       {:name ::c1-m1
        :view #'c1/c1-m1}]
      ]
     ["2/"
      [""
       {:name ::chapter-2
        :view #'c2/render}]
      ["respuestas/"
       {:name ::c2-r
        :view #'c2/render-respuestas}]]
     ["3/"
      [""
       {:name ::chapter-3
        :view #'c3/render}]
      ["respuestas/"
       {:name ::c3-r
        :view #'c3/render-respuestas}]]
     ["4/"
      [""
       {:name ::chapter-4
        :view #'c4/render}]
      ["m1/"
       {:name ::c4-m1
        :view #'c4/render-m1}]
      ["m2/"
       {:name ::c4-m2
        :view #'c4/render-m2}]
      ["m3/"
       {:name ::c4-m3
        :view #'c4/render-m3}]
      ["m4/"
       {:name ::c4-m4
        :view #'c4/render-m4}]
      ["m5/"
       {:name ::c4-m5
        :view #'c4/render-m5}]
      ["m6/"
       {:name ::c4-m6
        :view #'c4/render-m6}]
      ["m7/"
       {:name ::c4-m7
        :view #'c4/render-m7}]
      ["m8/"
       {:name ::c4-m8
        :view #'c4/render-m8}]
      ["m9/"
       {:name ::c4-m9
        :view #'c4/render-m9}]
      ["m10/"
       {:name ::c4-m10
        :view #'c4/render-m10}]
      ["m11/"
       {:name ::c4-m11
        :view #'c4/render-m11}]]
     ["5/"
      [""
       {:name ::chapter-5
        :view #'c5/render}]
      ["ej15/"
       {:name ::c5-ej15
        :view #'c5/ej-15}]
      ["ej25/"
       {:name ::c5-ej25
        :view #'c5/ej-25}]
      ["ej34/"
       {:name ::c5-ej34
        :view #'c5/ej-34}]
      ["ej35/"
       {:name ::c5-ej35
        :view #'c5/ej-35}]
      ["respuestas/"
       {:name ::c5-r
        :view #'c5/render-respuestas}]]
     ["6/"
      [""
       {:name ::chapter-6
        :view #'c6/render}]
      ["respuestas/"
       {:name ::c6-r
        :view #'c6/render-respuestas}]]
     ["7/"
      [""
       {:name ::chapter-7
        :view #'c7/render}]
      ["respuestas/"
       {:name ::c7-r
        :view #'c7/render-respuestas}]]
     ["8/"
      [""
       {:name ::chapter-8
        :view #'c8/render}]
      ["respuestas/"
       {:name ::c8-r
        :view #'c8/render-respuestas}]]
     ["9/"
      [""
       {:name ::chapter-9
        :view #'c9/render}]
      ["respuestas/"
       {:name ::c9-r
        :view #'c9/render-respuestas}]]
     ["10/"
      [""
       {:name ::chapter-10
        :view #'c10/render}]
      ["respuestas/"
       {:name ::c10-r
        :view #'c10/render-respuestas}]]
     ["11/"
      [""
       {:name ::chapter-11
        :view #'c11/render}]
      ["respuestas/"
       {:name ::c11-r
        :view #'c11/render-respuestas}]]
          ["12/"
      [""
       {:name ::chapter-12
        :view #'c12/render}]
      ["respuestas/"
       {:name ::c12-r
        :view #'c12/render-respuestas}]]
     ["13/"
      [""
       {:name ::chapter-13
        :view #'c13/render}]
      ["respuestas/"
       {:name ::c13-r
        :view #'c13/render-respuestas}]]
     ["14/"
      [""
       {:name ::chapter-14
        :view #'c14/render}]
      ["respuestas/"
       {:name ::c14-r
        :view #'c14/render-respuestas}]]
     ["15/"
      [""
       {:name ::chapter-15
        :view #'c15/render}]
      ["respuestas/"
       {:name ::c15-r
        :view #'c15/render-respuestas}]]
     ["16/"
      [""
       {:name ::chapter-16
        :view #'c16/render}]
      ["respuestas/"
       {:name ::c16-r
        :view #'c16/render-respuestas}]]
     ["17/"
      [""
       {:name ::chapter-17
        :view #'c17/render}]
      ["respuestas/"
       {:name ::c17-r
        :view #'c17/render-respuestas}]]
     ["18/"
      [""
       {:name ::chapter-18
        :view #'c18/render}]
      ["respuestas/"
       {:name ::c18-r
        :view #'c18/render-respuestas}]]
     ["19/"
      [""
       {:name ::chapter-19
        :view #'c19/render}]
      ["respuestas/"
       {:name ::c19-r
        :view #'c19/render-respuestas}]]
     ["20/"
      [""
       {:name ::chapter-20
        :view #'c20/render}]
      ["respuestas/"
       {:name ::c20-r
        :view #'c20/render-respuestas}]]
     ["21/"
      [""
       {:name ::chapter-21
        :view #'c21/render}]
      ["respuestas/"
       {:name ::c21-r
        :view #'c21/render-respuestas}]]
     ]

    
    ["Files/:file-name" ;; used for getting the correct resource path
     {:name ::files}]
    ]))

(defn ignore-click?
  "have Reitit ignore the click if it is for a media resource (which should be handled by a lightbox modal)"
  [router e el uri]
  (let [uri-extension (goop/extension (str uri)) ;; !!<-- this causes the confirm not to show up
        should-ignore? (and (rfh/ignore-anchor-click? router e el uri)
                            (not-empty uri-extension))]
    (not should-ignore?))) ;; I'm not sure why it requires the boolean to be reversed with "not" here, but this works.

(defn init-routes!
  "Start the routing"
  []
  (rfe/start! routes
              (fn [m]
                (reset! current-view (get-in m [:data :view]))
                (.scrollTo js/window 0 0))
              {:use-fragment false
               :ignore-anchor-click? ignore-click?}))
