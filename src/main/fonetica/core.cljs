(ns fonetica.core
  (:require [reagent.core :as r]
            [reagent.dom :as rdom]
            [fonetica.views.styles :as style]
            [fonetica.routes :as routes]
            [fonetica.views.base :as base]
            [fonetica.views.components.nav :as nav]
            [fonetica.views.components.modal :as modal]))

(defn mount-components []
  (rdom/render [#'nav/navbar] (.getElementById js/document "fonetica-nav"))
  (rdom/render [#'modal/render-modal] (.getElementById js/document "modal-container"))  
  (rdom/render [#'base/main-view] (.getElementById js/document "app")))

(defn init! []  
  (routes/init-routes!)
  (mount-components)
  (when goog.DEBUG ;; use cljs styles for styles in dev
    (println "DEV: We are generating styles in CLJS")
    (style/mount-style (style/fonetica))))
